Ctrl + A Select all (objects, text)
Ctrl + C or Ctrl + Insert Copy to clipboard (objects, text)
Ctrl + X or Shift + Delete Cut to clipboard (objects, text)
Ctrl + V or Shift + Insert Paste from clipboard (objects, text)
Ctrl + N Create a new document, project or similar action
Ctrl + S Save current document, project, etc.
Ctrl + P Print
Ctrl + Z Undo the last action