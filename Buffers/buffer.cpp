#include "buffer.h"

#include <HighLighters/highlighter.h>
#include "Style/appearance.h"

Buffer::Buffer(const QString &fileName) : m_pathFile(fileName), m_highLighter(new HighLighter()) {
    connect(this, &Buffer::openFile, this, &Buffer::setSyntaxForFile);
    connect(this, &Buffer::saveFile, this, &Buffer::setSyntaxForFile);
}

/* ============================================================================================================================= */

Buffer::~Buffer() {

}

/* ============================================================================================================================= */

QString Buffer::pathFile() const {
    return m_pathFile;
}

/* ============================================================================================================================= */

void Buffer::setPathFile(const QString &pathFile) {
    if (pathFile.isEmpty()) {
        m_fileName = QString(tr("untitled"));
        m_hasPath = false;
        emit createFile();
        return;
    }
    if (QFileInfo(pathFile).exists()) {
        m_pathFile = pathFile;
        m_fileName = QFileInfo(pathFile).fileName();
        m_hasPath = true;
        loadFile(pathFile);
    }
}

/* ============================================================================================================================= */

QString Buffer::fileName() const {
    return m_fileName;
}

/* ============================================================================================================================= */

bool Buffer::hasPath() const {
    return m_hasPath;
}

/* ============================================================================================================================= */

void Buffer::save(const QString &textToSave) {
    save(m_pathFile, textToSave);
}

/* ============================================================================================================================= */

bool Buffer::save(const QString &pathFile, const QString &textToSave) {
    m_pathFile = pathFile;
    m_fileName = QFileInfo(pathFile).fileName();
    m_hasPath = true;
    if (!file.isWritable()) { file.close(); }
    file.setFileName(pathFile);
    emit saveFile();
    if (!file.open(QFile::WriteOnly)) {
        errorWithFile(tr("Cannot write file %1:\n%2.").arg(QDir::toNativeSeparators(m_fileName), file.errorString()));
        return false;
    }
    QTextStream out(&file);
    out << textToSave;
    file.close();
    return true;
}

/* ============================================================================================================================= */

QString Buffer::getTextAll() {
    return file.readAll();
}

/* ============================================================================================================================= */

QByteArray Buffer::getTextFragment(qint64 fromGlobalPos, qint64 size) {
    file.seek(fromGlobalPos * 16);
    return file.read(size * 16);;
}

/* ============================================================================================================================= */

bool Buffer::loadFile(const QString &fileName) {
    file.setFileName(fileName);
    if (!file.isOpen()){
        if (!file.open(QFile::ReadOnly)) {
            errorWithFile(tr("Cannot read file %1:\n%2.").arg(fileName).arg(file.errorString()));
            return false;
        }
    }
    emit openFile();
    return true;
}

/* ============================================================================================================================= */

quint64 Buffer::getFileSize() {
    if(!file.isOpen()) { file.setFileName(m_pathFile); }
    return file.size();
}

/* ============================================================================================================================= */

HighLighter *Buffer::getHighLighter() const {
    return m_highLighter;
}

/* ============================================================================================================================= */

void Buffer::setSyntaxForFile() {
    QString name = "";
    QDir dir(":/highlightning/highlights/");
    if (dir.exists() && !QFileInfo(fileName()).suffix().isEmpty()) {
        dir.setFilter(QDir::Files);
        dir.setSorting(QDir::Name);
        QFileInfoList list = dir.entryInfoList();
        for (int i = 0; i < list.size(); ++i) {
            QFileInfo fileInfo = list.at(i);
            JsonClass options(":/highlightning/highlights/" + fileInfo.fileName());
            QString extensions = options.getExtensions();
            name = options.getNameExtension();
            if(extensions.indexOf(QFileInfo(fileName()).suffix()) != -1)
                break;
            name = tr("none");
        }
    }
    setSyntax(name);                                                                                                // Подсветка
}

/* ============================================================================================================================= */

void Buffer::setSyntax(const QString &name, QTextDocument *doc) {
    if(name == "C++/Qt") {
        m_highLighter = new CppHighLighter;
    } else if(name == "Html/Xml") {
        m_highLighter = new HtmlHighLighter;
    } else if(name == "Python") {
        m_highLighter = new PhytonHighLighter;
    } else if(name == "Text") {

    } else {
        m_highLighter = new HighLighter;
    }
    m_highLighter->setDocument(doc);
}
