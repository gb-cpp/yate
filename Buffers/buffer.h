#ifndef BUFFER_H
#define BUFFER_H

#include <QObject>
#include <QDir>
#include <QFileInfo>
#include <QTextStream>

#include "HighLighters/highlighter.h"
#include "HighLighters/cpphighlighter.h"
#include "HighLighters/htmlhighlighter.h"
#include "HighLighters/phytonhighlighter.h"

class Buffer : public QObject {
    Q_OBJECT

public:
    Buffer(const QString &fileName = "");
    ~Buffer();
    QString pathFile() const;
    QString fileName() const;
    QString getTextAll();
    QByteArray getTextFragment(qint64 fromGlobalPos, qint64 size);
    HighLighter *getHighLighter() const;
    quint64 getFileSize();
    void setPathFile(const QString &pathFile);
    void save(const QString &textToSave);
    bool hasPath() const;
    bool save(const QString &pathFile, const QString &textToSave);
    bool loadFile(const QString &fileName);

private:
    QString m_fileName;
    QString m_pathFile;
    QFile file;
    HighLighter *m_highLighter;
    bool m_hasPath;

public slots:
    void setSyntaxForFile();
    void setSyntax(const QString &name, QTextDocument *doc = nullptr);
signals:
    void saveFile();
    void createFile();
    void openFile();
    void errorWithFile(QString);
};

#endif // BUFFER_H
