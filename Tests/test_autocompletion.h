#ifndef TEST_AUTOCOMPLETION_H
#define TEST_AUTOCOMPLETION_H

#include <QObject>
#include <QtTest/QtTest>
#include "mainwindow.h"
#include "Widgets/textedit.h"
#include "Widgets/menubar.h"
#include "initialization.h"
#include "Widgets/textedit.h"
#include "Buffers/buffer.h"

class Test_Autocompletion : public QObject
{
    Q_OBJECT
public:
    explicit Test_Autocompletion(QObject *parent = nullptr);

private:
    TextEdit *tEdit;

private slots:
    void testAutocompletionText();                   // Тест автодополнения из файла-словаря Text
    void testAutocompletionCpp();                    // Тест автодополнения из файла-словаря Cpp
    void testAutocompletionOff();                    // Тест автодополнения в режиме Off
    void testAutocompletionEsc();                    // Тест автодополнения при нажатии Esc
    void testAutocompletionCtrlAndSpace();          // Тест автодополнения при нажатии комбинации Ctrl + Space
};

#endif // TEST_AUTOCOMPLETION_H
