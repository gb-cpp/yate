#ifndef TEST_THEMEMANAGER_H
#define TEST_THEMEMANAGER_H

#include <QtTest/QtTest>

#include "Style/thememanager.h"

class Test_ThemeManager: public QObject
{
    Q_OBJECT
private slots:
    void isDefaultTheme();
    void getName();
    void getFilename();
    void load();
};

#endif // TEST_THEMEMANAGER_H
