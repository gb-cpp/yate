#include <QtTest/QTest>
#include "test_funcconsole.h"
#include "Widgets/winconsole.h"
#include "Widgets/nixconsole.h"
#include <QApplication>
#include "QDebug"

Test_FuncConsole::Test_FuncConsole(QObject *parent) : QObject(parent)
{

}

void Test_FuncConsole::makingFolder()
{
    //
    QString OS = QSysInfo::productType();
    if(OS == "windows"){
        winConsole *console = new winConsole();
		QTest::keyClicks(console, "cd "+qApp->applicationDirPath());
        QTest::keyClick(console, Qt::Key::Key_Return);
		QTest::keyClicks(console, "mkdir testfolder");
        QTest::keyClick(console, Qt::Key::Key_Return);
		QDir dir(qApp->applicationDirPath()+"/testfolder");
        QTest::qWait(200);
        delete console;
        QCOMPARE(dir.exists(), true);
        if(dir.exists()){
            qDebug() << "deleting folder";
             dir.rmdir(dir.path());
        }
    }
    else
    {
        nixConsole *console = new nixConsole();
        QString str = QDir::homePath();
        QTest::keyClicks(console, "cd " + str);
        QTest::keyClick(console, Qt::Key::Key_Return);
        QTest::keyClicks(console, "mkdir testfolder");
        QTest::keyClick(console, Qt::Key::Key_Return);
        str +="/testfolder";
        QDir dir(str);
        QTest::qWait(200);
        delete console;
        QCOMPARE(dir.exists(), true);
        if(dir.exists()){
            qDebug() << "deleting folder";
             dir.rmdir(dir.path());
        }
    }
}

void Test_FuncConsole::gettingOutput()
{
    QString OS = QSysInfo::productType();
    if(OS == "windows"){
        winConsole *console = new winConsole();
        QString testInput = "Hello world";
        QTest::keyClicks(console, "echo " + testInput);
        QTest::keyClick(console, Qt::Key::Key_Return);
        QTest::qWait(200);
		bool PassTest =  console->getQplainTest().indexOf(testInput) >= 0;
        delete console;
        QCOMPARE(PassTest, true);
    }
    else
    {
        nixConsole *console = new nixConsole();
        QTest::keyClicks(console, "pwd");
        QTest::keyClick(console, Qt::Key::Key_Return);
        QTest::qWait(200);
        QString str = console->toPlainText();
        QString prompt =  console->returnPrompt();
        delete console;
        QCOMPARE(str, prompt + "pwd\n" + QDir::currentPath()+"\n" + prompt);
    }
}
