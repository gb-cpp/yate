#include "test_thememanager.h"

void Test_ThemeManager::isDefaultTheme()
{
   QVERIFY(ThemeManager::isDefaultTheme(ThemeManager::DEFAULT_THEME_DARK_NAME));
   QVERIFY(ThemeManager::isDefaultTheme(ThemeManager::DEFAULT_THEME_LIGHT_NAME));
   QVERIFY(ThemeManager::isDefaultTheme("testthemename") == false);
}

void Test_ThemeManager::getName()
{
    ThemeManager themeManager;

    QVERIFY(themeManager.getName() == ThemeManager::DEFAULT_THEME_LIGHT_NAME);

    themeManager.load(ThemeManager::DEFAULT_THEME_DARK);

    QVERIFY(themeManager.getName() == ThemeManager::DEFAULT_THEME_DARK_NAME);
}

void Test_ThemeManager::getFilename()
{
    ThemeManager themeManager;

    QVERIFY(themeManager.getFilename() == ThemeManager::DEFAULT_THEME_LIGHT);

    themeManager.load(ThemeManager::DEFAULT_THEME_DARK);

    QVERIFY(themeManager.getFilename() == ThemeManager::DEFAULT_THEME_DARK);
}

void Test_ThemeManager::load()
{
    ThemeManager themeManager;

    QVERIFY_EXCEPTION_THROWN(themeManager.load("notafile"), QString);
}
