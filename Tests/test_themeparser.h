#ifndef TEST_THEMEPARSER_H
#define TEST_THEMEPARSER_H

#include <QtTest/QtTest>

#include <QObject>

class Test_ThemeParser : public QObject
{
    Q_OBJECT
private slots:
    void parse();
};

#endif // TEST_THEMEPARSER_H
