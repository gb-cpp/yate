#include "test_hotkeyseditor.h"

#include <QDebug>

Test_HotkeysEditor::Test_HotkeysEditor(QObject *parent) : QObject(parent)
	{
	}

void Test_HotkeysEditor::initTestCase()
	{

		editor = HotkeysEditor::getInstance();

		content = new QWidget();
		kse = new QKeySequenceEdit(defaultSequence,content);

		mainWidget = new QWidget();
		mainWidget->setObjectName(widgetName);

		HotkeyEditor_shortcut hotkey;
		HotkeyEditor_MAP hotkeysList;

		hotkey.group_name = groupName;
		hotkey.key_sequence = kse->keySequence();
		hotkey.visible_name = hotkeyVisibleName;
		hotkeysList.insert(hotkeyName,hotkey);

		editor->sendHotkeys(mainWidget,widgetVisibleName,hotkeysList,QRandomGenerator::global()->bounded(100));

		key = mainWidget->metaObject()->className();
		if(editor->weights.contains(key)){
			key = QString::number(editor->weights.value(key)) + "_" +key;
		}
		kse->setObjectName(hotkeyName);
		content->setObjectName(key);
	}

void Test_HotkeysEditor::test_getHotkeys()
	{
		HotkeyEditor_MAP result_hotkeyslist = editor->getHotkeys(mainWidget);
		HotkeyEditor_shortcut result_hotkey = result_hotkeyslist.value(hotkeyName);
		bool test_result = (result_hotkey.group_name == groupName) && (result_hotkey.key_sequence == kse->keySequence()) && (result_hotkey.visible_name == hotkeyVisibleName);
		QCOMPARE(test_result,true);
	}

void Test_HotkeysEditor::test_loadFile()
	{
		/*
		QMap<QString, HotkeysEditor::widget_info> *buffer = nullptr;
		editor->loadSettingsFile("not-a-file",buffer);
		QCOMPARE(buffer,nullptr);
		if(buffer != nullptr) delete buffer;
		*/
		QCOMPARE(true,true); //тест можно удалить
	}

void Test_HotkeysEditor::test_conflict_data()
	{
		QTest::addColumn<QString>("sequence_1");
		QTest::addColumn<QString>("sequence_2");
		QTest::addColumn<QString>("sequence_3");
		QTest::addColumn<int>("result");
		QTest::newRow("empty") << "" << "" << "" << 0; // 0 = 000
		QTest::newRow("conflict_one_two") << "Ctrl+F" << "Ctrl+F" << "Ctrl+H" << 6; // 6 = 110
		QTest::newRow("conflict_one_three") << "Ctrl+F" << "Ctrl+A" << "Ctrl+F" << 5; // 5 = 101
		QTest::newRow("conflict_one_two_three") << "Ctrl+F" << "Ctrl+F" << "Ctrl+F" << 7; // 7 = 111
		QTest::newRow("conflict_two_three") << "Ctrl+A" << "Ctrl+F" << "Ctrl+F" << 3; // 3 = 011
		QTest::newRow("no_conflict") << "Ctrl+A" << "Ctrl+F" << "Ctrl+H" << 0; // 0 = 000
	}

void Test_HotkeysEditor::test_conflict()
	{
		QFETCH(QString, sequence_1);
		QFETCH(QString, sequence_2);
		QFETCH(QString, sequence_3);
		QFETCH(int, result);

		QWidget content_widget;
			content_widget.setObjectName(widgetName);
		QKeySequenceEdit kse_1(QKeySequence(sequence_1),&content_widget);
			kse_1.setObjectName("one");
		QKeySequenceEdit kse_2(QKeySequence(sequence_2),&content_widget);
			kse_2.setObjectName("two");
		QKeySequenceEdit kse_3(QKeySequence(sequence_3),&content_widget);
			kse_3.setObjectName("three");
		QLabel conflict1(&content_widget);
			conflict1.setObjectName("one");
		QLabel conflict2(&content_widget);
			conflict2.setObjectName("two");
		QLabel conflict3(&content_widget);
			conflict3.setObjectName("three");
		editor->checkConflict(&content_widget);
		bool result1 = (result>>2)%2;
		bool result2 = (result>>1)%2;
		bool result3 = (result>>0)%2;
		bool test_result = (!conflict1.text().isEmpty() == result1) && (!conflict2.text().isEmpty() == result2) && (!conflict3.text().isEmpty() == result3);
		QCOMPARE(test_result,true);
	}

void Test_HotkeysEditor::test_UI_EditingFinished()
	{
		kse->setKeySequence(QKeySequence("Ctrl+F"));
		editor->kseFinishedEditing(kse,content);
		HotkeyEditor_MAP result_hotkeyslist = editor->getHotkeys(mainWidget);
		HotkeyEditor_shortcut result_hotkey = result_hotkeyslist.value(hotkeyName);
		QCOMPARE(result_hotkey.key_sequence == kse->keySequence(),true);
	}

void Test_HotkeysEditor::test_UI_Default()
	{
		editor->resetDefault(kse,content);
		HotkeyEditor_MAP result_hotkeyslist = editor->getHotkeys(mainWidget);
		HotkeyEditor_shortcut result_hotkey = result_hotkeyslist.value(hotkeyName);
		QCOMPARE(result_hotkey.key_sequence == defaultSequence,true);
	}

void Test_HotkeysEditor::test_UI_Clear()
	{
		editor->clearKSE(kse,content);
		HotkeyEditor_MAP result_hotkeyslist = editor->getHotkeys(mainWidget);
		HotkeyEditor_shortcut result_hotkey = result_hotkeyslist.value(hotkeyName);
		QCOMPARE(result_hotkey.key_sequence == QKeySequence(),true);
	}
