#ifndef TEST_FUNCCONSOLE_H
#define TEST_FUNCCONSOLE_H

#include <QObject>

class Test_FuncConsole : public QObject
{
    Q_OBJECT
public:
	explicit Test_FuncConsole(QObject *parent = nullptr);

private slots:
	void makingFolder(); //Тест на создание папки
	void gettingOutput(); //Тест на получение информации из консоли
};

#endif // TEST_FUNCCONSOLE_H
