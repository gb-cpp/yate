#include "test_autocompletion.h"
#include "Style/appearance.h"
#include <QKeySequence>

Test_Autocompletion::Test_Autocompletion(QObject *parent) : QObject(parent)
{
    Appearance *appear = Appearance::getInstance();

    Buffer *buff = new Buffer;

    tEdit = new TextEdit(appear, buff);
}

void Test_Autocompletion::testAutocompletionText()
{
    // Вводим несколько символов и нажимаем Enter для вставки, затем переходим на новую строку и
    // Вводим несколько других символов и нажимаем Enter для вставки
    // Также проверяем вставку текста с учетом регистра

    tEdit->clear();

    tEdit->VisibleAutocompleteText(true);

    QCompleter *c = tEdit->completer();

    QTest::keyClicks(tEdit, "fran");
    QTest::keyClick(c->popup(), Qt::Key_Enter);
    QTest::keyClick(tEdit, Qt::Key_Enter);
    QTest::keyClicks(tEdit, "ap");
    QTest::keyClick(c->popup(), Qt::Key_Down);
    QTest::keyClick(c->popup(), Qt::Key_Enter);

    QCOMPARE(tEdit->toPlainText(), QString("France\nAPI"));

}

void Test_Autocompletion::testAutocompletionCpp()
{
    // Вводим несколько символов, нажимаем в списке слов кандидатов стрелочку вниз, чтобы выбрать не первое предлагаемое слово
    // и нажимаем Enter для вставки

    tEdit->clear();

    tEdit->VisibleAutocompleteCpp(true);

    QCompleter *c = tEdit->completer();

    QTest::keyClicks(tEdit, "fo");
    QTest::keyClick(c->popup(), Qt::Key_Down);
    QTest::keyClick(c->popup(), Qt::Key_Enter);

    QCOMPARE(tEdit->toPlainText(), QString("for()"));
}

void Test_Autocompletion::testAutocompletionOff()
{
    // Проверяем отсутствие функционирования автодополнения

    tEdit->clear();

    tEdit->VisibleAutocompleteOff(true);

    QCOMPARE(tEdit->completer() == nullptr, true);
}

void Test_Autocompletion::testAutocompletionEsc()
{
    // Вводим несколько символов и нажимаем Esc для отмены вставки по автодополнению

    tEdit->clear();

    tEdit->VisibleAutocompleteText(true);

    QCompleter *c = tEdit->completer();

    QTest::keyClicks(tEdit, "fran");

    QCOMPARE(c->popup()->isVisible(), true);

    QTest::keyClick(c->popup(), Qt::Key_Escape);

    QCOMPARE(c->popup()->isVisible(), false);

    QCOMPARE(tEdit->toPlainText(), QString("fran"));
}

void Test_Autocompletion::testAutocompletionCtrlAndSpace()
{
    // Проверяем комбинацию Ctrl + Space для принудительного вызова списка слов кандидатов для автодополнения

    tEdit->clear();

    tEdit->VisibleAutocompleteText(true);

    QCompleter *c = tEdit->completer();

    QTest::keySequence(tEdit, QKeySequence("Ctrl+Space"));
    QCOMPARE(c->popup()->isVisible(), true);

    QTest::keyClick(c->popup(), Qt::Key_Down);
    QTest::keyClick(c->popup(), Qt::Key_Enter);

    QCOMPARE(c->popup()->isVisible(), false);

    QCOMPARE(tEdit->toPlainText(), QString("A4"));
}
