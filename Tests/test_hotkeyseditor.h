#ifndef TEST_HOTKEYSEDITOR_H
#define TEST_HOTKEYSEDITOR_H

#include <QObject>
#include <QtTest>
#include "Widgets/hotkeyseditor.h"
class Test_HotkeysEditor : public QObject
{
	Q_OBJECT
public:
	explicit Test_HotkeysEditor(QObject *parent = nullptr);
private:

	const QString widgetName = "test_widget";
	const QString groupName = "test_group";
	const QString hotkeyName = "test_hotkey";
	const QString hotkeyVisibleName = "Test Hotkey";
	const QString widgetVisibleName = "Test Widget";
	const QKeySequence defaultSequence = QKeySequence("Ctrl+N");
	QString key;
	HotkeysEditor *editor = nullptr;
	QWidget *mainWidget = nullptr;
	QWidget *content = nullptr;
	QKeySequenceEdit *kse = nullptr;
private slots:
	void initTestCase();
	void test_getHotkeys();
	void test_loadFile();
	void test_conflict_data();
	void test_conflict();
	void test_UI_EditingFinished();
	void test_UI_Default();
	void test_UI_Clear();

};

#endif // TEST_HOTKEYSEDITOR_H
