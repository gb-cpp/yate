#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    SettingsLoader * settings = SettingsLoader::getInstance();


    window()->setGeometry(settings->getValue({"main-window", "position-x"}).toInt(),
                          settings->getValue({"main-window", "position-y"}).toInt(),
                          settings->getValue({"main-window", "width"}).toInt(),
                          settings->getValue({"main-window", "height"}).toInt());
    this->moveToCenter();

    mainSplitter = nullptr;
    mdiArea = nullptr;
    workspaceSplitter = nullptr;
	dragArea = nullptr;
}


/* ============================================================================================================================= */

MainWindow::~MainWindow() {

}

/* ============================================================================================================================= */

void MainWindow::closeEvent(QCloseEvent *event) {

    SettingsLoader * settings = SettingsLoader::getInstance();

    settings->setValue({"main-window", "position-x"}, window()->geometry().x());
    settings->setValue({"main-window", "position-y"}, window()->geometry().y());
    settings->setValue({"main-window", "width"}, window()->geometry().width());
    settings->setValue({"main-window", "height"}, window()->geometry().height());
    settings->saveJsonToFile();

    mdiArea->closeAllSubWindows();
    if ((mdiArea->subWindowList()).isEmpty()) {
        event->accept();
    } else {
        event->ignore();
    }
}
/* ============================================Drag & Drop===================================================================== */
void MainWindow::setDragArea(DragArea* value)
{
    dragArea = value;
}

/* ============================================================================================================================= */

void MainWindow::setAppearance(Appearance *value) {
    appearance = value;
}

/* ====================================== Окно по центру======================================================================== */

void MainWindow::moveToCenter()
{
    move(qApp->desktop()->availableGeometry(this).center()-rect().center());
}

/* ============================================================================================================================= */

void MainWindow::setMdiArea(QMdiArea *value) {
    mdiArea = value;
}

/* ============================================================================================================================= */

void MainWindow::setWorkspaceSplitter(WorkspaceSplitter *workspaceSplitter) {
    this->workspaceSplitter = workspaceSplitter;
}

/* ============================================================================================================================= */

void MainWindow::setMainSplitter(QSplitter *value) {
    mainSplitter = value;
}
