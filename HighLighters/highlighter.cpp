#include "highlighter.h"

#include "Style/appearance.h"
#include "Style/thememanager.h"

void HighLighter::setName(const QString &name) {
    m_name = name;
}

/* ============================================================================================================================= */

void HighLighter::highlightBlock(const QString &text) {
    Q_UNUSED(text)
}

/* ============================================================================================================================= */

void HighLighter::highLighterKeyWords(const QString &text) {
    for (const RuleKeywords &item : m_vecKeyWords) {
        QRegularExpression expression("\\b" + item.word + "\\b");
        expression.setPatternOptions(item.ignoreCase? QRegularExpression::NoPatternOption :
                                                      QRegularExpression::CaseInsensitiveOption);
        QRegularExpressionMatchIterator i = expression.globalMatch(text);
        while (i.hasNext()) {
            QRegularExpressionMatch match = i.next();
            setFormat(match.capturedStart(), match.capturedLength(), item.format);
        }
    }
}

/* ============================================================================================================================= */

void HighLighter::highLighterRules(const QString &text) {
    for(const Rule &ruleItem : m_vecRules) {
        QRegularExpression expression(ruleItem.rule);
        QRegularExpressionMatchIterator i = expression.globalMatch(text);
        while (i.hasNext()) {
            QRegularExpressionMatch match = i.next();
            setFormat(match.capturedStart(), match.capturedLength(), ruleItem.format);
        }
    }
}

/* ============================================================================================================================= */

void HighLighter::loadRules() {
    const ThemeManager * themeManager = Appearance::getInstance()->getThemeManager();
    QString foreground, font;

    for(const QString &name : options->getMainObject().keys()) {
        foreground = themeManager->getCurrentTheme()->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, name+"Foreground");
        font = themeManager->getCurrentTheme()->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, name+"Font");

        if(!options->getKeywordsInRulerName(name).isEmpty()) {
            for(const QString &item : options->getKeywordsInRulerName(name)) {
                RuleKeywords tempRuleKeyWords;
                tempRuleKeyWords.ignoreCase = options->getOptionCaseWords(name);
                tempRuleKeyWords.word = item;

                tempRuleKeyWords.format = options->getTextCharFormat(foreground,font);
                m_vecKeyWords.push_back(tempRuleKeyWords);
            }
        }
        if(!options->getRegExpInRulerName(name).isEmpty()) {
            Rule itemRule;
            itemRule.rule = options->getRegExpInRulerName(name);
            itemRule.format = options->getTextCharFormat(foreground, name);
            m_vecRules.push_back(itemRule);
        }
    }
}

/* ============================================================================================================================= */

HighLighter::HighLighter(QTextDocument *parent) : QSyntaxHighlighter(parent), m_name("None") {
    m_vecKeyWords.clear();
    m_vecRules.clear();

    threadPool.setMaxThreadCount(1);
    if(Appearance::getInstance())
        connect(Appearance::getInstance(),SIGNAL(onUpdated()),this,SLOT(onAppearanceChanged()));
}

/* ============================================================================================================================= */

QString HighLighter::getName() const {
    return m_name;
}

void HighLighter::onAppearanceChanged()
{
    if(getName() != "None")
        threadPool.start(new ReHighLighter(this), QThread::HighPriority);
}
