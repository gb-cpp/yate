#include "brackets.h"

void Brackets::highlightBlock(const QString &text) {
    UTextBlockData *data = new UTextBlockData();
    setBracketsOpt(RoundBrackets, data, text);
    setBracketsOpt(CurlyBraces, data, text);
    setBracketsOpt(SquareBrackets, data, text);
    setCurrentBlockUserData(data);
}

/* ============================================================================================================================= */

bool Brackets::isLeftBrackets(QChar symbol) {
    return symbol == '(' || symbol == '{' || symbol == '[';
}

/* ============================================================================================================================= */

bool Brackets::isRightBrackets(QChar symbol) {
    return symbol == ')' || symbol == '}' || symbol == ']';
}

/* ============================================================================================================================= */

QChar Brackets::getPairBrackets(QChar symbol) {
    if(symbol == '(')
        return ')';
    else if(symbol == ')')
        return '(';
    if(symbol == '{')
        return '}';
    else if(symbol == '}')
        return '{';
    if(symbol == '[')
        return ']';
    else if(symbol == ']')
        return '[';
    return '0';
}

/* ============================================================================================================================= */

void Brackets::setBracketsOpt(UBrackets brackets, UTextBlockData *data, QString text) {
    QChar leftChar = '0';
    QChar rightChar = '0';
    switch(brackets) {
        case RoundBrackets:
            leftChar = '('; rightChar = ')';
            break;
        case CurlyBraces:
            leftChar = '{'; rightChar = '}';
            break;
        case SquareBrackets:
            leftChar = '['; rightChar = ']';
            break;
    }
    insertBrackets(leftChar, rightChar, data, text);
}

/* ============================================================================================================================= */

void Brackets::insertBrackets(QChar leftChar, QChar rightChar, UTextBlockData *data, QString text) {
    int leftPosition = text.indexOf(leftChar);
    while(leftPosition != -1) {
        UBracketInfo *info = new UBracketInfo();
        info->character = leftChar;
        info->position = leftPosition;

        data->insert(info);
        leftPosition = text.indexOf(leftChar, leftPosition + 1);
    }
    int rightPosition = text.indexOf(rightChar);
    while(rightPosition != -1) {
        UBracketInfo *info = new UBracketInfo();
        info->character = rightChar;
        info->position = rightPosition;
        data->insert(info);
        rightPosition = text.indexOf(rightChar, rightPosition + 1);
    }
}

/* ============================================================================================================================= */

Brackets::Brackets(QTextDocument *parent) : HighLighter(parent) {

}

void Brackets::onChangeAppearance()
{
}

/* ============================================================================================================================= */

void Brackets::matchBrackets() {
    QTextEdit *edit = (QTextEdit*)sender();
    QList <QTextEdit::ExtraSelection> selections;
    edit->setExtraSelections(selections);
    QTextBlock textBlock = edit->textCursor().block();
    UTextBlockData *data = static_cast <UTextBlockData *> (textBlock.userData());
    if(data) {
        QVector <UBracketInfo *> brackets = data->brackets();
        int position = edit->textCursor().block().position();
        for(int i = 0; i < brackets.size(); i++) {
            UBracketInfo *bracket = brackets.at(i);
            int currentPosition = edit->textCursor().position() - textBlock.position();
            if (bracket->position == currentPosition && isLeftBrackets(bracket->character)) {       // Нажали на левую скобку?
                if (matchLeftBrackets(textBlock, i + 1, 0))
                    createBracketsSelection(position + bracket->position);
            }
            if (bracket->position == currentPosition - 1 && isRightBrackets(bracket->character)) {  // Нажали на правую скобку?
                if (matchRightBrackets(textBlock, i - 1, 0))
                    createBracketsSelection(position + bracket->position);
            }
        }
    }
}

/* ============================================================================================================================= */

bool Brackets::matchLeftBrackets(QTextBlock currentBlock, int index,  int numberLeftBracket) {
    UTextBlockData *data = static_cast <UTextBlockData *> (currentBlock.userData());
    QVector<UBracketInfo *> brackets = data->brackets();
    int positionInDocument = currentBlock.position();
    for (; index < brackets.count(); index++) {                                                     // Соответствие в строке?
            UBracketInfo *bracket = brackets.at(index);
            if (isLeftBrackets(bracket->character)) {
                ++numberLeftBracket;
                continue;
            }
            if (isRightBrackets(bracket->character) && numberLeftBracket == 0) {
                createBracketsSelection(positionInDocument + bracket->position);
                return true;
            } else {
                --numberLeftBracket;
            }
        }
        currentBlock = currentBlock.next();                                                         // Нет совпадений?
                                                                                                    // Попробуем следующий блок
        if (currentBlock.isValid()) return matchLeftBrackets(currentBlock, 0, numberLeftBracket);
        return false;                                                                               // Совпадений нет
}

/* ============================================================================================================================= */

bool Brackets::matchRightBrackets(QTextBlock currentBlock, int index, int numberRightBracket) {
    UTextBlockData *data = static_cast <UTextBlockData *> (currentBlock.userData());
    QVector<UBracketInfo *> brackets = data->brackets();
    int positionInDocument = currentBlock.position();
    for (int i = index; i >= 0; i--) {
        UBracketInfo *bracket = brackets.at(i);
        if (isRightBrackets(bracket->character)) {
            ++numberRightBracket;
            continue;
        }
        if (isLeftBrackets(bracket->character) && numberRightBracket == 0) {
            createBracketsSelection(positionInDocument + bracket->position);
            return true;
        } else {
            --numberRightBracket;
        }
    }
    currentBlock = currentBlock.previous();
    if (currentBlock.isValid()) {
        UTextBlockData *data = static_cast <UTextBlockData *> (currentBlock.userData());
        QVector <UBracketInfo *> brackets = data->brackets();
        return matchRightBrackets(currentBlock, brackets.count() - 1, numberRightBracket);
    }
    return false;
}

/* ============================================================================================================================= */

void Brackets::createBracketsSelection(int position) {
    QTextEdit *edit = (QTextEdit*)sender();
    QList <QTextEdit::ExtraSelection> listSelections = edit->extraSelections();
    QTextEdit::ExtraSelection selection;
    QTextCharFormat format = selection.format;
    format.setForeground(QColor(Appearance::getInstance()->getThemeManager()->getCurrentTheme()->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "bracketsColorHighlight")));
    selection.format = format;
    QTextCursor cursor = edit->textCursor();
    cursor.setPosition(position);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
    selection.cursor = cursor;
    listSelections.append(selection);
    edit->setExtraSelections(listSelections);
}

/* ============================================================================================================================= */

QVector<UBracketInfo *> UTextBlockData::brackets() {
    return m_brackets;
}

/* ============================================================================================================================= */

void UTextBlockData::insert(UBracketInfo *info) {
    int i = 0;
    while(i < m_brackets.size() && info->position > m_brackets.at(i)->position) {
        i++;
    }
    m_brackets.insert(i, info);
}
