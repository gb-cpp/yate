#include "jsonclass.h"

JsonClass::JsonClass(const QString &path) {
    m_JsonObject = getJsonObjectInFile(path);
}

/* ============================================================================================================================= */

QJsonObject JsonClass::getMainObject() const {
    return m_JsonObject;
}

/* ============================================================================================================================= */

QJsonObject JsonClass::getJsonObjectInFile(const QString &path) const {
    QFile file(path);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString set = file.readAll();
    return QJsonDocument::fromJson(set.toUtf8()).object();
}

/* ============================================================================================================================= */

QJsonObject JsonClass::getJsonObjectinName(const QString &name) const {
    return m_JsonObject.find(name)->toObject();
}

/* ============================================================================================================================= */

QStringList JsonClass::getKeywordsInRulerName(const QString &rulerName) const {
    QStringList list;
    for(const QJsonValue &word : m_JsonObject.find(rulerName)->toObject().find("keywords")->toArray())
        list.append(word.toString());
    return list;
}

/* ============================================================================================================================= */

QString JsonClass::getExtensions() const {
    return m_JsonObject.find("SyntaxDefinition")->toObject().find("extensions")->toString();
}

/* ============================================================================================================================= */

QString JsonClass::getNameExtension() const {
    return m_JsonObject.find("SyntaxDefinition")->toObject().find("name")->toString();
}

/* ============================================================================================================================= */

QString JsonClass::getOptionInRulerName(const QString &rulerName, const QString &optionName) const {
    return m_JsonObject.find(rulerName)->toObject().find(optionName)->toString();
}

/* ============================================================================================================================= */

bool JsonClass::getOptionCaseWords(const QString &rulerName) const {
    return m_JsonObject.find(rulerName)->toObject().find("ignoreCase")->toBool();
}

/* ============================================================================================================================= */

QTextCharFormat JsonClass::getTextCharFormat(const QString &colorName, const QString &fontStyle) const {
    QTextCharFormat charFormat;
    QColor color(colorName);
    charFormat.setForeground(color);
    if (fontStyle.contains("bold", Qt::CaseInsensitive))
        charFormat.setFontWeight(QFont::Bold);
    if (fontStyle.contains("italic", Qt::CaseInsensitive))
        charFormat.setFontItalic(true);
    return charFormat;
}

/* ============================================================================================================================= */

QTextCharFormat JsonClass::getTextCharFormatInNameRule(const QString &name) const {
    QTextCharFormat charFormat;
    charFormat = getTextCharFormat(getOptionInRulerName(name, "foreground"), getOptionInRulerName(name, "font"));
    return  charFormat;
}

/* ============================================================================================================================= */

QString JsonClass::getRegExpInRulerName(const QString &rulerName) const {                                   //QRegularExpression
    return m_JsonObject.find(rulerName)->toObject().find("rule")->toString();
}
