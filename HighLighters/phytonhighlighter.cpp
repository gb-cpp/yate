#include "phytonhighlighter.h"

#include "Style/appearance.h"
#include "Style/thememanager.h"

void PhytonHighLighter::highlightBlock(const QString &text) {
    highLighterRules(text);
    highLighterKeyWords(text);
    QRegularExpression func_def("([\\d\\w_]+)(?=(\\s*\\(.*\\)))");
    QRegularExpressionMatchIterator func_def_item = func_def.globalMatch(text);
    while (func_def_item.hasNext()) {
        QRegularExpressionMatch match = func_def_item.next();
        if(text.indexOf(QRegExp("([^\\w\\d\\S]|^)def[\\s]*")) != -1
                && text.indexOf(QRegExp("def[\\s]*")) < match.capturedStart()) {
            setFormat(match.capturedStart(), match.capturedLength(), defFuncfrmt);
        }
    }
    QRegularExpression ex_patrn("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(\"[^\"]*)|('[^']*)");
    QRegularExpressionMatchIterator ex_item = ex_patrn.globalMatch(text);
    while (ex_item.hasNext()) {
        QRegularExpressionMatch match_item = ex_item.next();
        setFormat(match_item.capturedStart() - 1, 2, strFrmt);
        setFormat(match_item.capturedStart() + match_item.capturedLength() - 1, 1, strFrmt);
        if((match_item.capturedStart() > 1
            && text.mid(match_item.capturedStart() - 2, 2).indexOf(QRegExp("[^aA-zZ_](f|F)")) != -1)
                || (match_item.capturedStart() == 1
                    && text.mid(match_item.capturedStart() - 1, 1).indexOf(QRegExp("f|F")) != -1)) {
            QRegularExpression f_ex("(?:(?:\"|')[^{}]*\{)|(?:}[^{}]*\{)|(?:}[^{}]*(?:\"|'))|(?:(?:\"|')[^{}]*(?:\"|'))");
            QRegularExpressionMatchIterator f_ex_item = f_ex.globalMatch(text.mid(match_item.capturedStart(),
                                                                                  match_item.capturedLength()));
            while (f_ex_item.hasNext()) {
                QRegularExpressionMatch match = f_ex_item.next();
                setFormat(match_item.capturedStart() + match.capturedStart()+1, match.capturedLength()-2, strFrmt);
            }
        } else {
            setFormat(match_item.capturedStart(), match_item.capturedLength(), strFrmt);
        }
    }
    QRegularExpression com_exp("#");
    QRegularExpressionMatchIterator com_exp_item = com_exp.globalMatch(text);
    while (com_exp_item.hasNext()) {
        QRegularExpressionMatch match_com = com_exp_item.next();
        if(text.contains(QRegExp("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(\"[^\"]*)"))) {
            QRegularExpression quote_exp("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')");
            QRegularExpressionMatchIterator quote_item = quote_exp.globalMatch(text);
            while (quote_item.hasNext()) {
                QRegularExpressionMatch match_quote = quote_item.next();
                if(match_com.capturedStart() > match_quote.capturedStart()
                        && match_com.capturedStart() < match_quote.capturedStart() + match_quote.capturedLength())
                    continue;
                else
                    setFormat(match_com.capturedStart(), text.length() - match_com.capturedStart(), commFrmt);
            }
        }
        else {
            setFormat(match_com.capturedStart(), text.length() - match_com.capturedStart(), commFrmt);
        }
    }
    QRegularExpression func_arg("[\\d\\w_]+(?=(\\s*\\(.*\\)))");
    QRegularExpressionMatchIterator func_arg_item = func_arg.globalMatch(text);
    while (func_arg_item.hasNext()) {
        QRegularExpressionMatch match_func_arg_item = func_arg_item.next();
        QRegularExpression arg("[\\w\\d_\\s]*=");
        QRegularExpressionMatchIterator arg_item = arg.globalMatch(match_func_arg_item.capturedTexts().at(1));
        while (arg_item.hasNext()) {
            QRegularExpressionMatch match = arg_item.next();
            setFormat(match_func_arg_item.capturedStart() +
                      match_func_arg_item.capturedLength() +
                      match.capturedStart(),
                      match.capturedLength() - 1, argFormat);
        }
    }
    QRegularExpression ex_prn("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(\"[^\"]*)|('[^']*)");
    QRegularExpressionMatchIterator ex_it = ex_prn.globalMatch(text);
    while (ex_it.hasNext()) {
        QRegularExpressionMatch match_it = ex_it.next();
        QRegularExpression exp("\\\\\\\\|\\\\n|\\\\t|\\\\r|\\\\0|\\\\'|\\\\\"");
        QRegularExpressionMatchIterator i = exp.globalMatch(text.mid(match_it.capturedStart(), match_it.capturedLength() + 1));
        while (i.hasNext()) {
            QRegularExpressionMatch match = i.next();
            setFormat(match_it.capturedStart() + match.capturedStart(), match.capturedLength(), escapeFrmt);
        }
    }
}

/* ============================================================================================================================= */

PhytonHighLighter::PhytonHighLighter(QTextDocument *parent) : HighLighter(parent) {
    options = new JsonClass(":/highlightning/highlights/python.json");
    setName(options->getNameExtension());
    loadRules();
    connect(Appearance::getInstance(), SIGNAL(onUpdate()), this, SLOT(onChangeAppearance()));
}

void PhytonHighLighter::loadRules()
{
    HighLighter::loadRules();

    const Theme * theme = Appearance::getInstance()->getThemeManager()->getCurrentTheme();

    defFuncfrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "DefFuncNameForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "DefFuncNameFont")
    );
    strFrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "StringForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "StringFont")
                );
    commFrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentFont")
                );
    argFormat = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "NamesArgsForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "NamesArgsFont")
                );
    escapeFrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "EscapeCharForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "EscapeCharFont")
                );
}

void PhytonHighLighter::onChangeAppearance()
{
    loadRules();
}
