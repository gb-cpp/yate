#ifndef BRACKETS_H
#define BRACKETS_H

#include "highlighter.h"
#include "Style/appearance.h"

/* Структура UBracketInfo ======================================================================================================= */

struct UBracketInfo {
    QChar character;
    int position;
};

/* Класс UTextBlockData ========================================================================================================= */

class UTextBlockData: public QTextBlockUserData {
public:
    QVector<UBracketInfo *> brackets();
    void insert(UBracketInfo *info);

private:
    QVector<UBracketInfo *> m_brackets;
};

/* Класс Brackets =============================================================================================================== */

class Brackets : public HighLighter {
    Q_OBJECT
public:
    explicit Brackets(QTextDocument *parent = nullptr);

protected:
    void highlightBlock(const QString &text) override;

private:
    bool isLeftBrackets(QChar symbol);
    bool isRightBrackets(QChar symbol);
    QChar getPairBrackets(QChar symbol);
    enum UBrackets {RoundBrackets, CurlyBraces, SquareBrackets};
    void setBracketsOpt(UBrackets brackets, UTextBlockData *data, QString text);
    void insertBrackets(QChar leftChar, QChar rightChar, UTextBlockData *data, QString text);
    bool matchLeftBrackets(QTextBlock currentBlock, int index, int numberLeftBracket);
    bool matchRightBrackets(QTextBlock currentBlock, int index, int numberRightBracket);
    void createBracketsSelection(int position);

public slots:
    void matchBrackets();
    void onChangeAppearance();
};

#endif // BRACKETS_H
