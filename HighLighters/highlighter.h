#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QTextEdit>
#include <QSignalMapper>
#include <QThreadPool>
#include "./jsonclass.h"

class HighLighter : public QSyntaxHighlighter {
    Q_OBJECT
private:
    struct RuleKeywords{
        bool ignoreCase;
        QString word;
        QTextCharFormat format;
    };
    struct Rule {
        QString rule;
        QTextCharFormat format;
    };
    QVector<RuleKeywords> m_vecKeyWords;
    QVector<Rule> m_vecRules;
    QString m_name;

protected:
    void highlightBlock(const QString &text) override;
    void highLighterKeyWords(const QString &text);
    void highLighterRules(const QString &text);
    void setName(const QString &name);
    void loadRules();
    JsonClass *options;
    QThreadPool threadPool;
public:
    explicit HighLighter(QTextDocument *parent = nullptr);
    QString getName() const;

private slots:
    void onAppearanceChanged();
};

class ReHighLighter: public QRunnable
{
    public:
    ReHighLighter(HighLighter * hl) {
        highlighter = hl;
    }

    void run() override {
        highlighter->rehighlight();
    }

    private:
        HighLighter * highlighter;
};

#endif // HIGHLIGHTER_H
