#ifndef CPPHIGHLIGHTER_H
#define CPPHIGHLIGHTER_H

#include "highlighter.h"
#include "jsonclass.h"
#include <QRegularExpression>

class CppHighLighter : public HighLighter
{
    Q_OBJECT

private:    
    enum {NormalState = -1, InsideStyleComment};
    QTextCharFormat comBlockfrmt;
    QTextCharFormat comfrmt;
    QTextCharFormat preprocwsorfrmt;
    QTextCharFormat stringfrmt;

protected:
    void highlightBlock(const QString &text) override;

public:
    explicit CppHighLighter(QTextDocument *parent = nullptr);

    void loadRules();

private slots:
    void onChangeAppearance();
};

#endif // CPPHIGHLIGHTER_H
