#ifndef HTMLHIGHLIGHTER_H
#define HTMLHIGHLIGHTER_H

#include <QRegularExpression>

#include "highlighter.h"
#include "jsonclass.h"

class HtmlHighLighter : public HighLighter {
    Q_OBJECT

public:
    explicit HtmlHighLighter(QTextDocument *parent = nullptr);

    void loadRules();

private slots:
    void onChangeAppearance();

protected:
    void highlightBlock(const QString &text) override;

private:

    /* Состояние подсветки, в которой находится текстовый блок на момент его закрытия =========================================== */

    enum {
        None,       // Без подсветки
        Tag,        // Подсветка внутри тега
        Comment,    // Внутри комментария
        Quote       // Внутри кавычек, которые внутри тега
    };
    QTextCharFormat edgeTagFormat;
    QTextCharFormat insideTagFormat;
    QTextCharFormat tagsFormat;
    QTextCharFormat multiLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat argumentFormat;
};

#endif // HTMLHIGHLIGHTER_H
