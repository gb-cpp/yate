#ifndef JSONCLASS_H
#define JSONCLASS_H

#include <QJsonObject>
#include <QTextCharFormat>
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>

class JsonClass {
public:
    explicit JsonClass(const QString &path);
    QJsonObject getMainObject() const;
    QJsonObject getJsonObjectInFile(const QString &path) const;
    QString getExtensions() const;
    QJsonObject getJsonObjectinName(const QString &name) const;
    QString getOptionInRulerName(const QString &rulerName, const QString &optionName) const;
    bool getOptionCaseWords(const QString &rulerName) const;
    QStringList getKeywordsInRulerName(const QString &rulerName) const;
    QString getRegExpInRulerName(const QString &rulerName) const;
    QTextCharFormat getTextCharFormat(const QString &colorName, const QString &style) const;
    QString getNameExtension() const;
    QTextCharFormat getTextCharFormatInNameRule(const QString &name) const;

private:
     QJsonObject m_JsonObject;

};

#endif // JSONCLASS_H
