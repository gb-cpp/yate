#include "htmlhighlighter.h"

#include "Style/appearance.h"
#include "Style/thememanager.h"

HtmlHighLighter::HtmlHighLighter(QTextDocument *parent) : HighLighter(parent) {
    options = new JsonClass(":/highlightning/highlights/html.json");
    setName(options->getNameExtension());
    loadRules();
    connect(Appearance::getInstance(), SIGNAL(onUpdate()), this, SLOT(onChangeAppearance()));
}

void HtmlHighLighter::loadRules()
{
    HighLighter::loadRules();

    const Theme * theme = Appearance::getInstance()->getThemeManager()->getCurrentTheme();

    edgeTagFormat = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "EdgeTagForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "EdgeTagFont")
                );
    insideTagFormat = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "InsideTagForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "InsideTagFont")
                );
    tagsFormat = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "TagsForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "TagsFont")
                );
    multiLineCommentFormat = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentFont")
                );
    quotationFormat = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "StringForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "StringFont")
                );
    argumentFormat = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "ArgumentForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "ArgumentFont")
                );
}

/* ============================================================================================================================= */

void HtmlHighLighter::highlightBlock(const QString &text) {
    highLighterKeyWords(text);                                                  // Ключевые слова
    highLighterRules(text);                                                     // Правила
    QRegExp openTag("<");
    QRegExp closeTag(">");
    QRegExp commentStartExpression ("<!--");
    QRegExp commentEndExpression ("-->");
    QRegExp quotes ("\"");
    setCurrentBlockState(None);
    int startIndex = 0;
    if (previousBlockState() != Tag && previousBlockState() != Quote) {         // Если не находимся внутри тега
        startIndex = openTag.indexIn(text);                                     // То пытаемся найти начало следующего тега
    }
    int subPreviousTag = previousBlockState();                                  // Забираем состояние предыдущего текстового блока
    while (startIndex >= 0) {                                                   // Ищем символ конца тега
        int endIndex = closeTag.indexIn(text, startIndex);
        int tagLength;
        if (endIndex == -1) {                                                   // Если конец тега не найден
            setCurrentBlockState(Tag);                                          // То устанавливаем состояние блока
            tagLength = text.length() - startIndex;
        } else {
            tagLength = endIndex - startIndex + closeTag.matchedLength();
        }
        if (subPreviousTag != Tag) {                                            // Устанавливаем форматирования для тега
                                                                                // С начала тега и до конца
                                                                                // Если предыдущее состояние не равнялось Tag
            setFormat(startIndex, 1, edgeTagFormat);
            setFormat(startIndex + 1, tagLength - 1, insideTagFormat);
            QRegularExpression arg("(\\s|\")[\\w\\d_]*=");                      // Подсветка атрибутов
            QRegularExpressionMatchIterator arg_item = arg.globalMatch(text);
            while (arg_item.hasNext()) {
                QRegularExpressionMatch match = arg_item.next();
                setFormat(match.capturedStart() + 1, match.capturedLength() - 2, argumentFormat);
            }
        } else {                                                                // Если же находимся уже внутри тега
                                                                                // С самого начала блока
                                                                                // И до конца тега
            setFormat(startIndex, tagLength, insideTagFormat);
            QRegularExpression arg(("(\\s|\")[\\w\\d_]*?="));                   // Подсветка атрибутов
            QRegularExpressionMatchIterator arg_item = arg.globalMatch(text);
            while (arg_item.hasNext()) {
                QRegularExpressionMatch match = arg_item.next();
                setFormat(match.capturedStart() + 1, match.capturedLength() - 2, argumentFormat);
            }
            arg.setPattern("[\\w\\d_]*?=");
            if(text.indexOf(QRegularExpression("[\\w\\d_]*?=")) == 0)
                setFormat(0, arg.match(text).capturedLength(), argumentFormat);
            subPreviousTag = None;
        }
        setFormat(endIndex, 1, edgeTagFormat);                                  // Форматируем символ конца тега
        /// QUOTES ///////////////////////////////////////
        int startQuoteIndex = 0;
        if (previousBlockState() != Quote) {                                    // Если не находимся в кавычках с предыдущего блока
            startQuoteIndex = quotes.indexIn(text, startIndex);                 // То пытаемся найти начало кавычек
        }
                                                                                // Подсвечиваем все кавычки внутри тега
        while (startQuoteIndex >= 0 && ((startQuoteIndex < endIndex) || (endIndex == -1))) {
            int endQuoteIndex = quotes.indexIn(text, startQuoteIndex + 1);
            int quoteLength;
            if (endQuoteIndex == -1) {                                          // Если закрывающая кавычка не найдена
                setCurrentBlockState(Quote);                                    // То устанавливаем состояние Quote для блока
                quoteLength = text.length() - startQuoteIndex;
            } else {
                quoteLength = endQuoteIndex - startQuoteIndex + quotes.matchedLength();
            }
            if ((endIndex > endQuoteIndex) || endIndex == -1) {
                setFormat(startQuoteIndex, quoteLength, quotationFormat);
                startQuoteIndex = quotes.indexIn(text, startQuoteIndex + quoteLength);
            } else {
                break;
            }
        }
        //////////////////////////////////////////////////
        startIndex = openTag.indexIn(text, startIndex + tagLength);             // Снова ищем начало тега
    }
    int startCommentIndex = 0;
    if (previousBlockState() != Comment) {                                      // Если предыдущее состояние тега не комментарий
        startCommentIndex = commentStartExpression.indexIn(text);               // То пытаемся найти начало комментария
    }
    while (startCommentIndex >= 0) {                                            // Если комментарий найден
                                                                                // Ищем конец комментария
        int endCommentIndex = commentEndExpression.indexIn(text, startCommentIndex);
        int commentLength;
        if (endCommentIndex == -1) {                                            // Если конец не найден
            setCurrentBlockState(Comment);                                      // То устанавливаем состояние Comment
            commentLength = text.length() - startCommentIndex;                  // Принцип аналогичен, что и для обычных тегов
        } else {
            commentLength = endCommentIndex - startCommentIndex + commentEndExpression.matchedLength();
        }
        setFormat(startCommentIndex, commentLength, multiLineCommentFormat);
        startCommentIndex = commentStartExpression.indexIn(text, startCommentIndex + commentLength);
    }
}

void HtmlHighLighter::onChangeAppearance()
{
    loadRules();
}
