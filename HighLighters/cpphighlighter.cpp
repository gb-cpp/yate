#include "cpphighlighter.h"

#include <QtDebug>
#include <QThreadPool>

#include "Style/appearance.h"
#include "Style/thememanager.h"

void CppHighLighter::highlightBlock(const QString &text) {
    highLighterRules(text);
    highLighterKeyWords(text);
    QRegularExpression ex_patrn("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(\"[^\"]*)|('[^']*)");
    QRegularExpressionMatchIterator ex_item = ex_patrn.globalMatch(text);
    while (ex_item.hasNext()) {
        QRegularExpressionMatch match_item = ex_item.next();
        setFormat(match_item.capturedStart(), match_item.capturedLength(), stringfrmt);
    }
    QRegularExpression com_exp("//");
    QRegularExpressionMatchIterator com_exp_item = com_exp.globalMatch(text);
    while (com_exp_item.hasNext()) {
        QRegularExpressionMatch match_com = com_exp_item.next();
        if(text.contains(QRegExp("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(\"[^\"]*)"))) {
            QRegularExpression quote_exp("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')");
            QRegularExpressionMatchIterator quote_item = quote_exp.globalMatch(text);
            while (quote_item.hasNext()) {
                QRegularExpressionMatch match_quote = quote_item.next();
                if(match_com.capturedStart() > match_quote.capturedStart()
                        && match_com.capturedStart() < match_quote.capturedStart() + match_quote.capturedLength())
                    continue;
                else
                    setFormat(match_com.capturedStart(), text.length() - match_com.capturedStart(), comfrmt);
            }
        } else {
            setFormat(match_com.capturedStart(), text.length() - match_com.capturedStart(), comfrmt);
        }
    }
    QRegularExpression prep_exp("#.*");
    QRegularExpressionMatchIterator prep_exp_item = prep_exp.globalMatch(text);
    while (prep_exp_item.hasNext()) {
        QRegularExpressionMatch match_prep = prep_exp_item.next();
        int endPos = text.indexOf(QRegularExpression("(/\\*)|(//)"));
        if(text.contains(QRegExp("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(\"[^\"]*)"))) {
            QRegularExpression quote_exp("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')");
            QRegularExpressionMatchIterator quote_item = quote_exp.globalMatch(text);
            while (quote_item.hasNext()) {
                QRegularExpressionMatch match_quote = quote_item.next();
                if(match_prep.capturedStart() > match_quote.capturedStart()
                        && match_prep.capturedStart() < match_quote.capturedStart() + match_quote.capturedLength()) continue;
                else setFormat(match_prep.capturedStart(), text.length() - match_prep.capturedStart(), preprocwsorfrmt);
            }
        } else {
            if(endPos == -1) setFormat(match_prep.capturedStart(), match_prep.capturedLength(), preprocwsorfrmt);
            else setFormat(match_prep.capturedStart(), endPos - match_prep.capturedStart(), preprocwsorfrmt);
        }
    }
    QRegularExpression startExpression("/\\*");
    QRegularExpression endExpression("\\*/");
    setCurrentBlockState(NormalState);
    int startIndex = 0;
    if (previousBlockState() != InsideStyleComment) {
        QRegularExpression ex("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(?:\"[^\"]*)");
        int quoteStart = -1;
        int quoteEnd = -1;
        startIndex = text.indexOf(startExpression);
        QRegularExpressionMatchIterator ex_item = ex.globalMatch(text);
        while (ex_item.hasNext()) {
            QRegularExpressionMatch match = ex_item.next();
            quoteStart = match.capturedStart();
            quoteEnd = match.capturedTexts().at(0).length() + match.capturedStart();
            if(startIndex > quoteStart && startIndex < quoteEnd)
                startIndex = text.indexOf(startExpression, startIndex + 1);
        }
        if(startIndex != -1 && quoteStart != -1 && startIndex > quoteStart && startIndex < quoteEnd) return;
        if(startIndex != -1 && text.indexOf("//") != -1 && startIndex>text.indexOf("//")) return;
    }
    while (startIndex >= 0) {
       QRegularExpressionMatch endMatch;
       int endIndex = text.indexOf(endExpression, startIndex, &endMatch);
       int commentLength;
       if (endIndex == -1) {
           setCurrentBlockState(InsideStyleComment);
           commentLength = text.length() - startIndex;
       } else {
           commentLength = endIndex - startIndex + endMatch.capturedLength();
       }
       setFormat(startIndex, commentLength, comBlockfrmt);
       startIndex = text.indexOf(startExpression, startIndex + commentLength);
    }
}

/* ============================================================================================================================= */

CppHighLighter::CppHighLighter(QTextDocument *parent) : HighLighter(parent) {
    options = new JsonClass(":/highlightning/highlights/cpp.json");
    setName(options->getNameExtension());
    loadRules();
    connect(Appearance::getInstance(), SIGNAL(onUpdate()), this, SLOT(onChangeAppearance()));
}

void CppHighLighter::loadRules()
{
    HighLighter::loadRules();

    const Theme * theme = Appearance::getInstance()->getThemeManager()->getCurrentTheme();

    comBlockfrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentBlockForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentBlockFont")
                );
    comfrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "CommentFont")
                );
    preprocwsorfrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "PreprocessorForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "PreprocessorFont")
                );
    stringfrmt = options->getTextCharFormat(
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "StringForeground"),
                theme->getValueBySelectorAndAttribute(Theme::THEME_HIGHLIGHTER, "StringFont")
                );
}

void CppHighLighter::onChangeAppearance()
{
    loadRules();
}
