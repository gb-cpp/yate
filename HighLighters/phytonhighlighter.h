#ifndef PHYTONHIGHLIGHTER_H
#define PHYTONHIGHLIGHTER_H

#include <QRegularExpression>

#include "highlighter.h"
#include "jsonclass.h"

class PhytonHighLighter : public HighLighter {
    Q_OBJECT
public:
    explicit PhytonHighLighter(QTextDocument *parent = nullptr);

    void loadRules();

private slots:
    void onChangeAppearance();

protected:
    void highlightBlock(const QString &text) override;

private:   
    QTextCharFormat defFuncfrmt;
    QTextCharFormat strFrmt;
    QTextCharFormat commFrmt;
    QTextCharFormat argFormat;
    QTextCharFormat escapeFrmt;
};

#endif // PHYTONHIGHLIGHTER_H
