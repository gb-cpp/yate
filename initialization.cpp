#include "initialization.h"

Initialization::Initialization() {
    mainWindow = new MainWindow();
    appearance = Appearance::getInstance(mainWindow);
	initHotkeys();
    splitter = new QSplitter;
    workspaceSplitter = new WorkspaceSplitter(Qt::Vertical);
    mdiArea = new QMdiArea();
    mainMenuBar = new MenuBar();
    mainMenuBar->setObjectName("mainMenuBar");
    butt = new QPushButton();
    butt->setObjectName("btnSplitForFileManager");
    butt->setFixedWidth(30);
    splitter->addWidget(butt);
    splitter->setCollapsible(0, false);
    butt->hide();
    connect(butt, &QPushButton::clicked, this, &Initialization::openCloseFileManager);
    connect(splitter, SIGNAL(splitterMoved(int,int)), this, SLOT(summonButt(int,int)));
}

/* ============================================================================================================================= */

MainWindow *Initialization::createMainWindow() {
    fileManager = new FileManager(mainWindow);                              // Создаём класс файлового менеджера
    fileManager->setMenuBar(mainMenuBar);                                   // Устанавливаем меню в файловый менеджер
    SettingsLoader * settingsLoader = SettingsLoader::getInstance();        // Добавляем класс настроек на основе файла ресурсов
    createMenu();                                                           // Создаём меню
    createMdiArea();                                                        // Создаём МДИ-интерфейс

	dragArea = new DragArea(mainWindow); //Drag & Drop
	dragArea->setResizeTo(workspaceSplitter);
	mainWindow->setDragArea(dragArea);

	connect(dragArea,SIGNAL(openFileByPath(QString, bool)),mainMenuBar,SLOT(openFileByPath(QString, bool)));
	connect(dragArea,SIGNAL(changeDir(QString)),fileManager,SLOT(slotChangeDir(QString)));

    if (settingsLoader != nullptr) {
        QString title = settingsLoader->getValue("caption").toString();

        mainWindow->setWindowTitle(title);
    }

    mainMenuBar->setAppearance();

    appearance->updateTheme();

    return mainWindow;
}

/* ============================================================================================================================= */

QSplitter *Initialization::getSplitter() const {
    return splitter;
}

/* ============================================================================================================================= */

WorkspaceSplitter *Initialization::getWorkspaceSplitter() const {
    return workspaceSplitter;
}

/* ============================================================================================================================= */

void Initialization::createMdiArea() {
    pMyGraphicsView = new graphicsView(workspaceSplitter);
    mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setViewMode(QMdiArea::ViewMode::TabbedView);
    mdiArea->setTabsClosable(true);
    mdiArea->setTabsMovable(true);
    QTabBar *tabBar = mdiArea->findChild<QTabBar *>();
    tabBar->setDrawBase(false);
    tabBar->setExpanding(false); // делаем табы по размеру зоголовка
    tabBar->setDocumentMode(true); //mac
    tabBar->setUsesScrollButtons(true); //mac
    connect(mdiArea, &QMdiArea::subWindowActivated, mainMenuBar, &MenuBar::activeWindowChanged);
    splitter->addWidget(fileManager);
    pMyGraphicsView->setVisible(true);
    mdiArea->setVisible(false);
    workspaceSplitter->addWidget(pMyGraphicsView);
    workspaceSplitter->addWidget(mdiArea);
	connect(mainMenuBar, SIGNAL(concealmenSignalWW()), this, SLOT(concealmentMdiArea()));
    connect(mainMenuBar->getFileOpen(), SIGNAL(concealmenSignalMA()), this, SLOT(concealmentWelcomeWidget()));
    splitter->addWidget(workspaceSplitter);
    mainWindow->setCentralWidget(splitter);
    mainWindow->setMainSplitter(splitter);
    mainWindow->setMdiArea(mdiArea);
    mainWindow->setWorkspaceSplitter(workspaceSplitter);
    mainWindow->setMenuBar(mainMenuBar);
}

/* ============================================================================================================================= */

void Initialization::createMenu() {
    mainMenuBar->setWorkspaceSplitter(workspaceSplitter);
	mainMenuBar->setMdiArea(mdiArea);
	}

void Initialization::initHotkeys()
	{
		HotkeysEditor::getInstance(mainWindow)->initHotkeyEditor();
		MenuBar();
		ImageViewer();
	}

/* ============================================================================================================================= */

void Initialization::concealmentMdiArea() {
    mdiArea->setVisible(false);
    pMyGraphicsView->setVisible(true);
}

/* ============================================================================================================================= */

void Initialization::concealmentWelcomeWidget() {
    pMyGraphicsView->setVisible(false);
    mdiArea->setVisible(true);
}

void Initialization::openCloseFileManager()
{
    fileManager->show();
    butt->hide();
    splitter->setSizes(QList<int>() << 30 << 240 << 600);
}

void Initialization::summonButt(int pos,int index) {
    if(pos == 0 && index == 2) {
        butt->show();
        fileManager->hide();
    }
}
