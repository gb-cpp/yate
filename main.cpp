#define Launch_TESTS //расскоментировать, чтобы начать тестирование
#ifdef Launch_TESTS
#include "Tests/test_autocompletion.h"
#include "Tests/test_funcconsole.h"
#include "Tests/test_hotkeyseditor.h"
#include "Tests/test_thememanager.h"
#endif
#include "initialization.h"



int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/images/appicon.png"));
#ifdef Launch_TESTS
    QTest::qExec(new Test_Autocompletion(), argc, argv); // Тесты автодополнения
	QTest::qExec(new Test_FuncConsole(), argc, argv); //Tесты консоли
	QTest::qExec(new Test_HotkeysEditor(), argc,argv);
	QTest::qExec(new Test_ThemeManager(), argc, argv); // Тесты темы
#endif
    // Инициализация работы текстового редактора
    Initialization initialization;
    initialization.createMainWindow()->show();
    return a.exec();
}
