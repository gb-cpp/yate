#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSplitter>
#include <QDebug>
#include <QMdiArea>
#include <QDesktopWidget>

#include "Style/appearance.h"
#include "Widgets/workspacesplitter.h"
#include "settingsloader.h"
#include "Widgets/dragarea.h"
class Appearance;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;
    void setMainSplitter(QSplitter *value);
    void setMdiArea(QMdiArea *value);
    void setWorkspaceSplitter(WorkspaceSplitter *workspaceSplitter);
    void setAppearance(Appearance *value);

    void moveToCenter(); // Первоначальное размещение по центру экрана

	void setDragArea(DragArea* value);

protected:
	void closeEvent(QCloseEvent *event) override;
private:
    QSplitter *mainSplitter;
    QMdiArea *mdiArea;
    Appearance *appearance;
    WorkspaceSplitter *workspaceSplitter;
	DragArea *dragArea;
};
#endif // MAINWINDOW_H
