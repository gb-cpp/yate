#ifndef FILEMODEL_H
#define FILEMODEL_H

#include <QFileSystemModel>
#include <QWidget>

class FileModel : public QFileSystemModel {
public:
    explicit FileModel(QWidget *parent = nullptr);

protected:
    bool hasChildren(const QModelIndex &parent = QModelIndex()) const override;
};

#endif // FILEMODEL_H
