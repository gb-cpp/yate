#include "filemodel.h"

FileModel::FileModel(QWidget *parent) : QFileSystemModel(parent) {
    setRootPath("");
    setReadOnly(false);
}

/* ============================================================================================================================= */

bool FileModel::hasChildren(const QModelIndex &parent) const {
    if (!parent.isValid()) return true;
    return QDir(filePath(parent)).count() > 2;
}
