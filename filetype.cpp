#include "filetype.h"

FileType::FileType(const QString &filePath)
    : mainType(eMainType::UNKNOWN)
    , subType(eSubType::UNKNOWN)
{
    QMimeType mime;
    if(!filePath.isEmpty())
        mime = mimeTypeForFile(filePath);
    else
        mime = mimeTypeForName("text/plain");

    if(mime.name().startsWith("image")){
        mainType = eMainType::IMAGE;
    }
    else if(mime.name().startsWith("text")){
        mainType = eMainType::TEXT;

        if(mime.name() == "text/x-c++src" || mime.name() == "text/x-c++hdr" || mime.name() == "text/x-chdr")
            subType = eSubType::CPP;
        else
            subType = eSubType::PLAIN;
    }
}

FileType::eMainType FileType::getMainType() const
{
    return mainType;
}

FileType::eSubType FileType::getSubType() const
{
    return subType;
}
