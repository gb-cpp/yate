#include "fileopen.h"
#include "filetype.h"

#include <QMessageBox>

FileOpen::FileOpen(QObject *parent) : QObject(parent) {
}

/* ============================================================================================================================= */

void FileOpen::setFilePath(const QString &filePath, bool openInHex) {

    if (bufferPtrs.empty()) {
        emit concealmenSignalMA();
    }

    FileType type(filePath);

    if(type.getMainType() == FileType::eMainType::IMAGE && !openInHex){
        ImageViewer *image = new ImageViewer;
        image->setImagePath(filePath);
        mdiArea->addSubWindow(image);
        image->setWindowTitle(QFileInfo(filePath).fileName());
        image->show();
    }
    else{
        Buffer *buffer = new Buffer(filePath);
        if (buffer->getFileSize() > 100000000) {
            QMessageBox::critical(mdiArea, tr("Error"), tr("Maximum file size 100Mb"));
            delete buffer;
            return;
        }
        bufferPtrs.push_front(buffer);

        if(type.getMainType() == FileType::eMainType::TEXT && !openInHex){
            TextEdit *txEdit = new TextEdit(appearance, buffer);
            mdiArea->addSubWindow(txEdit);

            switch(type.getSubType()){
            case FileType::eSubType::CPP:
                txEdit->VisibleAutocompleteCpp(true);
                break;
            case FileType::eSubType::PLAIN:
                txEdit->VisibleAutocompleteText(true);
                break;
            default:
                txEdit->VisibleAutocompleteOff(true);
            }

            connect(txEdit, &TextEdit::deleteBuffer, this, &FileOpen::deleteBuffer);
            txEdit->show();
        }
        else{
            HexEdit *hexEdit = new HexEdit(appearance, buffer);
            mdiArea->addSubWindow(hexEdit);
            connect(hexEdit, &HexEdit::deleteBuffer, this, &FileOpen::deleteBuffer);
            hexEdit->show();
        }
    }



    return;
}

/* ============================================================================================================================= */

void FileOpen::setMdiArea(QMdiArea *value) {
    mdiArea = value;
}

/* ============================================================================================================================= */

void FileOpen::setAppearance(Appearance *value) {
    appearance = value;
}

/* ============================================================================================================================= */

QVector<Buffer *> FileOpen::getContainerWithBuffers() const {
    return bufferPtrs;
	}

/* ============================================================================================================================= */

void FileOpen::deleteBuffer(Buffer *buffer) {
    qint16 pos =  bufferPtrs.indexOf(buffer);
    if (pos != -1) {
        delete buffer;
        bufferPtrs.remove(pos);
    } else {
        delete(buffer);
    }
}
