#ifndef FILEOPEN_H
#define FILEOPEN_H

#include <QObject>
#include <QMdiArea>

#include "Buffers/buffer.h"
#include "Style/appearance.h"
#include "Widgets/hotkeyseditor.h"
class Appearance;


class FileOpen : public QObject {
    Q_OBJECT

public:
    explicit FileOpen(QObject *parent = nullptr);
    void setFilePath(const QString &filePath, bool openInHex);
    void setMdiArea(QMdiArea *value);
    void setAppearance(Appearance *value);
    QVector<Buffer *> getContainerWithBuffers() const;

private:
	QMdiArea *mdiArea = nullptr;
	Appearance *appearance = nullptr;
    QVector<Buffer *> bufferPtrs;

private slots:
    void deleteBuffer(Buffer *);

signals:

    void concealmenSignalMA();
};

#endif // FILEOPEN_H
