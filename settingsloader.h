/*
Класс предназначен для чтения/записи переменных в файлы формата JSON
Для работы с классом необходимо объявить указатель типа SettingsLoader и инициализировать его значением статической
функции SettingsLoader::getInstance(QString). В качестве аргумента передать имя существующего файла формата JSON.

В процессе инициализации происходит проверка и чтение данных из файла JSON в объект QJsonObject для работы с данными.
Если JSON-файл невозможно открыть на чтение, генерируется исключение std::invalid_argument со значением <имя json-файла>.
Если данные из файла невозможно прочитать, генерируется исключение QJsonParseError. В случае генерации любого из
исключений, инициализации объявленного объекта не происходит.
при наличии возможности чтения данных из файла, функция SettingsLoader::getInstance(QString) создаёт элемент по имени
файла в статическом контейнере QMap, присваивает ему значение указателя на созданный объект SettingsLoader и возвращает
указатель на этот объект.

Для получения значения ключа, элемента массива или всего массива используется перегруженная функция:
    QJsonValue getValue(QString property);                  вариант для уникального имени ключа или массива
    QJsonValue getValue(QStringList jsonNameList);          вариант для неуникальных имен ключей

В случае использования метода для уникального имени ключа, класс находит первый попавшийся ключ с запрашиваемым
именем и возвращает его значение. Поиск прекращается.

Примеры использования:
  Аргументами функции могут быть уникальные имена ключей, массивов и элементы массивов.
    getValue("position-x").toInt() - получение значения ключа "position-x"
    getValue("array1[]").toArray(); - получение массива с именем "array1"
    getValue("array1[2]").toInt(); - получение значения 2-го элемента массива "array1"

  Аргументами функции в виде элементов QStringList могут быть имена секций, имена массивов, элементы массивов, имена ключей.
    Необходимо перечислять все имена секций и массивов последовательно, согласно иерархии вложенности.
    Количество вложений секций не ограничено.
    getValue({"main-window", "position-x"}).toInt() - получение значения ключа, хранящегося в секции "main-window".
    getValue({"main-window", "array1[]"}).toArray(); - получение массива с именем "array1"
    getValue({"main-window", "array1[2]"}).toInt(); - получение значения 2-го элемента массива "array1"

Для записи значений, массивов и элементов массива используется перегруженная функция:
    (работа м методами сохранения аналогична методам получения значений)

    setValue("position-x", value);  - сохранение значения value в JSON объект c уникальным именем "position-x"
    setValue({"main-window", "position-x"}, value);  - сохранение значения value в JSON объект "position-x",
                                                       находящийся в секции "main-window"
    setValue({"main-window", "array1[2]"}, value);

Сохранение объекта JSON в файл, ассоциированный в QMap с указателем на данный объект выполняется вызовом метода
    saveJsonToFile(). Если возникает ошибка открытия на запись JSON-файла, функция генерирует исключение
    std::runtime_error со значением <имя json-файла>
*/

#ifndef SETTINGSLOADER_H
#define SETTINGSLOADER_H

#include <QJsonObject>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonArray>
#include <QMap>

class SettingsLoader {
public:
    ~SettingsLoader();
    QJsonValue getValue(QString property) const;
    QJsonValue getValue(QStringList jsonNameList) const;
    bool setValue(QString property, QJsonValue value);
    bool setValue(QStringList jsonNameList, QJsonValue value);
	void saveJsonToFile(QString filename = SETTINGS_FILENAME) const;
    static SettingsLoader *getInstance();

    QJsonObject getRawObject() {
        return jsonObject;
    }

	SettingsLoader(QString filename);

private:
    static SettingsLoader * instance;
    const static QString SETTINGS_FILENAME;
    SettingsLoader();
    SettingsLoader(const SettingsLoader &) = delete;
    SettingsLoader& operator=(const SettingsLoader &) = delete;

	void getDefaultSettingsFileContents(QString filename = SETTINGS_FILENAME);                                          // Загрузка файла JSON из ресурсов
	void getSettingsFileContents(QString filename = SETTINGS_FILENAME);                                                 // Загрузка файла JSON из файловой системы

    QJsonValue getValueRecursion(const QJsonObject, const QString &, const int &) const;
    QJsonValue getArrayRecursion(const QJsonArray, const QString &, const int &, bool) const;
    QJsonValue getValueRecursion(const QJsonObject, const QStringList) const;
    bool setValueRecursion(QJsonObject &, const QString &, const int &, const QJsonValue &);
    bool setArrayRecursion(QJsonArray &, const QString &, const int &, const QJsonValue &, bool);
    bool setValueRecursion(QJsonObject &, const QStringList, const QJsonValue &);


    QJsonObject jsonObject;                                                         // Объект JSON для работы с данными
};

#endif // SETTINGSLOADER_H
