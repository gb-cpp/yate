#ifndef MOUSEWHEELFILTER_H
#define MOUSEWHEELFILTER_H

#include <QObject>
#include <QEvent>

class MouseWheelFilter : public QObject
{
    Q_OBJECT
public:
    bool eventFilter(QObject *obj, QEvent *event) override;

signals:

};

#endif // MOUSEWHEELFILTER_H
