#ifndef FILETYPE_H
#define FILETYPE_H

#include <QMimeDatabase>

class FileType : public QMimeDatabase
{

public:
    FileType(const QString &);

    enum class eMainType {UNKNOWN, IMAGE, TEXT};
    enum class eSubType {UNKNOWN, CPP, PLAIN};

    eMainType getMainType() const;
    eSubType getSubType() const;

private:
    eMainType mainType;
    eSubType subType;
};

#endif // FILETYPE_H
