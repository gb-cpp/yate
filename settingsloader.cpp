#include "settingsloader.h"

SettingsLoader * SettingsLoader::instance = nullptr;
const QString SettingsLoader::SETTINGS_FILENAME = "settings.json";

SettingsLoader::SettingsLoader() {

    QFileInfo fileInfo(SETTINGS_FILENAME);

    QFile file(":/settings/" + fileInfo.fileName());

    if (file.open(QIODevice::ReadOnly)) {                                           // Если данный файл есть в ресурсах
        file.close();
		getDefaultSettingsFileContents(SETTINGS_FILENAME);                                           // То загрузить настройки из файла ресурсов
    }

	getSettingsFileContents(SETTINGS_FILENAME);
}

SettingsLoader::SettingsLoader(QString filename)
{
		if(filename.isEmpty()) filename = SETTINGS_FILENAME;
    QFileInfo fileInfo(filename);

    QFile file(fileInfo.fileName());

    if (file.open(QIODevice::ReadOnly)) {                                           // Если данный файл есть в ресурсах
        file.close();
		getDefaultSettingsFileContents(filename);                                           // То загрузить настройки из файла ресурсов
    }

	getSettingsFileContents(filename);
}

/* ============================================================================================================================= */

SettingsLoader::~SettingsLoader() {}

/* ============================================================================================================================= */

void SettingsLoader::getDefaultSettingsFileContents(QString filename) {
	QFileInfo fileInfo(filename);

    QFile file(":/settings/" + fileInfo.fileName());

    if (file.open(QIODevice::ReadOnly)) {
        QString set = file.readAll();
        jsonObject = QJsonDocument::fromJson(set.toUtf8()).object();
        file.close();
    }
}

/* ============================================================================================================================= */

void SettingsLoader::getSettingsFileContents(QString filename) {
	QFile file(filename);

    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QString set = file.readAll();
        QJsonParseError jsonError;
        QJsonDocument jsonDoc = QJsonDocument::fromJson(set.toUtf8(), &jsonError);
        if (!jsonDoc.isNull())                                                      // Нет ошибок чтения данных
            jsonObject = QJsonDocument::fromJson(set.toUtf8()).object();
        file.close();
    }
}

/* ============================================================================================================================= */

SettingsLoader *SettingsLoader::getInstance() {
    if(instance == nullptr)
        instance = new SettingsLoader;

    return instance;
}

/* ============================================================================================================================= */

QJsonValue SettingsLoader::getValue(QString property) const {
    int index = -1;                                                                 // Индекс, на случай получения из массива
                                                                                    // "-1" - получаемое значение не из массива
                                                                                    // "-2" - получение массива целиком
    int b = property.indexOf("[");
    int e = property.indexOf("]");
        if (e - b > 1) {                                                            // Запрошен элемент массива
        index = property.mid(b + 1, e - b - 1).toInt();                             // Чтение индекса между [ ]
        property.remove(b, e - b + 1);                                              // Удаление из property [ ]
    } else if (e - b == 1) {
        index = -2;
        property.remove(b, e - b + 1);                                              // Удаление из property [ ]
    }
    QJsonValue value = getValueRecursion(jsonObject, property, index);
    return  value;
}

/* ============================================================================================================================= */

QJsonValue SettingsLoader::getValueRecursion(const QJsonObject jsonRecursion,
                                             const QString &property,
                                             const int &index) const {
    QJsonValue value = QJsonValue::Null;
    QJsonObject::const_iterator it;
    for (it = jsonRecursion.begin(); it != jsonRecursion.end(); ++it) {             // Перебор содержимого объекта json
        if (it->isObject()) {
            value = getValueRecursion(it->toObject(), property, index);             // Рекурентный вызов и обработка в нём секции
            if (value != QJsonValue::Null) break;
        } else if (it->isArray()) {
            if (it.key() == property) {                                             // Если имя массива совпадает с property
                if (index > -1) {
                    value = getArrayRecursion(it->toArray(), property, index, true);// Вызов функции обработки массива
                } else if (index == -2) {
                    value = it.value();                                             // Получение всего массива
                }
            } else {
                value = getArrayRecursion(it->toArray(), property, index, false);
            }
            if (value != QJsonValue::Null) {                                        // Если было считано или отсутствует значение
                break;                                                              // Выход из цикла
            }
        } else if (it.key() == property &&                                          // Если встретился ключ с именени property
                   index == -1) {                                                   // ...и property — не имя массива
            value = it.value();                                                     // Получение всего массива
            break;                                                                  // Выход из цикла
        }
    }
    return value;
}

/* ============================================================================================================================= */

QJsonValue SettingsLoader::getArrayRecursion(const QJsonArray jsonArray,
                                             const QString &property,
                                             const int &index,
                                             bool propertyArray) const {
    QJsonValue value = QJsonValue::Null;
    if (propertyArray && index > -1) {                                              // Если у текущего массива имя равно property и надо получить элемент массива
        value = jsonArray[index];                                                   // Возврат значения элемента массива или QJsonValue::Undefined в случае отсутствия элемента массива
        propertyArray = false;                                                      // После считывания значения нужного элемента массива сброс флага равенства имени массива и property
    } else {                                                                        // У текущего массива другое имя. Необходим поиск среди его элементов
        for (int i = 0; i < jsonArray.size(); ++i) {
            if (jsonArray[i].isObject())                                            // Если текущий элемент массива является объектом json
                value = getValueRecursion(jsonArray[i].toObject(), property, index);// Вызов функции обработки объекта
            if (value != QJsonValue::Null) break;                                   // Если было считано или отсутствует значение, выход из цикла
        }
    }
    return value;
}

/* ============================================================================================================================= */

QJsonValue SettingsLoader::getValue(QStringList jsonNameList) const {
    return getValueRecursion(jsonObject, jsonNameList);
}

/* ============================================================================================================================= */

QJsonValue SettingsLoader::getValueRecursion(const QJsonObject jsonRecursion,
                                             const QStringList jsonNameList) const {
    QJsonValue value;
    QString jsonName;
    int index = -1;
    int b = jsonNameList[0].indexOf("[");
    int e = jsonNameList[0].indexOf("]");
    if (b == -1) {                                                                  // Объект, не массив
        jsonName = jsonNameList[0];
    } else if (e - b > 1) {                                                         // Запрошен элемент массива
        index = jsonNameList[0].mid(b + 1, e - b - 1).toInt();                      // Чтение индекса между [ ]
        jsonName = jsonNameList[0].mid(0, b);
    } else if (e - b == 1) {                                                        // Массив целиком
        index = -2;
        jsonName = jsonNameList[0].mid(0, b);
    }
    if (jsonNameList.size() > 1) {                                                  // Не достигнут низ иерархии
        if (index == -1) {                                                          // Текущий элемент — не элемент массива.
            QJsonObject jsonSubObject = jsonRecursion.value(jsonName).toObject();
            value = getValueRecursion(jsonSubObject,                                // Преобазование в объект для обработки
                                      jsonNameList.mid(1, jsonNameList.size() - 1));
        } else if (index > -1) {                                                    // Текущий элемент списка — элемент массива.
            QJsonObject jsonSubObject = jsonRecursion.value(jsonName)               // Получение значения объекта,
                                                     .toArray()[index]              // ...преобразование  в массив, получение
                                                     .toObject();                   // ...и преобразование в объект
            value = getValueRecursion(jsonSubObject,
                                      jsonNameList.mid(1, jsonNameList.size() - 1));
        } else if (index == -2) {                                                   // Запрошен массив без конкретного индекса
            value = QJsonValue::Undefined;                                          // Поиск вниз по иерархии невозможен
        }
    } else {                                                                        // Низ иерархии
        if (index > -1) {
            value = jsonRecursion.value(jsonName).toArray()[index];
        } else {
            value = jsonRecursion.value(jsonName);
        }
    }
    return value;
}

/* ============================================================================================================================= */

bool SettingsLoader::setValue(QString property, QJsonValue value) {
    int index = -1;                                                     // Индекс, на случай получения значения из массива.
                                                                        // "-1" - получаемое значение не из массива
                                                                        // "-2" - получение массива целиком
    int b = property.indexOf("[");
    int e = property.indexOf("]");
    if (e - b > 1) {                                                    // Запрошен элемент массива
        index = property.mid(b + 1, e - b - 1).toInt();                 // Чтение индекса между [ ]
        property.remove(b, e - b + 1);                                  // Удаление из property [ ]
    } else if (e - b == 1) {
        index = -2;
        property.remove(b, e - b + 1);                                  // Удаление из property [ ]
    }
    return setValueRecursion(jsonObject, property, index, value);
}

/* ============================================================================================================================= */

bool SettingsLoader::setValueRecursion(QJsonObject &jsonRecursion,
                                       const QString &property,
                                       const int &index,
                                       const QJsonValue &value) {
    bool valueChanged = false;                                          // Флаг изменённого значения
    QJsonObject::iterator it;
    for (it = jsonRecursion.begin(); it != jsonRecursion.end(); ++it) {
        if (it->isObject()) {                                           // Если встретился объект
            QJsonObject jsonSection = it->toObject();                   // Получение секции и преобразовании её в объект
            valueChanged = setValueRecursion(jsonSection,               // Рекурентный вызов и обработка в нём секции
                                             property,
                                             index,
                                             value);
            if (valueChanged) {
                *it = jsonSection;
                break;
            }
        } else if (it->isArray()) {                                     // Если встретился массив
            if (index > -1) {
                QJsonArray jsonArray = it->toArray();
                if (it.key() == property) {                             // Если имя массива совпадает со значением property
                    valueChanged = setArrayRecursion(jsonArray,
                                                     property,
                                                     index,
                                                     value,
                                                     true);
                } else {
                    valueChanged = setArrayRecursion(jsonArray,
                                                     property,
                                                     index,
                                                     value,
                                                     false);
                }
                if (valueChanged) {
                    break;
                }
            } else if (index == -2 && it.key() == property) {
                it.value() = value;
                valueChanged = true;
            }
        } else if (it.key() == property && index < 0) {                 // Если ключ с именени property и — не имя массива
            it.value() = value;
            valueChanged = true;
            break;
        }
    }
    return valueChanged;
}

/* ============================================================================================================================= */

bool SettingsLoader::setArrayRecursion(QJsonArray &jsonArray,
                                       const QString &property,
                                       const int &index,
                                       const QJsonValue &value,
                                       bool propertyArray) {
    bool valueChanged = false;
    if (propertyArray && index > -1) {                                  // Если имя текущего массива совпадает с property
        jsonArray[index] = value;                                       // Запись нового значения
        valueChanged = true;                                            // Флаг записи
        propertyArray = false;
    } else {                                                            // У текущего массива другое имя
        for (int i = 0; i < jsonArray.size(); ++i) {                    // Необходим поиск среди его элементов
            if (jsonArray[i].isObject()) {
                QJsonObject objectElement = jsonArray[i].toObject();    // Получение объекта из элемента массива
                valueChanged = setValueRecursion(objectElement,         // Вызов основной функции рекурсии
                                                 property,
                                                 index,
                                                 value);
                if (valueChanged) {                                     // Если в objectElement изменено значение
                    jsonArray[i] = objectElement;                       // Запись объекта в элемент массива
                    break;
                }
            }
        }
    }
    return valueChanged;
}

/* ============================================================================================================================= */

bool SettingsLoader::setValue(QStringList jsonNameList, QJsonValue value) {
    return setValueRecursion(jsonObject, jsonNameList, value);
}

/* ============================================================================================================================= */

bool SettingsLoader::setValueRecursion(QJsonObject &jsonRecursion,
                                       const QStringList jsonNameList,
                                       const QJsonValue &value) {
    QString jsonName;
    bool valueChanged = false;
    int index = -1;
    int b = jsonNameList[0].indexOf("[");
    int e = jsonNameList[0].indexOf("]");
    if (b == -1) {                                                      // Объект, не массив
        jsonName = jsonNameList[0];
    } else if (e - b > 1) {                                             // Запрошен элемент массива
        index = jsonNameList[0].mid(b + 1, e - b - 1).toInt();          // Чтение индекса между [ ]
        jsonName = jsonNameList[0].mid(0, b);
    } else if (e - b == 1) {                                            // Массив целиком
        index = -2;
        jsonName = jsonNameList[0].mid(0, b);
    }
    if (jsonNameList.size() > 1) {                                      // Не достигнут низ иерархии
        if (index == -1) {                                              // Текущий элемент списка имён — не элемент массива
            QJsonObject jsonSubObject = jsonRecursion[jsonName].toObject();     // Преобазование его в объект для дальнейшей обработки
            valueChanged = setValueRecursion(jsonSubObject, jsonNameList.mid(1, jsonNameList.size() - 1), value);
            if (valueChanged) jsonRecursion[jsonName] = jsonSubObject;
        } else if (index > -1) {                                                // Текущий элемент списка имён — элемент массива. Преобазование его элемента с индексом в объект для дальнейшей обработки
            QJsonArray jsonArray = jsonRecursion.value(jsonName).toArray();     // Получение значения объекта, преобразование его в массив
            QJsonObject jsonSubObject = jsonArray[index].toObject();            // Получение элемента массива и преобразование его в объект
            valueChanged = setValueRecursion(jsonSubObject, jsonNameList.mid(1, jsonNameList.size() - 1), value);
            if (valueChanged) {
                jsonArray[index] = jsonSubObject;
                jsonRecursion[jsonName] = jsonArray;
            }
        }
    } else {                                                            // Низ иерархии
        if (index > -1) {
            QJsonArray jsonArray = jsonRecursion.value(jsonName).toArray();
            jsonArray[index] = value;
            jsonRecursion[jsonName] = jsonArray;
            valueChanged = true;
        } else {
            jsonRecursion[jsonName] = value;
            valueChanged = true;
        }
    }
    return valueChanged;
}

/* Сохранить Json в файл ======================================================================================================= */

void SettingsLoader::saveJsonToFile(QString filename) const {
		if(filename.isEmpty()) filename = SETTINGS_FILENAME;
	QFile file(filename);

    if (file.open(QIODevice::WriteOnly)) {
        QJsonDocument set;
        set.setObject(jsonObject);
        file.write(set.toJson());
        file.close();
    } else {
		std::runtime_error saveFileError(filename.toUtf8());
        throw saveFileError;                                            // Исключение по ошибке открытия файла на запись
    }
}
