QT       += core gui printsupport testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 debug
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Buffers/buffer.cpp \
    Filters/mousewheelfilter.cpp \
    Models/filemodel.cpp \
    Style/EditorWidgets/colorwidget.cpp \
    Style/EditorWidgets/comboboxtypewidget.cpp \
    Style/EditorWidgets/comboboxwidget.cpp \
    Style/EditorWidgets/editorwidgetfactory.cpp \
    Style/EditorWidgets/fontfamilywidget.cpp \
    Style/appearance.cpp \
    Style/theme.cpp \
    Style/thememanager.cpp \
    Style/themeparser.cpp \
    Tests/test_funcconsole.cpp \
    Tests/test_hotkeyseditor.cpp \
    Tests/test_autocompletion.cpp \
    Tests/test_thememanager.cpp \
    HighLighters/brackets.cpp \
    HighLighters/cpphighlighter.cpp \
    HighLighters/highlighter.cpp \
    HighLighters/htmlhighlighter.cpp \
    HighLighters/jsonclass.cpp \
    HighLighters/phytonhighlighter.cpp \
    Tests/test_themeparser.cpp \
    Widgets/Welcome/graphicsitem.cpp \
    Widgets/Welcome/graphicsview.cpp \
    Widgets/dragarea.cpp \
    Widgets/hotkeyseditor.cpp \
    Widgets/nixconsole.cpp \
    Widgets/winconsole.cpp \
    Widgets/filemanager.cpp \
    Widgets/hexedit.cpp \
    Widgets/imageviewer.cpp \
    Widgets/menubar.cpp \
    Widgets/textedit.cpp \
    Widgets/widgetfoundation.cpp \
    Widgets/workspacesplitter.cpp \
    fileopen.cpp \
    filetype.cpp \
    initialization.cpp \
    main.cpp \
    mainwindow.cpp \
    settingsloader.cpp \

HEADERS += \
    Buffers/buffer.h \
    Filters/mousewheelfilter.h \
    Models/filemodel.h \
    Style/EditorWidgets/colorwidget.h \
    Style/EditorWidgets/comboboxtypewidget.h \
    Style/EditorWidgets/comboboxwidget.h \
    Style/EditorWidgets/editorwidgetfactory.h \
    Style/EditorWidgets/fontfamilywidget.h \
    Style/appearance.h \
    Style/theme.h \
    Style/thememanager.h \
    Style/themeparser.h \
    Tests/test_autocompletion.h \
    Tests/test_funcconsole.h \
    Tests/test_hotkeyseditor.h \
    HighLighters/brackets.h \
    HighLighters/cpphighlighter.h \
    HighLighters/highlighter.h \
    HighLighters/htmlhighlighter.h \
    HighLighters/jsonclass.h \
    HighLighters/phytonhighlighter.h \
    Tests/test_thememanager.h \
    Tests/test_themeparser.h \
    Widgets/Welcome/graphicsitem.h \
    Widgets/Welcome/graphicsview.h \
    Widgets/dragarea.h \
    Widgets/hotkeyseditor.h \
    Widgets/nixconsole.h \
    Widgets/winconsole.h \
    Widgets/filemanager.h \
    Widgets/hexedit.h \
    Widgets/imageviewer.h \
    Widgets/menubar.h \
    Widgets/textedit.h \
    Widgets/widgetfoundation.h \
    Widgets/workspacesplitter.h \
    fileopen.h \
    filetype.h \
    initialization.h \
    mainwindow.h \
    settingsloader.h \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Resources/resources.qrc \
    Resources/welcome.qrc \
    Resources/res.qrc

FORMS += \
    Style/appearance.ui \
    Widgets/hotkeyseditor.ui
