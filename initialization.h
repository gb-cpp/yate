#ifndef INITIALIZATION_H
#define INITIALIZATION_H

#include <QObject>
#include <QMdiArea>
#include <QSplitter>
#include <QMessageBox>
#include <QMenu>
#include <QMenuBar>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>

#include <stdexcept>

#include "Style/appearance.h"
#include "Widgets/Welcome/graphicsview.h"
#include "Widgets/filemanager.h"
#include "Widgets/menubar.h"
#include "Widgets/workspacesplitter.h"
#include "mainwindow.h"
#include "settingsloader.h"
#include "Widgets/hotkeyseditor.h"
class Initialization : public QObject {
    Q_OBJECT

public:
    Initialization();
    MainWindow *createMainWindow();
    QSplitter *getSplitter() const;
    WorkspaceSplitter *getWorkspaceSplitter() const;

private:
    MainWindow *mainWindow;
    QSplitter *splitter;
    WorkspaceSplitter *workspaceSplitter;
    MenuBar *mainMenuBar;
    QMdiArea *mdiArea;
    Appearance *appearance;
    FileManager *fileManager;
    graphicsView *pMyGraphicsView;
    void createMdiArea();
    void createMenu();
	void initHotkeys();
    QPushButton *butt;
	DragArea *dragArea;
private slots:
        void openCloseFileManager();
        void summonButt(int pos,int index);

public slots:
    void concealmentMdiArea();
    void concealmentWelcomeWidget();
};

#endif // INITIALIZATION_H
