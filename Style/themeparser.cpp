#include "themeparser.h"

ThemeParser::ThemeParser(): position(0), themeString(""){}

void ThemeParser::reset()
{
    position = 0;
    this->isValid = true;
    this->selectors.clear();
}

Theme * ThemeParser::parse(QString themeString)
{
    this->themeString = themeString;
    reset();
    parse();

    return new Theme(selectors, isValid);
}

void ThemeParser::parse()
{
    QString selector;

    isValid = true;

    try {
        while(!isEnd()) {

            eatNonValuableCharacters();

            if(isEnd() || isLeftCurlyBrace()) position++;

            selector = parseSelector();

            selectors[selector] = parseSelectorAttributes();

            if(isEnd() || isRightCurlyBrace()) position++;

            eatNonValuableCharacters();
        }
    }  catch (...) {
        isValid = false;
    }
}

QString ThemeParser::parseSelector()
{
    QString selector = "";

    while(!isEnd() && !isLeftCurlyBrace()){

        eatComment();

        if(isEnd())
            return selector;

        selector += themeString.at(position++);
    }

    if(!isEnd() && isLeftCurlyBrace()) position++;

    return selector.trimmed();
}

std::map<QString, QString> ThemeParser::parseSelectorAttributes()
{
    std::map<QString, QString> attributes;

    QString attribute;
    QString value;

    while(!isEnd() && !isRightCurlyBrace()) {

        attribute = parseAttribute().trimmed();

        value = parseValue();

        attributes[attribute] = value;

        eatNonValuableCharacters();
    }

    return attributes;
}

QString ThemeParser::parseAttribute()
{
    QString attribute = "";

    while(!isEnd() && !isColon()) {

        eatNonValuableCharacters();

        if(!isEnd() && isColon()) break;

        attribute += themeString.at(position++);
    }

    if(!isEnd() && isColon())
        position++;

    return attribute;
}

QString ThemeParser::parseValue()
{
    QString value = "";

    eatNonValuableCharacters();
    while(!isEnd() && !isSemicolumn()) {

        if(isEnd() || isSemicolumn()) break;

        value += themeString.at(position++);
    }

    if(isSemicolumn())
        position++;

    return value;
}

void ThemeParser::eatNonValuableCharacters()
{
    eatSpace();
    while(!isEnd() && isCommentStart()) {
        eatComment();
        eatSpace();
    }
}

void ThemeParser::eatComment()
{
    if(!isEnd() && isCommentStart()) {

        while(!isEnd() && !isCommentEnd())
            position++;

        if(isEnd())
            return;

        if(isCommentEnd())
            position += 2;
    }
}

void ThemeParser::eatSpace()
{
    while(!isEnd() && isSpace())
        position++;
}

QString ThemeParser::peekNext()
{
    if(position+1 < themeString.length())
        return themeString.at(position+1);

    return "";
}

bool ThemeParser::isSpace()
{
    return themeString.at(position).isSpace();
}

bool ThemeParser::isCommentStart()
{
    return isSlash() && peekNext() == "*";
}

bool ThemeParser::isCommentEnd()
{
    return isStar() && peekNext() == "/";
}

bool ThemeParser::isSlash()
{
    return themeString.at(position) == "/";
}

bool ThemeParser::isStar()
{
    return themeString.at(position) == "*";
}

bool ThemeParser::isSemicolumn()
{
    return themeString.at(position) == ";";
}

bool ThemeParser::isColon()
{
    return themeString.at(position) == ":";
}

bool ThemeParser::isLeftCurlyBrace()
{
    return themeString.at(position) == "{";
}

bool ThemeParser::isRightCurlyBrace()
{
    return themeString.at(position) == "}";
}

bool ThemeParser::isLetterOrNumber()
{
    return themeString.at(position).isLetterOrNumber();
}

bool ThemeParser::isEnd()
{
    return !(position < themeString.length());
}

