#ifndef THEMEPARSER_H
#define THEMEPARSER_H

#include <QString>
#include <map>

#include "theme.h"

class ThemeParser
{
public:
    ThemeParser();

    Theme * parse(QString themeString);

private:

    bool isValid;
    long long position;
    QString themeString;
    std::map<QString, std::map<QString, QString>> selectors;

    void parse();

    QString parseSelector();
    std::map<QString, QString> parseSelectorAttributes();
    QString parseAttribute();
    QString parseValue();

    bool isEnd();
    bool isLetterOrNumber();
    bool isSemicolumn();
    bool isColon();
    bool isLeftCurlyBrace();
    bool isRightCurlyBrace();
    bool isSlash();
    bool isStar();
    bool isSpace();

    bool isCommentStart();
    bool isCommentEnd();

    void eatComment();
    void eatNonValuableCharacters();
    void eatSpace();
    void printState(QString additionalinfo = "");
    QString peekNext();
    void reset();
    QString normalize(const QString &str) const;
};

#endif // THEMEPARSER_H
