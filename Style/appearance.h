#ifndef APPEARANCE_H
#define APPEARANCE_H

#include <QDialog>
#include <QDebug>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QFontComboBox>
#include <QComboBox>
#include <QColorDialog>
#include <QComboBox>

#include <map>
#include <functional>
#include <vector>

#include "Widgets/Welcome/graphicsview.h"
#include "Widgets/menubar.h"
#include "Widgets/textedit.h"
#include "mainwindow.h"
#include "thememanager.h"
#include "ui_appearance.h"
#include "EditorWidgets/editorwidgetfactory.h"

class MainWindow;
class MenuBar;

namespace Ui {
class Appearance;
}

/* Это диалоговое окно отвечает за внешний вид приложения.
 * Автор: Савелий Никулин
 * GitLab/GitHub: @SteveMoore18
 */

/* Класс Appearance ============================================================================================================ */

class Appearance : public QDialog {
    Q_OBJECT

private slots:
    void on_cbUIStyles_activated(const QString &arg1);
    void on_btnAddNewStyle_clicked();
    void on_btnRemoveStyle_clicked();
    void on_btnEditStyle_clicked();
    void on_btnRefresh_clicked();
    void on_btnImport_clicked();
    void on_btnSave_clicked();
    void on_btnClone_clicked();

private:
    Ui::Appearance *ui;

    QWidget * mainWindow;
    QMap<QString,QString> themesDictionary;
    ThemeManager themeManager;
    QColorDialog colorDialog;

    void enableCustomMode(const bool status);

    explicit Appearance(QWidget *parent);

    void setSettingsTable();
    void setSettingsHeadings();
    void addSettingsRow(int & row, QJsonObject & element);
    void updateSettingsTable();

    void warningMessage(QString message);

    void setThemesNames();
    void setThemesNamesAndSettings();
    void updateThemesNamesAndSettings();

    static Appearance * instance;

signals:
    void onUpdate(const Appearance * appearance);
    void onUpdate();
    void onUpdated(const Appearance * appearance);
    void onUpdated();

public:

    static Appearance * getInstance(QWidget *mainWindow = nullptr);

    void setThemeSettings();
    void setThemeAttributes(const QJsonObject &element, QString &value);

    Appearance(Appearance const&) = delete;
    void operator=(Appearance const&) = delete;
    ~Appearance();

    const ThemeManager * getThemeManager();

    void saveData();

    void updateTheme();
    bool isCustomMode();
};

#endif // APPEARANCE_H
