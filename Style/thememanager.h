#ifndef THEMEMANAGER_H
#define THEMEMANAGER_H

#include "themeparser.h"
#include "theme.h"
#include <QObject>
#include <map>
#include <vector>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QFont>
#include <QJsonDocument>
#include <QJsonArray>

class ThemeManager
{
public:
    ThemeManager();
    ~ThemeManager();

    static const QString DEFAULT_THEME_LIGHT;
    static const QString DEFAULT_THEME_DARK;

    static const QString THEME_EDITABLE_ATTRIBUTES;

    static const QString DEFAULT_THEME_DARK_NAME;
    static const QString DEFAULT_THEME_LIGHT_NAME;

    static const QString DEFAULT_THEME_TEMPLATE;
    static const QString DEFAULT_THEME_TEMPLATE_NAME_PLACEHOLDER;

    static const QString THEME_LINE_NUMBER;

    static const QString STYLES_PATH;

    static const std::map<QString, QString> DEFAULT_THEMES;

    static bool isDefaultTheme(const QString &);
    bool isDefaultTheme();

    void reload();
    void load(const QString &);
    void remove();

    QString getFilename() const;
    QString getName() const;
    QString getRawData() const;
    QString getAttribute(const QString &, const QString &) const;
    void setAttribute(const QString &, const QString &, const QString &);

    std::map<QString,QString> themesDictionary;

    Theme *getCurrentTheme() const;

    QJsonArray getEditableAttributes();

    void createTheme(QString themeName);
    void importTheme(QString path);
    void cloneTheme();
    QString saveTheme();

    const std::map<QString, QString> *getThemes() const;
private:

    QJsonDocument editableAttributes;

    ThemeParser themeParser;
    Theme * currentTheme;
    Theme * defaultTheme;

    QString filename;
    QString themeString;

    void parse();

    void setEditableAttributes();

    QString readFile(const QString & filename);

    bool isAttributeExistsInTheme(const std::map <QString, std::map<QString, QString>> &, const QString &, const QString & ) const;
    QString getRelativePathToStyles(const QString &filename);
    QString getRelativePathToStyles();
    QString getAbsolutePathToStyles(const QString &filename);
    QString getAbsolutePathToStyles();
    void loadDefaultThemes();
    void loadCustomThemes();
    QString fixedThemeName(const Theme *);
    QString fixedThemeName(QString themeName);
    QString saveTheme(Theme *theme);
    void deleteCurrentTheme();
    void setTheme(Theme *theme);
};

#endif // THEMEMANAGER_H
