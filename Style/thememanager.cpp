#include "thememanager.h"

const QString ThemeManager::STYLES_PATH = "user_styles/";

const QString ThemeManager::DEFAULT_THEME_TEMPLATE = ":/styles/light.css";

const QString ThemeManager::DEFAULT_THEME_LIGHT = ":/styles/light.css";
const QString ThemeManager::DEFAULT_THEME_DARK = ":/styles/dark.css";

const QString ThemeManager::DEFAULT_THEME_DARK_NAME = "dark-standard";
const QString ThemeManager::DEFAULT_THEME_LIGHT_NAME = "light-standard";

const QString ThemeManager::THEME_EDITABLE_ATTRIBUTES = ":/styles/editableattributes.json";

const std::map<QString,QString> ThemeManager::DEFAULT_THEMES {
    {ThemeManager::DEFAULT_THEME_DARK_NAME, ThemeManager::DEFAULT_THEME_DARK},
    {ThemeManager::DEFAULT_THEME_LIGHT_NAME, ThemeManager::DEFAULT_THEME_LIGHT}
};

ThemeManager::ThemeManager(): currentTheme(nullptr), defaultTheme(nullptr)
{
    QDir().mkdir(STYLES_PATH);

    setEditableAttributes();

    load(DEFAULT_THEME_LIGHT);

    defaultTheme = currentTheme;

    loadDefaultThemes();

    loadCustomThemes();
}

ThemeManager::~ThemeManager()
{
    if(currentTheme != nullptr) delete currentTheme;
    if(defaultTheme != currentTheme) delete defaultTheme;
}

void ThemeManager::setEditableAttributes()
{
    QFile file(ThemeManager::THEME_EDITABLE_ATTRIBUTES);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString editableAttributesRaw = file.readAll();
    file.close();

    editableAttributes = QJsonDocument::fromJson(editableAttributesRaw.toUtf8());
}

QJsonArray ThemeManager::getEditableAttributes()
{
    return editableAttributes.array();
}

bool ThemeManager::isDefaultTheme(const QString & themeName)
{
    return ThemeManager::DEFAULT_THEMES.find(themeName) != ThemeManager::DEFAULT_THEMES.end();
}

bool ThemeManager::isDefaultTheme()
{
    return ThemeManager::DEFAULT_THEMES.find(currentTheme->getName()) != ThemeManager::DEFAULT_THEMES.end();
}

void ThemeManager::reload()
{
    load(filename);
}

void ThemeManager::load(const QString & filename)
{
    themeString = readFile(filename);

    this->filename = filename;

    parse();
}

QString ThemeManager::readFile(const QString & filename)
{
    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly))
        throw "Cannot read file " + filename;

    QString themeStr = file.readAll();

    file.close();

    return themeStr;
}

void ThemeManager::parse()
{
    if(defaultTheme != currentTheme && currentTheme != nullptr)
        delete currentTheme;

    currentTheme = themeParser.parse(themeString);
}

void ThemeManager::setAttribute(const QString & selector, const QString & attribute, const QString & value)
{
    currentTheme->setValueBySelectorAndAttribute(selector, attribute, value);
}

QString ThemeManager::getRelativePathToStyles(const QString & filename)
{
    return getRelativePathToStyles() + filename;
}

QString ThemeManager::getRelativePathToStyles()
{
    return STYLES_PATH;
}

QString ThemeManager::getAbsolutePathToStyles(const QString & filename)
{
    return getAbsolutePathToStyles() + filename;
}

QString ThemeManager::getAbsolutePathToStyles()
{
    return QDir().currentPath() + "/" + STYLES_PATH;
}

Theme * ThemeManager::getCurrentTheme() const
{
    return currentTheme;
}

QString ThemeManager::getFilename() const
{
    return filename;
}

QString ThemeManager::getName() const
{
    if(currentTheme->isSelectorAndAttributeExists(Theme::THEME_INFO, Theme::THEME_INFO_NAME))
        return currentTheme->getValueBySelectorAndAttribute(Theme::THEME_INFO, Theme::THEME_INFO_NAME);

    return "";
}

QString ThemeManager::getRawData() const
{
    return currentTheme->toString(true);
}

const std::map<QString, QString> * ThemeManager::getThemes() const
{
    return &themesDictionary;
}

void ThemeManager::loadDefaultThemes()
{
    for (auto const& [themeName, themePath] : DEFAULT_THEMES)
        themesDictionary[themeName] = themePath;
}

void ThemeManager::loadCustomThemes()
{
    QDir directory(getAbsolutePathToStyles());
    QStringList styles = directory.entryList(QStringList() << "*.css" << "*.CSS", QDir::Files);

    foreach(QString filename, styles) {

        QString themeStr = readFile(getRelativePathToStyles(filename));
        Theme * theme = themeParser.parse(themeStr);
        QString themeName = fixedThemeName(theme);
        delete theme;

        themesDictionary[themeName] = getAbsolutePathToStyles(filename);
    }
}

QString ThemeManager::saveTheme(Theme * theme)
{
    QString filename = getRelativePathToStyles(theme->getName() + ".css");

    theme->addMissingAttributes(*defaultTheme);

    QFile file(filename);
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << theme->toString(false);
    }
    file.close();

    return filename;
}

QString ThemeManager::saveTheme()
{
    return saveTheme(currentTheme);
}

void ThemeManager::remove()
{
    themesDictionary.erase(currentTheme->getName());

    QFile::remove(filename);

    load(
        themesDictionary.at(std::prev(themesDictionary.end())->first)
    );
}

void ThemeManager::deleteCurrentTheme()
{
    if(currentTheme != nullptr && currentTheme != defaultTheme)
        delete currentTheme;
}

void ThemeManager::setTheme(Theme * theme)
{
    deleteCurrentTheme();

    currentTheme = theme;
}

void ThemeManager::createTheme(QString themeName)
{
    QString themeStr = readFile(DEFAULT_THEME_TEMPLATE);
    Theme * theme = themeParser.parse(themeStr);

    theme->setValueBySelectorAndAttribute(Theme::THEME_INFO, Theme::THEME_INFO_NAME, fixedThemeName(themeName));

    themesDictionary[theme->getName()] = saveTheme(theme);

    setTheme(theme);
}

void ThemeManager::importTheme(QString path)
{
    if(path.isEmpty()) return;

    Theme * theme = themeParser.parse(readFile(path));

    theme->setValueBySelectorAndAttribute(Theme::THEME_INFO, Theme::THEME_INFO_NAME, fixedThemeName(theme));

    themesDictionary[theme->getName()] = saveTheme(theme);

    setTheme(theme);
}

void ThemeManager::cloneTheme()
{
    currentTheme->setValueBySelectorAndAttribute(Theme::THEME_INFO, Theme::THEME_INFO_NAME, fixedThemeName(currentTheme->getName()));

    themesDictionary[currentTheme->getName()] = saveTheme(currentTheme);
}

QString ThemeManager::fixedThemeName(const Theme * theme)
{
    QString themeName;

    if(theme->isSelectorAndAttributeExists(Theme::THEME_INFO, Theme::THEME_INFO_NAME))
        themeName = theme->getValueBySelectorAndAttribute(Theme::THEME_INFO, Theme::THEME_INFO_NAME);

    return fixedThemeName(themeName);
}

QString ThemeManager::fixedThemeName(QString themeName)
{
    if(themeName.length() == 0) themeName = "Untitled";

    if(themesDictionary.find(themeName) != themesDictionary.end()) {
        int counter = 1;
        while(themesDictionary.find(themeName + QString::number(counter)) != themesDictionary.end())
            counter++;
        themeName = (themeName + QString::number(counter));
    }

    return themeName;
}
