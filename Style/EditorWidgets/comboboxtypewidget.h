#ifndef COMBOBOXTYPEWIDGET_H
#define COMBOBOXTYPEWIDGET_H

#include <QObject>
#include <QWidget>

#include "../appearance.h"
#include "../theme.h"

class ComboBoxTypeWidget: public QObject
{
    Q_OBJECT
    static void restoreFromSettings(Appearance *, QComboBox *, QJsonObject &element);
    static void connectToAppearance(Appearance *, QComboBox *fontComboBox, QJsonObject &element);
    static void updateIcons(Appearance *appearance, QString &value);
public:
    static QWidget * make(Appearance *, QJsonObject &);
};

#endif // COMBOBOXTYPEWIDGET_H
