#include "comboboxwidget.h"

QWidget *ComboBoxWidget::make(Appearance * appearance, QJsonObject & element)
{
    QComboBox * comboBox = new QComboBox(Appearance::getInstance());

    for(QJsonValue value: element["available_values"].toArray())
        comboBox->addItem(value.toString());

    restoreFromSettings(appearance, comboBox, element);
    connectToAppearance(appearance, comboBox, element);

    comboBox->setStyleSheet("border:none;");

    return comboBox;
}

void ComboBoxWidget::connectToAppearance(Appearance * appearance, QComboBox * comboBox, QJsonObject & element)
{
    connect(comboBox, &QComboBox::currentTextChanged, [appearance,element,comboBox]() {
        QString value = comboBox->currentText();
        QString valueWithAppendedValue = value + element["appended_value"].toString();
        if(element["get_from_settings"].toBool() == true)
            SettingsLoader::getInstance()->setValue(element["settings_key"].toString(), value);
        appearance->setThemeAttributes(element, valueWithAppendedValue);
        appearance->updateTheme();
    });
}

void ComboBoxWidget::restoreFromSettings(Appearance * appearance, QComboBox * comboBox, QJsonObject & element)
{
    QString value = appearance->getThemeManager()->getCurrentTheme()->getValueBySelectorAndAttribute(element["selectors"].toArray().first().toString(), element["attribute"].toString());

    if(element["get_from_settings"].toBool())
        value = SettingsLoader::getInstance()->getValue(element["settings_key"].toString()).toString();

    QString valueWithAppendedValue = value + element["appended_value"].toString();

    comboBox->setCurrentIndex(comboBox->findText(value));
    appearance->setThemeAttributes(element, valueWithAppendedValue);

}
