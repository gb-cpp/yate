#ifndef EDITORWIDGETFACTORY_H
#define EDITORWIDGETFACTORY_H

#include <QObject>
#include "../appearance.h"
#include "colorwidget.h"
#include "fontfamilywidget.h"
#include "comboboxwidget.h"
#include "comboboxtypewidget.h"

class EditorWidgetFactory : public QObject
{
    Q_OBJECT
public:
    static QWidget * make(Appearance *, QJsonObject &);

private:
    static const std::map<QString, std::function<QWidget *(Appearance *, QJsonObject &)>> TYPES;

};

#endif // EDITORWIDGETFACTORY_H
