#ifndef COMBOBOXWIDGET_H
#define COMBOBOXWIDGET_H

#include <QObject>
#include <QWidget>

#include "../appearance.h"

class ComboBoxWidget: public QObject
{
    Q_OBJECT
    static void restoreFromSettings(Appearance *, QComboBox *, QJsonObject &element);
    static void connectToAppearance(Appearance *, QComboBox *fontComboBox, QJsonObject &element);
public:
    static QWidget * make(Appearance *, QJsonObject &);
};

#endif // COMBOBOXWIDGET_H
