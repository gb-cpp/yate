#include "comboboxtypewidget.h"

QWidget *ComboBoxTypeWidget::make(Appearance * appearance, QJsonObject & element)
{
    QComboBox * comboBox = new QComboBox(Appearance::getInstance());

    for(QJsonValue value: element["available_values"].toArray())
        comboBox->addItem(value.toString());

    restoreFromSettings(appearance, comboBox, element);
    connectToAppearance(appearance, comboBox, element);

    comboBox->setStyleSheet("border:none;");

    return comboBox;
}

void ComboBoxTypeWidget::connectToAppearance(Appearance * appearance, QComboBox * comboBox, QJsonObject & element)
{
    connect(comboBox, &QComboBox::currentTextChanged, [appearance,element,comboBox]() {
        QString value = comboBox->currentText();

        if(element["get_from_settings"].toBool() == true)
            SettingsLoader::getInstance()->setValue(element["settings_key"].toString(), comboBox->currentText());

        updateIcons(appearance, value);

        appearance->setThemeAttributes(element, value);
        appearance->updateTheme();
    });
}

void ComboBoxTypeWidget::updateIcons(Appearance * appearance, QString & newValue)
{
    for(auto & [selector, attributes]: *(appearance->getThemeManager()->getCurrentTheme()))
        for(auto & [attribute, value]: attributes)
            if(newValue == "dark")
                value.replace(Theme::ICONS_DARK_PATH, Theme::ICONS_LIGHT_PATH);
            else
                value.replace(Theme::ICONS_LIGHT_PATH, Theme::ICONS_DARK_PATH);
}

void ComboBoxTypeWidget::restoreFromSettings(Appearance * appearance, QComboBox * comboBox, QJsonObject & element)
{
    QString value = appearance->getThemeManager()->getCurrentTheme()->getValueBySelectorAndAttribute(element["selectors"].toArray().first().toString(), element["attribute"].toString());

    if(element["get_from_settings"].toBool())
        value = SettingsLoader::getInstance()->getValue(element["settings_key"].toString()).toString();

    comboBox->setCurrentIndex(comboBox->findText(value));

    appearance->setThemeAttributes(element, value);
}
