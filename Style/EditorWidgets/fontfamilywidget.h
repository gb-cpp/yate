#ifndef FONTFAMILYWIDGET_H
#define FONTFAMILYWIDGET_H

#include <QObject>
#include <QWidget>

#include "../appearance.h"

class FontFamilyWidget: public QObject
{
    Q_OBJECT
    static void restoreFromSettings(Appearance *, QFontComboBox *, QJsonObject &element);
    static void connectToAppearance(Appearance *, QFontComboBox *fontComboBox, QJsonObject &element);
public:
    static QWidget * make(Appearance *, QJsonObject &);
};

#endif // FONTFAMILYWIDGET_H
