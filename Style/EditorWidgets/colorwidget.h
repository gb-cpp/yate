#ifndef COLORWIDGET_H
#define COLORWIDGET_H

#include <QObject>
#include <QWidget>

#include "../appearance.h"

class ColorWidget: public QObject
{
    Q_OBJECT
    static void restoreFromSettings(Appearance *, QPushButton *button, QJsonObject &element);
    static void connectToAppearance(Appearance *,QPushButton *button, QJsonObject &element);
    static QColor invertedColor(QColor &color);
    static void setColorWidgetColor(QPushButton * button, QColor &color, QColor &invertedColor);
public:
    static QWidget * make(Appearance *,QJsonObject &);
};

#endif // COLORWIDGET_H
