#include "colorwidget.h"

void ColorWidget::restoreFromSettings(Appearance * appearance, QPushButton * button, QJsonObject &element)
{

}

void ColorWidget::connectToAppearance(Appearance * appearance, QPushButton *button, QJsonObject &element)
{
    connect(button, &QPushButton::clicked, [element, button, appearance]() {
        QColor newColor = QColorDialog::getColor(button->palette().button().color().name(), Appearance::getInstance());
        if(!newColor.isValid())
            return;
        QColor newInvertedColor = invertedColor(newColor);
        QString newColorStr = newColor.name();

        setColorWidgetColor(button, newColor, newInvertedColor);
        button->setText(newColor.name());
        appearance->setThemeAttributes(element, newColorStr);
        appearance->updateTheme();
    });
}

QWidget *ColorWidget::make(Appearance * appearance, QJsonObject & element)
{
    QString firstSelector = element["selectors"].toArray().first().toString();
    QColor currentColor(appearance->getThemeManager()->getCurrentTheme()->getValueBySelectorAndAttribute(firstSelector, element["attribute"].toString()));
    QColor currentInvertedColor = invertedColor(currentColor);
    QPushButton * newButton = new QPushButton(currentColor.name());

    setColorWidgetColor(newButton, currentColor, currentInvertedColor);

    connectToAppearance(appearance, newButton, element);

    return newButton;
}

void ColorWidget::setColorWidgetColor(QPushButton * button, QColor & background, QColor & color)
{
    button->setStyleSheet("QPushButton { background-color: " + background.name() + "; color:" + color.name() + "; border: none; }");
}

QColor ColorWidget::invertedColor(QColor & color)
{
    if(color.red() == color.blue() && color.blue() == color.green() && (color.red() > 100 && color.red() < 155))
        return QColor(255,255,255);
    return QColor(255 - color.red(), 255 - color.green(), 255 - color.blue());
}
