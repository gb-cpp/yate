#include "editorwidgetfactory.h"
#include "Filters/mousewheelfilter.h"

const std::map<QString, std::function<QWidget *(Appearance *, QJsonObject &)>> EditorWidgetFactory::TYPES = {
    {"font-family", &FontFamilyWidget::make},
    {"font-size", &ComboBoxWidget::make},
    {"color", &ColorWidget::make},
    {"background-color", &ColorWidget::make},
    {"background", &ColorWidget::make},
    {"type", &ComboBoxTypeWidget::make},
    {"border-color", &ColorWidget::make},
    {"selection-color", &ColorWidget::make},
    {"selection-background-color", &ColorWidget::make},
    {"argumentFont", &ComboBoxWidget::make},
    {"argumentForeground", &ColorWidget::make},
    {"booleanConstantsFont", &ComboBoxWidget::make},
    {"booleanConstantsForeground", &ColorWidget::make},
    {"bracketsColorHighlight", &ColorWidget::make},
    {"commentFont", &ComboBoxWidget::make},
    {"commentForeground", &ColorWidget::make},
    {"commentBlockFont", &ComboBoxWidget::make},
    {"commentBlockForeground", &ColorWidget::make},
    {"compoundKeywordsFont",&ComboBoxWidget::make},
    {"compoundKeywordsForeground", &ColorWidget::make},
    {"controlFlowFont", &ComboBoxWidget::make},
    {"controlFlowForeground", &ColorWidget::make},
    {"defFuncNameFont", &ComboBoxWidget::make},
    {"defFuncNameForeground", &ColorWidget::make},
    {"digitsFont", &ComboBoxWidget::make},
    {"digitsForeground", &ColorWidget::make},
    {"escapeCharFont", &ComboBoxWidget::make},
    {"escapeCharForeground", &ColorWidget::make},
    {"edgeTagFont", &ComboBoxWidget::make},
    {"edgeTagForeground", &ColorWidget::make},
    {"exceptionHandlingFont", &ComboBoxWidget::make},
    {"exceptionHandlingForeground", &ColorWidget::make},
    {"friendFont", &ComboBoxWidget::make},
    {"friendForeground", &ColorWidget::make},
    {"integratedFunctionFont", &ComboBoxWidget::make},
    {"integratedFunctionForeground", &ColorWidget::make},
    {"insideTagFont", &ComboBoxWidget::make},
    {"insideTagForeground", &ColorWidget::make},
    {"jumpKeywordsFont", &ComboBoxWidget::make},
    {"jumpKeywordsForeground", &ColorWidget::make},
    {"keywordsFont", &ComboBoxWidget::make},
    {"keywordsForeground", &ColorWidget::make},
    {"loopKeywordsFont", &ComboBoxWidget::make},
    {"loopKeywordsForeground", &ColorWidget::make},
    {"methodNameFont", &ComboBoxWidget::make},
    {"methodNameForeground", &ColorWidget::make},
    {"modifiersFont", &ComboBoxWidget::make},
    {"modifiersForeground", &ColorWidget::make},
    {"namesArgsFont", &ComboBoxWidget::make},
    {"namesArgsForeground", &ColorWidget::make},
    {"namespaceFont", &ComboBoxWidget::make},
    {"namespaceForeground", &ColorWidget::make},
    {"operatorsFont", &ComboBoxWidget::make},
    {"operatorsForeground", &ColorWidget::make},
    {"objectFont", &ComboBoxWidget::make},
    {"objectForeground", &ColorWidget::make},
    {"preprocessorFont", &ComboBoxWidget::make},
    {"preprocessorForeground", &ColorWidget::make},
    {"qtWordsFont", &ComboBoxWidget::make},
    {"qtWordsForeground", &ColorWidget::make},
    {"stringFont", &ComboBoxWidget::make},
    {"stringForeground", &ColorWidget::make},
    {"syntaxDefinitionFont", &ComboBoxWidget::make},
    {"syntaxDefinitionForeground", &ColorWidget::make},
    {"tagsFont", &ComboBoxWidget::make},
    {"tagsForeground",  &ColorWidget::make},
    {"templateFont", &ComboBoxWidget::make},
    {"templateForeground",  &ColorWidget::make},
    {"thisFont", &ComboBoxWidget::make},
    {"thisForeground", &ColorWidget::make},
    {"typeKeywordsFont", &ComboBoxWidget::make},
    {"typeKeywordsForeground",  &ColorWidget::make}
};

QWidget *EditorWidgetFactory::make(Appearance * appearance,  QJsonObject & element)
{
    QString attribute = element["attribute"].toString();
    QWidget * newWidget;

    if(TYPES.find(attribute) != TYPES.end())
        newWidget = TYPES.at(attribute)(appearance, element);
    else
        newWidget = new QLabel("Unknown attribute");

    if(!element["get_from_settings"].toBool())
        newWidget->setDisabled(!appearance->isCustomMode());

    newWidget->installEventFilter(new MouseWheelFilter);

    return newWidget;
}
