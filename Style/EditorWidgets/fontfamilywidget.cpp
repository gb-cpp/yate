#include "fontfamilywidget.h"

QWidget *FontFamilyWidget::make(Appearance * appearance, QJsonObject & element)
{
    QFontComboBox * fontComboBox = new QFontComboBox(appearance);

    restoreFromSettings(appearance, fontComboBox, element);

    connectToAppearance(appearance, fontComboBox, element);

    fontComboBox->setStyleSheet("border:none;");

    return fontComboBox;
}

void FontFamilyWidget::connectToAppearance(Appearance * appearance, QFontComboBox * fontComboBox, QJsonObject & element)
{
    connect(fontComboBox, &QFontComboBox::currentTextChanged, [element,fontComboBox,appearance]() {
        QString fontString = fontComboBox->currentText();
        SettingsLoader::getInstance()->setValue(element["settings_key"].toString(), QJsonValue(fontString));
        appearance->setThemeAttributes(element, fontString);
        appearance->updateTheme();
    });
}

void FontFamilyWidget::restoreFromSettings(Appearance * appearance, QFontComboBox * fontComboBox, QJsonObject & element)
{
    QString value = appearance->getThemeManager()->getCurrentTheme()->getValueBySelectorAndAttribute(element["selectors"].toArray().first().toString(), element["attribute"].toString());

    if(element["get_from_settings"].toBool())
        value = SettingsLoader::getInstance()->getValue(element["settings_key"].toString()).toString();

    fontComboBox->setCurrentFont(QFont(value));
    appearance->setThemeAttributes(element, value);
}
