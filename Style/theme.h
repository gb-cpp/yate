#ifndef THEME_H
#define THEME_H

#include <map>
#include <iterator>
#include <cstddef>
#include <QString>
#include <QColor>
#include <vector>

class Theme
{
public:

    const static QString THEME_INFO;
    const static QString THEME_LINE_NUMBER;
    const static QString THEME_HIGHLIGHTER;
    const static QString THEME_HIGHLIGHTER_CPP;
    const static QString THEME_HIGHLIGHTER_HTML;
    const static QString THEME_HIGHLIGHTER_PYTHON;

    const static std::vector<QString> SELECTORS;

    const static QString THEME_INFO_TYPE;
    const static QString THEME_INFO_LIGHT_ATTRIBUTE;
    const static QString THEME_INFO_DARK_ATTRIBUTE;
    const static QString THEME_INFO_NAME;

    const static QString ICONS_DARK_PATH;
    const static QString ICONS_LIGHT_PATH;

    const static std::map<QString, QString> EMPTY_SELECTOR;

    Theme(std::map<QString, std::map<QString, QString>> theme, bool isValid): theme(theme), isValid(isValid) {}
    Theme();
    Theme(Theme const&) = default;
    Theme& operator=(const Theme & other);
    ~Theme() = default;

    std::map<QString, std::map<QString, QString>>::iterator begin() { return theme.begin(); }
    std::map<QString, std::map<QString, QString>>::iterator end()   { return theme.end(); }

    bool isSelectorExists(const QString &) const;
    const std::map<QString, QString> & getSelector(const QString &) const;

    bool isSelectorAndAttributeExists(const QString &, const QString &) const;
    QString getValueBySelectorAndAttribute(const QString &, const QString &) const;

    void setValueBySelectorAndAttribute(const QString &, const QString &, const QString &);

    bool isValidTheme() const;
    bool isDark() const;
    bool isLight() const;

    QColor getLineNumberColor() const;
    QColor getLineNumberBackground() const;

    QString toString(bool onlyStyleSelectors = true);

    QString getName() const;

    void addMissingAttributes(const Theme &from);

    QString getStyleSelectorString();
    QString getThemeSelectorsString();
private:
    std::map<QString, std::map<QString, QString>> theme;
    bool isValid;
};

#endif // THEME_H
