#include "appearance.h"

Appearance * Appearance::instance = nullptr;

Appearance *Appearance::getInstance(QWidget *mainWindow)
{
    if(mainWindow != nullptr && instance == nullptr)
        instance = new Appearance(mainWindow);

    return instance;
}

Appearance::Appearance(QWidget *mainWindow) : QDialog(mainWindow), ui(new Ui::Appearance), mainWindow(mainWindow)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setThemesNamesAndSettings();
}

Appearance::~Appearance()
{
    delete ui;
}

void Appearance::setThemesNamesAndSettings()
{
    setThemesNames();
    setThemeSettings();
    enableCustomMode(!themeManager.isDefaultTheme());
}

void Appearance::setThemesNames()
{
    ui->cbUIStyles->clear();

    for(auto & [themeName,themePath]: *(themeManager.getThemes()))
        ui->cbUIStyles->addItem(themeName);

    QString themeName = SettingsLoader::getInstance()->getValue("theme").toString();

    if(themeManager.getThemes()->find(themeName) != themeManager.getThemes()->end()){
        ui->cbUIStyles->setCurrentText(themeName);
        themeManager.load(themeManager.getThemes()->at(themeName));
    }
}

void Appearance::setThemeSettings()
{
    setSettingsHeadings();
    setSettingsTable();
}

void Appearance::setSettingsHeadings()
{
    ui->settings->verticalHeader()->hide();
    ui->settings->setHorizontalHeaderLabels({"Item","Current value"});
    ui->settings->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void Appearance::setSettingsTable()
{
    ui->settings->setRowCount(0);

    for (auto settingsParameter : themeManager.getEditableAttributes()) {

        QJsonObject element = settingsParameter.toObject();
        int row = ui->settings->rowCount();
        ui->settings->insertRow(row);

        addSettingsRow(row, element);
    }

    updateTheme();
}

void Appearance::addSettingsRow(int & row, QJsonObject & element)
{
    ui->settings->setCellWidget(row, 0, new QLabel(element["description"].toString()));
    ui->settings->setCellWidget(row, 1, EditorWidgetFactory::make(this,element));
}

void Appearance::updateTheme()
{
    this->mainWindow->setStyleSheet(themeManager.getRawData());

    emit onUpdate(this);
    emit onUpdate();

    emit onUpdated();
}

void Appearance::setThemeAttributes(const QJsonObject &element, QString &value)
{
    for(const QJsonValue & selector: element["selectors"].toArray())
        themeManager.getCurrentTheme()->setValueBySelectorAndAttribute(selector.toString(), element["attribute"].toString(), value);
}

const ThemeManager * Appearance::getThemeManager()
{
    return &themeManager;
}

bool Appearance::isCustomMode()
{
    return !themeManager.isDefaultTheme();
}

void Appearance::enableCustomMode(const bool status) {
    for(auto & button: {ui->btnEditStyle, ui->btnRefresh, ui->btnRemoveStyle, ui->btnSave})
        button->setEnabled(status);
}

void Appearance::warningMessage(QString message)
{
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.exec();
}

void Appearance::on_btnAddNewStyle_clicked() {
    themeManager.createTheme(ui->lineStyleName->text());

    SettingsLoader::getInstance()->setValue("theme", themeManager.getCurrentTheme()->getName());

    setThemesNamesAndSettings();
}

void Appearance::on_btnImport_clicked()
{
    themeManager.importTheme(QFileDialog::getOpenFileName(this, "Open css file.", "", "CSS (*.css)"));

    SettingsLoader::getInstance()->setValue("theme", themeManager.getCurrentTheme()->getName());

    setThemesNamesAndSettings();
}

void Appearance::on_btnRefresh_clicked()
{
    themeManager.reload();

    setThemesNamesAndSettings();
}

void Appearance::on_btnEditStyle_clicked()
{
    MenuBar * menuBar = mainWindow->findChild<MenuBar *>("mainMenuBar");
    if(menuBar != nullptr)
        menuBar->openFileByPath(themeManager.getFilename());
}

void Appearance::on_cbUIStyles_activated(const QString &arg1)
{
    themeManager.load(themeManager.getThemes()->at(arg1));

    SettingsLoader::getInstance()->setValue("theme", arg1);

    setThemesNamesAndSettings();
}

void Appearance::on_btnRemoveStyle_clicked()
{
    themeManager.remove();

    SettingsLoader::getInstance()->setValue("theme", themeManager.getCurrentTheme()->getName());

    setThemesNamesAndSettings();
}

void Appearance::on_btnSave_clicked()
{
    themeManager.saveTheme();
}

void Appearance::on_btnClone_clicked()
{
    themeManager.cloneTheme();

    SettingsLoader::getInstance()->setValue("theme", themeManager.getCurrentTheme()->getName());

    setThemesNamesAndSettings();
}
