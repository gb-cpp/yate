#include "theme.h"

const QString Theme::THEME_INFO = "ThemeInfo";
const QString Theme::THEME_LINE_NUMBER = "ThemeLineNumber";
const QString Theme::THEME_HIGHLIGHTER = "ThemeHighlighter";

const std::vector<QString> Theme::SELECTORS = {Theme::THEME_INFO,Theme::THEME_LINE_NUMBER,Theme::THEME_HIGHLIGHTER};

const QString Theme::THEME_INFO_NAME = "name";
const QString Theme::THEME_INFO_LIGHT_ATTRIBUTE = "light";
const QString Theme::THEME_INFO_DARK_ATTRIBUTE = "dark";
const QString Theme::THEME_INFO_TYPE = "type";

const QString Theme::ICONS_DARK_PATH = ":/images/icons/black";
const QString Theme::ICONS_LIGHT_PATH = ":/images/icons/white";

const std::map<QString, QString> Theme::EMPTY_SELECTOR = {};

Theme::Theme()
{

}

Theme & Theme::operator=(const Theme & from)
{
    if(&from != this) {
        for(const auto & [selector, attributes]: from.theme){
            theme[selector] = {};
            for(const auto & [attribute, value]: attributes){
                theme[selector][attribute] = value;
            }
        }
    }

    return *this;
}

void Theme::addMissingAttributes(const Theme & from)
{
    if(&from != this) {
        for(const auto & [selector, attributes]: from.theme){
            if(theme.find(selector) == theme.end())
                theme[selector] = from.getSelector(selector);
            for(const auto & [attribute, value]: attributes) {
                if(theme.at(selector).find(attribute) == theme.at(selector).end())
                    theme[selector][attribute] = value;
            }
        }
    }
}

bool Theme::isSelectorExists(const QString & selector) const
{
    for(auto & [selectorInTheme, attributes]: theme)
        if(selectorInTheme.toLower() == selector.toLower())
            return true;

    return false;
}

bool Theme::isSelectorAndAttributeExists(const QString &selector, const QString &attribute) const
{
    if(!isSelectorExists(selector))
        return false;

    for(auto & [attributeInTheme, value]: getSelector(selector))
        if(attributeInTheme.toLower() == attribute.toLower())
            return true;

    return false;
}

const std::map<QString, QString> & Theme::getSelector(const QString &selector) const
{
    for(auto & [selectorInTheme, attributes]: theme)
        if(selectorInTheme.toLower() == selector.toLower())
            return attributes;

    return EMPTY_SELECTOR;
}

QString Theme::getValueBySelectorAndAttribute(const QString &selector, const QString &attribute) const
{
    for(auto & [attributeInTheme, value]: getSelector(selector))
        if(attributeInTheme.toLower() == attribute.toLower())
            return value;

    return "";
}

void Theme::setValueBySelectorAndAttribute(const QString &selector, const QString &attribute, const QString &value)
{
    theme[selector][attribute] = value;
}

bool Theme::isValidTheme() const
{
    return isValid;
}

bool Theme::isDark() const
{
    return !isLight();
}

bool Theme::isLight() const
{
    if(isSelectorAndAttributeExists(this->THEME_INFO, this->THEME_INFO_TYPE))
        return getValueBySelectorAndAttribute(this->THEME_INFO, this->THEME_INFO_TYPE) == this->THEME_INFO_LIGHT_ATTRIBUTE;

    return false;
}

QColor Theme::getLineNumberColor() const
{
    QString value = getValueBySelectorAndAttribute(Theme::THEME_LINE_NUMBER, "color");

    return QColor(value.length() ? value : "#ffffff");
}

QColor Theme::getLineNumberBackground() const
{
    QString value = getValueBySelectorAndAttribute(Theme::THEME_LINE_NUMBER, "background");

    return QColor(value.length() ? value : "#ffffff");
}

QString Theme::getStyleSelectorString()
{
    QString output;

    for(const auto & [selector, attributes]: theme) {

        if(std::find(SELECTORS.begin(), SELECTORS.end(), selector) != SELECTORS.end())
            continue;

        output += (selector + "{\n");

        for(const auto & [attribute, value]: attributes)
            output += ("\t" + attribute + ": " + value + ";\n");

        output += "}\n";
    }

    return output;
}

QString Theme::getThemeSelectorsString()
{
    QString output;

    for(const auto & selector: SELECTORS) {
        if(!isSelectorExists(selector))
            continue;

        output += (selector + "{\n");

        for(const auto & [attribute, value]: getSelector(selector))
            output += ("\t" + attribute + ": " + value + ";\n");

        output += "}\n";
    }

    return output;
}

QString Theme::toString(bool onlyStyleSelectors)
{
    QString output = "";

    if(!onlyStyleSelectors)
        output = getThemeSelectorsString();

    output += getStyleSelectorString();

    return output;
}

QString Theme::getName() const
{
    return getValueBySelectorAndAttribute(THEME_INFO, THEME_INFO_NAME);
}
