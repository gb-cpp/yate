#include "workspacesplitter.h"
#include "widgetfoundation.h"

WorkspaceSplitter::WorkspaceSplitter(Qt::Orientation orientation) {
    setOrientation(orientation);
    pWidgets = new QVector<QWidget*>();
    vector = new QVector<QWidget*>();
}

/* Добавлем к нему: пользовательский виджет, указатель на этот класс и указатель на вектор pWidgets ============================= */

void WorkspaceSplitter::addNewWidget(QWidget *widget) {
    for(int i = 0; i < vector->length(); i++){
        if(vector->at(i)->objectName() == widget->objectName()) return;
    }
    vector->push_back(widget);
    WidgetFoundation *wF = new WidgetFoundation();          // Создаем обьект фундамент
    wF->setWidget(widget);
    wF->setWorkspaceSplitter(this);
    wF->setPWidgets(pWidgets, vector);
    QWidget *w1 = wF->getWidget();                          // Из-за внутренних проблем, пришлось получить только указатель QWidget
    pWidgets->push_back(w1);                                // А потом добавлять его в вектор
    addWidget(w1);
}
