#include "nixconsole.h"
nixConsole::nixConsole(QWidget *parent) : QPlainTextEdit(parent){
    process = new QProcess(this);
    clipboard = QApplication::clipboard();
    cursor = QTextCursor(this->textCursor());
    history = new QStringList;
    historyPos = 0;
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(outConsole()));
    connect(process, SIGNAL(readyReadStandardError()), this, SLOT(outErrortoConsole()));
    process->start("sh");
    insertPrompt();
    setObjectName("Console");
}

/* ============================================================================================================================= */

nixConsole::~nixConsole(){
    process->close();
    process = nullptr;
    history = nullptr;
}

/* ============================================================================================================================= */

void nixConsole::insertPrompt(){
    userHostName = QSysInfo::machineHostName();
    userName = qgetenv("USER");
    if (userName.isEmpty()){
        userName = qgetenv("USERNAME");
    }
    prompt = userName + "@" + userHostName + "$";
    textCursor().insertText(prompt);
    scrollDown();
}

/* ============================================================================================================================= */

void nixConsole::scrollDown(){
    QScrollBar *vbar = verticalScrollBar();
    vbar->setValue(vbar->maximum());
}

/* ============================================================================================================================= */

void nixConsole::keyPressEvent(QKeyEvent *event){
    switch (event->key()){
    case Qt::Key_Return:
        onEnter();
        break;
    case Qt::Key_Backspace:
        onBackspace();
        break;
    case Qt::Key_Up:
        historyBack();
        break;
    case Qt::Key_Down:
        historyForward();
        break;
    case Qt::Key_Right:
        moveCursor(QTextCursor::Right);
        break;
    case Qt::Key_Left:
        moveCursor(QTextCursor::Left);
        break;
    case Qt::Key_PageUp:
        moveCursor(QTextCursor::Up);
        break;
    case Qt::Key_PageDown:
        moveCursor(QTextCursor::Down);
        break;
    default:
        if (event->matches(QKeySequence::Copy)){
            QString selectedText = this->textCursor().selectedText();
            clipboard->setText(selectedText);
        }
        else if (event->matches(QKeySequence::Paste)){
            insertPlainText(clipboard->text());
        }
        else if (!(this->textCursor().position() < this->toPlainText().size())){
            userText += event->text();
            insertPlainText(event->text());
        }
    }
}

/* ============================================================================================================================= */

void nixConsole::mouseDoubleClickEvent(QMouseEvent *){}

/* ============================================================================================================================= */

void nixConsole::contextMenuEvent(QContextMenuEvent *){}

/* ============================================================================================================================= */

void nixConsole::onEnter(){
    QByteArray arr = userText.toUtf8() + '\n';
    process->write(arr);
    historyAdd(userText);
    userText = "";
    emit outConsole();
}

/* ============================================================================================================================= */

void nixConsole::onBackspace(){

    if (!userText.isEmpty()){
        if ((this->textCursor().selectedText().size())> 0){
            cursor.movePosition(QTextCursor::EndOfLine, QTextCursor::MoveAnchor);
            this->setTextCursor(cursor);
        }
        int pos = userText.size();
        userText = userText.remove(pos - 1, 1);
        cursor.deletePreviousChar();
    }
}

/* ============================================================================================================================= */

void nixConsole::moveCursor(QTextCursor::MoveOperation operation){
    cursor.movePosition(operation, QTextCursor::MoveAnchor, 1);
    this->setTextCursor(cursor);
}

/* ============================================================================================================================= */

void nixConsole::historyAdd(QString cmd){
    history->append(cmd);
    historyPos = history->length();
}

/* ============================================================================================================================= */

void nixConsole::historyBack(){
    if(!historyPos){
        return;
    }
    cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, this->blockCount());
    cursor.movePosition(QTextCursor::StartOfBlock);
    cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
    cursor.removeSelectedText();
    cursor.insertText(prompt + history->at(historyPos - 1));
    userText = history->at(historyPos - 1);
    cursor.movePosition(QTextCursor::Right,QTextCursor::MoveAnchor);
    setTextCursor(cursor);
    historyPos--;
}

/* ============================================================================================================================= */

void nixConsole::historyForward(){
   if(historyPos == history->length()){
       return;
   }
   cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, this->blockCount());
   cursor.movePosition(QTextCursor::StartOfBlock);
   cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
   cursor.removeSelectedText();
   if(historyPos == history->length() - 1){
        cursor.insertText(prompt);
        cursor.movePosition(QTextCursor::Right,QTextCursor::MoveAnchor);
   } else{
        cursor.insertText(prompt + history->at(historyPos + 1));
        userText = history->at(historyPos + 1);
        cursor.movePosition(QTextCursor::Right,QTextCursor::MoveAnchor);
   }
   setTextCursor(cursor);
   historyPos++;
}

/* ============================================================================================================================= */

void nixConsole::outConsole(){
    output = process->readAllStandardOutput();
    if((static_cast<QProcess*>(sender()) == nullptr)){
        insertPlainText("\n");
        insertPrompt();
    } else {
        cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, this->blockCount());
        cursor.movePosition(QTextCursor::StartOfBlock);
        cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
        insertPlainText(output);
        insertPrompt();
        verticalScrollBar()->setSliderPosition(this->verticalScrollBar()->maximum());
        userText = "";
    }
}

/* ============================================================================================================================= */

void nixConsole::outErrortoConsole(){
    output = process->readAllStandardError();
    cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, this->blockCount());
    cursor.movePosition(QTextCursor::StartOfBlock);
    cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
    cursor.removeSelectedText();
    insertPlainText(output);
    insertPrompt();
    userText = "";
}

/* ============================================================================================================================= */
QString nixConsole::returnPrompt(){
    return prompt;
}
/* ============================================================================================================================= */
