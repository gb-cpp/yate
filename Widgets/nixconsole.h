#ifndef NIXCONSOLE_H
#define NIXCONSOLE_H

#include <QObject>
#include <QPlainTextEdit>
#include <QWidget>
#include <QClipboard>
#include <QProcess>
#include <QApplication>
#include <QScrollBar>
#include <QTextCodec>
#include <QDir>


class nixConsole : public QPlainTextEdit
{
    Q_OBJECT
public:
    explicit nixConsole(QWidget *parent = nullptr);
    ~nixConsole() override;
    QString returnPrompt();                                 //функция для автоматических тестов

protected:
    void keyPressEvent(QKeyEvent *) override;               //переназначаем клавиши клавиатуры
    void mouseDoubleClickEvent(QMouseEvent *) override;     //отключаем двойной клик мыши
    void contextMenuEvent(QContextMenuEvent *) override;    //отключаем контекстное меню

private:
    QString userName;                                       //имя пользователя
    QString userHostName;                                   //название компьютера пользователя
    QString userText;                                       //текст введенный пользователем
    QString prompt;                                         //строка приглашения пользователя ввести команду (userName@userHostName$)
    QString OS;                                             //операционная система
    QByteArray output;                                      //вывод данных из консоли
    QStringList *history;                                   //история введеных команд пользователем
    QProcess *process;                                      //процесс, запускающей шелл
    QClipboard *clipboard;                                  //буфер обмена
    QTextCursor cursor;                                     //текстовый курсорс
    int historyPos;                                         //счетчик введенных пользователем команд, отвечающий за перемещение по 
                                                            //истории


    void onEnter();                                         //функция отвечающая за нажатие на кнопку Enter
    void onBackspace();                                     //функция отвечающая за нажатие на кнопку Backspace
    void insertPrompt();                                    //функция отвечающая за вывод приглашения пользователя ввести команду
                                                            //(userName@hostName&)
    void historyAdd(QString);                               //функция отвечающая за добавление команды в историю команд
    void historyBack();                                     //функция отвечающая за перемещение по истории команд назад
    void historyForward();                                  //функция отвечающая за перемещение по истории команд вперед
    void moveCursor(QTextCursor::MoveOperation operation);  //функция отвечающая за перемещение курсора по консоли стрелками на 
                                                            //клавиатуре
    void scrollDown();                                      //функция отвечающая за опускания скроллбара в самый низ

private slots:
    void outConsole();                                      //слот отвечающий за вывод из шелла в нашу консоль
    void outErrortoConsole();                               //слот отвечающий за вывод ошибок из шелла в нашу консоль

};

#endif // CONSOLE_H
