#include "menubar.h"

MenuBar::MenuBar(QWidget *parent) : QMenuBar(parent), m_checkSyntax("None") {
    fileOpen = new FileOpen(this);
    fileMenu = this->addMenu(tr("File"));

	HotkeyEditor_shortcut temp;

	newAct = new QAction(tr("New"));
    newAct->setShortcuts(QKeySequence::New);
	newAct->setObjectName("1_new_file");
	temp.group_name = "Main menu";
	temp.visible_name = tr("New file");
	temp.key_sequence = newAct->shortcut();
	shortcutsList.append(newAct);
	hotkeysList.insert(newAct->objectName(),temp);
    fileMenu->addAction(newAct);

    openAct = new QAction(tr("Open"));
    openAct->setShortcuts(QKeySequence::Open);
	openAct->setObjectName("2_open_file");
	temp.group_name = "Main menu";
	temp.visible_name = tr("Open file");
	temp.key_sequence = openAct->shortcut();
	shortcutsList.append(openAct);
	hotkeysList.insert(openAct->objectName(),temp);
    fileMenu->addAction(openAct);

    saveAct = new QAction(tr("Save"));
    saveAct->setShortcuts(QKeySequence::Save);
	saveAct->setObjectName("3_save_file");
	temp.group_name = "Main menu";
	temp.visible_name = tr("Save file");
	temp.key_sequence = saveAct->shortcut();
	shortcutsList.append(saveAct);
	hotkeysList.insert(saveAct->objectName(),temp);
    fileMenu->addAction(saveAct);

	saveAsAct = new QAction(tr("Save file As"));
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
	saveAsAct->setObjectName("4_save_as");
	temp.group_name = "Main menu";
	temp.visible_name = tr("Save file As");
	temp.key_sequence = saveAsAct->shortcut();
	shortcutsList.append(saveAsAct);
	hotkeysList.insert(saveAsAct->objectName(),temp);
    fileMenu->addAction(saveAsAct);

    printAct = new QAction(tr("Print"));
    printAct->setShortcuts(QKeySequence::Print);
	printAct->setObjectName("5_print");
	temp.group_name = "Main menu";
	temp.visible_name = tr("Print");
	temp.key_sequence = printAct->shortcut();
	shortcutsList.append(printAct);
	hotkeysList.insert(printAct->objectName(),temp);
    fileMenu->addAction(printAct);

    fileMenu->addSeparator();

    exitAct = new QAction(tr("Exit"));
    exitAct->setShortcuts(QKeySequence::Quit);
	exitAct->setObjectName("6_exit");
	temp.group_name = "Main menu";
	temp.visible_name = tr("Exit");
	temp.key_sequence = exitAct->shortcut();
	shortcutsList.append(exitAct);
	hotkeysList.insert(exitAct->objectName(),temp);
    fileMenu->addAction(exitAct);

    editMenu = this->addMenu(tr("Edit"));

    cutAct = new QAction(tr("Cut"));
    cutAct->setShortcuts(QKeySequence::Cut);
    editMenu->addAction(cutAct);

    copyAct = new QAction(tr("Copy"));
    copyAct->setShortcuts(QKeySequence::Copy);
    editMenu->addAction(copyAct);

    pasteAct = new QAction(tr("Paste"));
    pasteAct->setShortcuts(QKeySequence::Paste);
    editMenu->addAction(pasteAct);

    syntaxGroup = new QActionGroup(this);
    syntaxMenu = this->addMenu(tr("Syntax"));
    syntaxMenu->setEnabled(false);

    QDir dir(":/highlightning/highlights/");
    if (dir.exists()) {
        dir.setFilter(QDir::Files);
        dir.setSorting(QDir::Name);
        QFileInfoList list = dir.entryInfoList();
        for (int i = 0; i < list.size(); ++i) {
            QFileInfo fileInfo = list.at(i);
            options = new JsonClass(":/highlightning/highlights/" + fileInfo.fileName());
            QString nameSyntax = options->getNameExtension();
            QAction *act = new QAction(nameSyntax);

			act->setObjectName(nameSyntax);
			temp.group_name = "Syntax";
			temp.visible_name = tr("Switch to ") + nameSyntax;
			temp.key_sequence = act->shortcut();
			shortcutsList.append(act);
			hotkeysList.insert(act->objectName(),temp);

            syntaxMenu->addAction(act);
            syntaxGroup->addAction(act);
            act->setCheckable(true);
            connect(act, &QAction::triggered, this, &MenuBar::setSyntax);
        }
    }
    noneAct = new QAction(tr("None"));

	noneAct->setObjectName("none");
	temp.group_name = "Syntax";
	temp.visible_name = "Turn off";
	temp.key_sequence = noneAct->shortcut();
	shortcutsList.append(noneAct);
	hotkeysList.insert(noneAct->objectName(),temp);

    syntaxMenu->addAction(noneAct);
    syntaxGroup->addAction(noneAct);
    noneAct->setCheckable(true);
    noneAct->setChecked(true);
    connect(noneAct, &QAction::triggered, this, &MenuBar::setSyntax);

/* ================================ Автодополнение ============================================================================= */

    autocompletionGroup = new QActionGroup(this);
    autocompletionMenu = this->addMenu(tr("Autocompletion"));
    autocompletionMenu->setEnabled(false);

    autoCompletionOff = new QAction(tr("OFF"));

	autoCompletionOff->setObjectName("off");
	temp.group_name = "Autocompletion";
	temp.visible_name = tr("Switch off");
	temp.key_sequence = autoCompletionOff->shortcut();
	shortcutsList.append(autoCompletionOff);
	hotkeysList.insert(autoCompletionOff->objectName(),temp);

    autocompletionMenu->addAction(autoCompletionOff);
    autocompletionGroup->addAction(autoCompletionOff);
    autoCompletionOff->setCheckable(true);
    autoCompletionOff->setChecked(true);
    connect(autoCompletionOff, &QAction::triggered, this, &MenuBar::setAutocompletion);

    autoCompletionText = new QAction(tr("Text"));

	autoCompletionText->setObjectName("text");
	temp.group_name = "Autocompletion";
	temp.visible_name = tr("Switch to Text");
	temp.key_sequence = autoCompletionText->shortcut();
	shortcutsList.append(autoCompletionText);
	hotkeysList.insert(autoCompletionText->objectName(),temp);

    autocompletionMenu->addAction(autoCompletionText);
    autocompletionGroup->addAction(autoCompletionText);
    autoCompletionText->setCheckable(true);
    autoCompletionText->setChecked(false);
    connect(autoCompletionText, &QAction::triggered, this, &MenuBar::setAutocompletion);

    autoCompletionCpp = new QAction(tr("C++"));

	autoCompletionCpp->setObjectName("cplusplus");
	temp.group_name = "Autocompletion";
	temp.visible_name = tr("Switch to C++");
	temp.key_sequence = autoCompletionCpp->shortcut();
	shortcutsList.append(autoCompletionCpp);
	hotkeysList.insert(autoCompletionCpp->objectName(),temp);

    autocompletionMenu->addAction(autoCompletionCpp);
    autocompletionGroup->addAction(autoCompletionCpp);
    autoCompletionCpp->setCheckable(true);
    autoCompletionCpp->setChecked(false);
    connect(autoCompletionCpp, &QAction::triggered, this, &MenuBar::setAutocompletion);

/* ============================================================================================================================= */

    instruments = this->addMenu(tr("Instruments"));

    consoleAct = new QAction(tr("Console"));
	consoleAct->setObjectName("console");
	temp.group_name = "Instruments";
	temp.visible_name = tr("Open Console window");
	temp.key_sequence = consoleAct->shortcut();
	shortcutsList.append(consoleAct);
	hotkeysList.insert(consoleAct->objectName(),temp);
    instruments->addAction(consoleAct);

    appearanceAct = new QAction(tr("Appearance"));
	appearanceAct->setObjectName("appearance");
	temp.group_name = "Instruments";
	temp.visible_name = tr("Open Appearance settings");
	temp.key_sequence = appearanceAct->shortcut();
	shortcutsList.append(appearanceAct);
	hotkeysList.insert(appearanceAct->objectName(),temp);
    instruments->addAction(appearanceAct);

	hotkeysEditorAct = new QAction(tr("Edit Hotkeys"));
	hotkeysEditorAct->setObjectName("hotkeys_editor");
	temp.group_name = "Instruments";
	temp.visible_name = tr("Open Hotkeys editor");
	temp.key_sequence = hotkeysEditorAct->shortcut();
	shortcutsList.append(hotkeysEditorAct);
	hotkeysList.insert(hotkeysEditorAct->objectName(),temp);
	instruments->addAction(hotkeysEditorAct);

    buttonVisibleLine = new QAction(tr("Line index"), this);
	buttonVisibleLine->setObjectName("line_index");
	temp.group_name = "Instruments";
	temp.visible_name = tr("Switch \"Line index\"");
	temp.key_sequence = buttonVisibleLine->shortcut();
	shortcutsList.append(buttonVisibleLine);
	hotkeysList.insert(buttonVisibleLine->objectName(),temp);
    buttonVisibleLine->setCheckable(true);
    buttonVisibleLine->setEnabled(false);
    instruments->addAction(buttonVisibleLine);

    colorWidget = new QAction(tr("Color widget"), this);
    colorWidget->setCheckable(true);
    colorWidget->setEnabled(false);
    instruments->addAction(colorWidget);

    helpMenu = this->addMenu(tr("Help"));
    aboutAct = new QAction(tr("About"));

	aboutAct->setObjectName("about");
	temp.group_name = "Help";
	temp.visible_name = tr("About");
	temp.key_sequence = aboutAct->shortcut();
	shortcutsList.append(aboutAct);
	hotkeysList.insert(aboutAct->objectName(),temp);

    helpMenu->addAction(aboutAct);
    connect(newAct, &QAction::triggered, this, &MenuBar::newFile);
    connect(openAct, &QAction::triggered, this, &MenuBar::openFile);
    connect(saveAct, &QAction::triggered, this, &MenuBar::saveFile);
    connect(saveAsAct, &QAction::triggered, this, &MenuBar::saveAsFile);
    connect(exitAct, &QAction::triggered, qApp, &QApplication::closeAllWindows);
    connect(cutAct, &QAction::triggered, this, &MenuBar::cut);
    connect(copyAct, &QAction::triggered, this, &MenuBar::copy);
    connect(pasteAct, &QAction::triggered, this, &MenuBar::paste);
    connect(printAct, &QAction::triggered, this, &MenuBar::print);
    connect(aboutAct, &QAction::triggered, this, &MenuBar::about);
    connect(consoleAct, &QAction::triggered, this, &MenuBar::addConsole);
    connect(buttonVisibleLine, &QAction::triggered, this, &MenuBar::switchNumberLine);
    connect(colorWidget, &QAction::triggered, this, &MenuBar::switchColorWidget);
	initHotkeys();
}

/* ============================================================================================================================= */

void MenuBar::setSyntax() {
    if(activeBuffer()) {
        if (sender() == noneAct) {
            activeBuffer()->buffer()->setSyntax(tr("None"), activeBuffer()->document());
        } else {
            activeBuffer()->buffer()->setSyntax(((QAction*)sender())->text(), activeBuffer()->document());
        }
    }
}

/* =============================================== Автодополнение ============================================================== */

void MenuBar::setAutocompletion()
{
    if(activeBuffer()) {
        if (sender() == autoCompletionCpp) {
            activeBuffer()->VisibleAutocompleteCpp(true);
        } else if (sender() == autoCompletionText){
            activeBuffer()->VisibleAutocompleteText(true);
        } else {
            activeBuffer()->VisibleAutocompleteOff(true);
        }
    }
}

/* ============================================================================================================================= */

void MenuBar::saveFile() {
    if (auto tedit = activeBuffer()) {
        if (tedit->buffer()->hasPath()) {
            tedit->saveFile();
        } else {
            saveAsFile();
        }
    }
}

/* ============================================================================================================================= */

void MenuBar::saveAsFile() {
    if (auto tedit = activeBuffer()) {
        tedit->saveAsFile();
        setSyntaxMenu();
    }
}

/* ============================================================================================================================= */

void MenuBar::print() {
    if (QMdiSubWindow *activeSubWindow = mdiArea->activeSubWindow()){
        QPrinter printer;
        QPrintDialog printDialog(&printer, this);
        printDialog.setWindowTitle(tr("Print"));
        if (printDialog.exec() != QDialog::Accepted) {
            return;
        }
        QString className = activeSubWindow->widget()->metaObject()->className();
        if (className == "TextEdit") {
            auto edit = qobject_cast<TextEdit*>(activeSubWindow->widget());
            edit->print(&printer);
        } else if (className == "HexEdit") {
            auto edit = qobject_cast<HexEdit*>(activeSubWindow->widget());
            edit->print(&printer);
        }
    }
}

/* ============================================================================================================================= */

void MenuBar::newFile() {
    fileOpen->setMdiArea(mdiArea);
    fileOpen->setAppearance(appearance);
    fileOpen->setFilePath("", false);
}

/* ============================================================================================================================= */

void MenuBar::openFile() {
    const QString fileName = QFileDialog::getOpenFileName(this, tr("Open"));
    openFileByPath(fileName);
}

/* ============================================================================================================================= */

void MenuBar::cut() {
    if (activeBuffer()) {
        activeBuffer()->cut();
    }
}

/* ============================================================================================================================= */

void MenuBar::copy() {
    if (activeBuffer()) {
        activeBuffer()->copy();
    }
}

/* ============================================================================================================================= */

void MenuBar::paste() {
    if (activeBuffer()) {
        activeBuffer()->paste();
    }
}

/* ============================================================================================================================= */

void MenuBar::about() {
    QMessageBox::about(this, tr("About YATE"), tr("The <b>YATE</b> is a multifunctional and convenient text editor."));
}

/* ============================================================================================================================= */

void MenuBar::addConsole() {
    QString OS = QSysInfo::productType();
    if(OS=="windows")
        workspaceSplitter->addNewWidget(new winConsole());
    else
        workspaceSplitter->addNewWidget(new nixConsole());
}

/* ============================================================================================================================= */

void MenuBar::switchNumberLine() {
    if (activeBuffer()){
        activeBuffer()->VisibleLine(!activeBuffer()->getVisibleLine());
    }
}

void MenuBar::switchColorWidget()
{
    if (activeBuffer()){
        activeBuffer()->setVisibleColorWidget(!activeBuffer()->getVisibleColorWidget());
    }
}

/* ============================================================================================================================= */

TextEdit *MenuBar::activeBuffer() const {
    if (QMdiSubWindow *activeSubWindow = mdiArea->activeSubWindow()) {
        return qobject_cast<TextEdit*>(activeSubWindow->widget());
    }
	return nullptr;
	}
/* ============================================================================================================================= */
void MenuBar::initHotkeys(){
		connect(HotkeysEditor::getInstance(),&HotkeysEditor::updated,this,&MenuBar::updateHotkeys);
		connect(hotkeysEditorAct, &QAction::triggered, HotkeysEditor::getInstance(), &HotkeysEditor::show);
		HotkeysEditor::getInstance()->sendHotkeys(this,visibleName,hotkeysList,0);
	}
void MenuBar::updateHotkeys()
	{
		hotkeysList = HotkeysEditor::getInstance()->getHotkeys(this);
		foreach(auto obj,shortcutsList){
			obj->setShortcut(hotkeysList.value(obj->objectName()).key_sequence);
		}
	}
/* ============================================================================================================================= */

void MenuBar::setAppearance() {
    connect(appearanceAct, &QAction::triggered, Appearance::getInstance(), &Appearance::show);
}

/* ============================================================================================================================= */

void MenuBar::openFileByPath(QString filePath, bool openInHex) {

    fileOpen->setMdiArea(mdiArea);
    fileOpen->setAppearance(appearance);

    if (filePath.isEmpty()) return;
    if (QMdiSubWindow *existing = findMdiChild(filePath)) {
        mdiArea->setActiveSubWindow(existing);
        return;
    }
    fileOpen->setFilePath(filePath, openInHex);
}

/* ============================================================================================================================= */

void MenuBar::activeWindowChanged(QMdiSubWindow *activeWindow) {
    if(activeWindow){
        setSyntaxMenu();
        setAutocompletionMenu();
        setNumberLineMenu();
        setColorWidgetMenu();

        QString className = activeWindow->widget()->metaObject()->className();
        if (className == "TextEdit"){
            setEnableInMenuList(true);
        }
        else{
            setEnableInMenuList(false);
            resetCheksInMenu();
        }
    }else{
        if(mdiArea->subWindowList().isEmpty())
        {
            emit concealmenSignalWW();
            setEnableInMenuList(false);
            resetCheksInMenu();
        }
	}
}
void MenuBar::resetCheksInMenu()
{
    colorWidget->setChecked(false);
    buttonVisibleLine->setChecked(false);
}
void MenuBar::setEnableInMenuList(bool status)
{
    syntaxMenu->setEnabled(status);
    autocompletionMenu->setEnabled(status);
    buttonVisibleLine->setEnabled(status);
    colorWidget->setEnabled(status);
}
/* ============================================================================================================================= */

void MenuBar::setMdiArea(QMdiArea *value) {
    mdiArea = value;
    mdiArea->setWindowIcon(
       QIcon(QPixmap(0,0))
    );
}

/* ============================================================================================================================= */

void MenuBar::setWorkspaceSplitter(WorkspaceSplitter *value) {
    workspaceSplitter = value;
}

/* ============================================================================================================================= */

QMdiSubWindow *MenuBar::findMdiChild(const QString &fileName) const {
    QString canonicalFilePath = QFileInfo(fileName).canonicalFilePath();
    const QList<QMdiSubWindow *> subWindows = mdiArea->subWindowList();
    for (QMdiSubWindow *window : subWindows) {
        QString className = window->widget()->metaObject()->className();
        if (className == "TextEdit") {
            TextEdit *textEdit = qobject_cast<TextEdit*>(window->widget());
            if (textEdit->buffer()->pathFile() == canonicalFilePath) {
                return window;
            }
        } else if (className == "ImageViewer") {
            ImageViewer *imgViewer = qobject_cast<ImageViewer*>(window->widget());
            if (imgViewer->getFilePath() == canonicalFilePath) {
                return window;
            }
        } else if (className == "HexEdit") {
            HexEdit *hexEdit = qobject_cast<HexEdit*>(window->widget());
            if (hexEdit->buffer()->pathFile() == canonicalFilePath) {
                return window;
            }
        }
    }
    return nullptr;
}

void MenuBar::setSyntaxMenu()
{
    if(activeBuffer()) {
        for(QAction *item : syntaxGroup->actions()) {
            if(activeBuffer()->buffer()->getHighLighter()->getName() == item->text()) {
                item->setChecked(true);
                break;
            } else {
                noneAct->setChecked(true);
            }
        }
    }
    else {
        noneAct->setChecked(true);
    }
}

void MenuBar::setAutocompletionMenu()
{
    if(activeBuffer()){
        if(activeBuffer()->getAutoCompletition()=="cpp") {
            autoCompletionCpp->setChecked(true);
        } else if (activeBuffer()->getAutoCompletition()=="text") {
            autoCompletionText->setChecked(true);
        } else {
            autoCompletionOff->setChecked(true);
        }
    }
    else {
        autoCompletionOff->setChecked(true);
    }
}

void MenuBar::setNumberLineMenu()
{
    if (activeBuffer()){
        buttonVisibleLine->setChecked(activeBuffer()->getVisibleLine());
    }
    else {
        buttonVisibleLine->setChecked(false);
    }
}
void MenuBar::setColorWidgetMenu()
{
    if (activeBuffer()){
        colorWidget->setChecked(activeBuffer()->getVisibleColorWidget());
    }
    else {
        colorWidget->setChecked(false);
    }
}
/* ============================================================================================================================= */

FileOpen *MenuBar::getFileOpen() {
    return fileOpen;
}
