#ifndef MENUBAR_H
#define MENUBAR_H

#include <QObject>
#include <QMenuBar>
#include <QMdiArea>
#include <QPointer>
#include <QPrinter>
#include <QPrintDialog>
#include <QFileDialog>

#include "Style/appearance.h"
#include "Widgets/nixconsole.h"
#include "Widgets/winconsole.h"
#include "fileopen.h"
#include "hexedit.h"
#include "imageviewer.h"
#include "workspacesplitter.h"
#include "Widgets/hotkeyseditor.h"
class FileOpen;
class TextEdit;

class MenuBar : public QMenuBar,public IHotkeys {
    Q_OBJECT

public:
    explicit MenuBar(QWidget *parent = nullptr);
    void setWorkspaceSplitter(WorkspaceSplitter *value);
    void setMdiArea(QMdiArea *value);

    void setAppearance();
    FileOpen* getFileOpen();


private:
	const QString visibleName = QString(tr("Main menu"));
    QMenu *fileMenu;                                                        // Меню «Файл»
    QAction *newAct;                                                        // Пункт «Новый»
    QAction *openAct;                                                       // Пункт «Открыть»
    QAction *saveAct;                                                       // Пункт «Сохранить»
    QAction *saveAsAct;                                                     // Пункт «Сохранить как»
    QAction *printAct;                                                      // Пукнт «Напечатать»
    QAction *exitAct;                                                       // Пункт «Выйти»

    QMenu *editMenu;                                                        // Меню «Правка»
    QAction *cutAct;                                                        // Пункт «Вырезать»
    QAction *copyAct;                                                       // Пункт «Скопировать»
    QAction *pasteAct;                                                      // Пункт «Вставить»

    QMenu *syntaxMenu;                                                      // Меню «Синтаксис»
    QActionGroup *syntaxGroup;                                              // Группа пунктов «Синтаксиса»
    QAction *noneAct;                                                       // Пункт «Никакой»
    JsonClass *options;


    QMenu *autocompletionMenu;
    QActionGroup *autocompletionGroup;
    QAction *autoCompletionText;                                            // Пункт «Text» для автодополнения
    QAction *autoCompletionCpp;                                             // Пункт «С++»  для автодополнения
    QAction *autoCompletionOff;                                             // Пункт «OFF»  для автодополнения

    QMenu *instruments;                                                     // Меню «Инструменты»
    QAction *consoleAct;                                                    // Пункт «Консоль»
    QAction *lineIndexAct;                                                  // Пункт «Нумерация строк»
    QAction *colorWidget;                                                   // Пункт «Виджет цвета»
    QAction *appearanceAct;                                                 // Пункт «Вид»
	QAction *hotkeysEditorAct;                                              // Пункт «Редактор горячих клавиш»
    QPointer<QAction> buttonVisibleLine = nullptr;

    QMenu *helpMenu;                                                        // Меню «Справка»
    QAction *aboutAct;                                                      // Пункт «О Yate»

    TextEdit *activeBuffer() const;

    WorkspaceSplitter *workspaceSplitter;                                   // Рабочая область. Сюда можно добавлять свои виджеты
    QMdiArea *mdiArea;
    Appearance *appearance;                                                 // Внешний вид приложения
	HotkeyEditor_MAP hotkeysList;
	QList<QAction*> shortcutsList;

    QString m_checkSyntax;
    QMdiSubWindow *findMdiChild(const QString &fileName) const;
    FileOpen *fileOpen;

    void setEnableInMenuList(bool status);
    void resetCheksInMenu();
    void setSyntaxMenu();
    void setColorWidgetMenu();
    void setAutocompletionMenu();
    void setNumberLineMenu();
	void initHotkeys() override;


public slots:
    void activeWindowChanged(QMdiSubWindow *activeWindow);
    void openFileByPath(QString filePath, bool openInHex = false);
	void updateHotkeys() override;

private slots:
    void saveFile();
    void saveAsFile();
    void print();
    void newFile();
    void openFile();
    void cut();
    void copy();
    void paste();
    void about();
    void addConsole();
    void switchNumberLine();
    void switchColorWidget();
    void setSyntax();
    void setAutocompletion();
signals:
	void concealmenSignalWW();
};

#endif // MENUBAR_H
