#include "graphicsview.h"

#include <QtCore>

graphicsView::graphicsView(QWidget *parent) : QGraphicsView(parent) {
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);                             // Отключим скроллбар по горизонтали
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);                               // Отключим скроллбар по вертикали
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);                    // Растягиваем содержимое по виджету
    this->setAlignment(Qt::AlignCenter);                                                    // Делаем привязку содержимого к центру
    this->setMinimumWidth(640);                                                             // Минимальная ширина окна
    this->setMinimumHeight(480);                                                            // Минимальная высота окна

    scene = new QGraphicsScene(this);
    gItem = new graphicsItem(this);

    connect(gItem, SIGNAL(reDraw()), this, SLOT(reDraw()));
    scene->addItem(gItem);

    gItem->setPos(this->width() / 2, 430.);
    logo = nullptr;

    setScene(scene);
    show();

    connect(Appearance::getInstance(),SIGNAL(onUpdate(const Appearance *)), this, SLOT(Style()));
}

/* ============================================================================================================================= */

graphicsView::~graphicsView() {

}

/* ============================================================================================================================= */

void graphicsView::reDraw() {
    scene->update();
    update();
}

/* ============================================================================================================================= */

void graphicsView::Style() {

    if(logo)
        scene->removeItem(logo);

    QPixmap qpix;
    if (Appearance::getInstance()->getThemeManager()->getCurrentTheme()->isDark()) {
        qpix = QPixmap(":/images/logo_white.svg");
    } else {
        qpix = QPixmap(":/images/logo_black.svg");
    }

    logo = scene->addPixmap(qpix);
    logo->setTransformationMode(Qt::SmoothTransformation);
    logo->setPos(0.0, 0.0);
    logo->setScale(0.25);
    fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
}

void graphicsView::resizeEvent(QResizeEvent *event) {
		Q_UNUSED(event);
		fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
}
