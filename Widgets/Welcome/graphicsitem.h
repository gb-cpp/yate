#ifndef NEWWIDGET_H
#define NEWWIDGET_H

#include <QObject>
#include <QGraphicsItem>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QBrush>
#include <QPainter>

/* QGraphicsItem - базовый класс для всех графических элементов ================================================================= */

class graphicsItem : public QObject, public QGraphicsItem {
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)

public:
    graphicsItem(QObject* parent = nullptr);
    ~graphicsItem() override;
    void setBrush(QBrush brush) {
        this->brush = brush;
        emit reDraw();
    }

    int width() const {
        return br.width();
    }

    int height() const {
        return br.height();
    }

signals: 
    void reDraw();

private:
    void readFile();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
    QRectF boundingRect() const override;
    int x, y;
    int Wcount = 0;
    QBrush brush;
    QTimer *Wtimer;
    QList<QString> list;
    QRect br;
};

#endif // NEWWIDGET_H
