#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QGraphicsView>

#include "Style/appearance.h"
#include "graphicsitem.h"

class Appearance;

class graphicsView: public QGraphicsView {
    Q_OBJECT

public:
    graphicsView(QWidget *parent =nullptr);
    ~graphicsView()override;

signals:
    void updateTimer();

private slots:
    void reDraw();
    void Style();

private:
    void resizeEvent(QResizeEvent *event) override;
    QGraphicsScene *scene;                                                              // Объявляем сцену для отрисовки
    QGraphicsItem *group1;                                                              // Объявляем группу элементов
    QGraphicsWidget *pWidget1;
    graphicsItem *gItem;
    QStyle *style;
    QGraphicsPixmapItem *logo;
    QGraphicsTextItem *pTextItem1;
    QGraphicsTextItem *pTextItem2;
    QString strYate;
    QString str1;
};

#endif // MYGRAPHICSVIEW_H
