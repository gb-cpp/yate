#include "graphicsitem.h"
#include <QDebug>

graphicsItem::graphicsItem(QObject *parent) : QObject(parent), QGraphicsItem() {
    readFile();
    x = 0;
    y = 0;
    brush.setColor(QColor(255, 255, 255));
    brush.setStyle(Qt::BrushStyle::SolidPattern);                                                   // Полностью закрашивать
    Wtimer = new QTimer;
    Wtimer->start();
    Wtimer->setInterval(3000);
    connect(Wtimer, &QTimer::timeout, [&] () {
        (Wcount < (list.size() - 1)) ? Wcount++ : Wcount = 0;
        emit reDraw();
    });
}

/* ============================================================================================================================= */

graphicsItem::~graphicsItem() {

}

/* ============================================================================================================================= */

void graphicsItem::readFile() {
    QFile pFile(":/hotkeys.txt");
    QString str="";
    QTextStream stream(&pFile);

    list.clear();

    if(!pFile.open(QIODevice::ReadOnly|QIODevice::Text))
        return;

    while(!stream.atEnd()) {
        str = stream.readLine();
        list.append(str);
        str.clear();
    }

    pFile.close();
}

/* ============================================================================================================================= */

void graphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {


    br = painter->boundingRect(x,y,100,100,0,list.at(Wcount));
    painter->setBrush(brush);
    painter->drawText(x - br.width()/2, y, list.at(Wcount));

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

/* ============================================================================================================================= */

QRectF graphicsItem::boundingRect() const {
    return QRectF(x, y, 100, 100);
}
