#include "winconsole.h"

winConsole::winConsole(QWidget *parent) : QPlainTextEdit(parent) {
    m_process = new QProcess(this);
    clipboard = QApplication::clipboard();
    cursor = QTextCursor(this->textCursor());
    output = "";
    codec = QTextCodec::codecForName("IBM 866");
    connect(m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(outConsole()));
    connect(m_process, SIGNAL(readyReadStandardError()), this, SLOT(outErrortoConsole()));
    m_process->start("cmd /S");
    setObjectName("Console");
}

/* ============================================================================================================================= */

winConsole::~winConsole() {
    m_process->close();
    m_process = nullptr;
    clipboard = nullptr;
    codec = nullptr;
}

/* ============================================================================================================================= */

void winConsole::on_keyEnter_clicked() {
    insertPlainText("\n");
    QByteArray arr = userText.toUtf8() + '\n';
    m_process->write(arr);
    userText = "";
}

/* ============================================================================================================================= */

void winConsole::mouseDoubleClickEvent(QMouseEvent *)
{

}

/* ============================================================================================================================= */

void winConsole::contextMenuEvent(QContextMenuEvent *)
{

}

/* ============================================================================================================================= */

void winConsole::on_keyBackspace_clicked() {
    if (!userText.isEmpty()) {
        if (this->textCursor().selectedText().size() > 0) {
            cursor.movePosition(QTextCursor::EndOfLine, QTextCursor::MoveAnchor);
            this->setTextCursor(cursor);
        }
        int pos = userText.size();
        userText = userText.remove(pos - 1, 1);
        cursor.deletePreviousChar();
    }
}

/* ============================================================================================================================= */

void winConsole::moveCursor(QTextCursor::MoveOperation operation) {
    cursor.movePosition(operation, QTextCursor::MoveAnchor, 1);
    this->setTextCursor(cursor);
}

/* ============================================================================================================================= */

void winConsole::outConsole() {
    output = m_process->readAllStandardOutput();
    insertPlainText(codec->toUnicode(output));
    verticalScrollBar()->setSliderPosition(this->verticalScrollBar()->maximum());
    userText = "";
}

/* ============================================================================================================================= */

void winConsole::outErrortoConsole() {
    output = m_process->readAllStandardError();
    appendHtml(QString("<font color='red'>%1</font>").arg(codec->toUnicode(output)));
    userText = "";
}

/* ============================================================================================================================= */

void winConsole::keyPressEvent(QKeyEvent *event) {
    switch (event->key()) {
        case Qt::Key_Return:
            on_keyEnter_clicked();
            break;
        case Qt::Key_Backspace:
            on_keyBackspace_clicked();
            break;
        case Qt::Key_Up:
            moveCursor(QTextCursor::Up);
            break;
        case Qt::Key_Down:
            moveCursor(QTextCursor::Down);
            break;
        case Qt::Key_Left:
            moveCursor(QTextCursor::Left);
            break;
        case Qt::Key_Right:
            moveCursor(QTextCursor::Right);
            break;
        default:
            if (event->matches(QKeySequence::Copy)) {
                QString selectedText = this->textCursor().selectedText();
                clipboard->setText(selectedText);
            } else if (event->matches(QKeySequence::Paste)) {
                insertPlainText(clipboard->text());
        } else {
            if (event->key() == Qt::Key_Control) return;
            if (!(this->textCursor().position() < this->toPlainText().size())) {
                userText += event->text();
                insertPlainText(event->text());
            }
        }
    }
}

/* ============================================================================================================================= */

QString winConsole::getQplainTest()
{
    QTextCodec *codec = QTextCodec::codecForName("IBM 866");
    return QString(codec->toUnicode(output));
}

