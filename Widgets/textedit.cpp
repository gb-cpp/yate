#include "textedit.h" 

TextEdit::TextEdit(Appearance *appearance, Buffer *buffer)
    : m_buffer(buffer)
    , m_appearance(appearance)
    , VisibleAutocompletionText(false)
    , VisibleAutocompletionCpp(false)
    , VisibleAutocompletionOff(true)
{
    colorWidget = nullptr;
    connect(this, &TextEdit::cursorPositionChanged, this, &TextEdit::deleteColorWidget);

    VisibleLineNumber = false;
    visibleColorWidget = false;
    lineNumberArea = new LineNumberArea(this);
    connect(this, &TextEdit::blockCountChanged, this, &TextEdit::updateLineNumberAreaWidth);
    connect(this, &TextEdit::updateRequest, this, &TextEdit::updateLineNumberArea);
    connect(this, &TextEdit::cursorPositionChanged, this, &TextEdit::highlightCurrentLine);
    updateLineNumberAreaWidth();
    highlightCurrentLine();
    setAttribute(Qt::WA_DeleteOnClose);
    connect(m_buffer, &Buffer::createFile, this, &TextEdit::newFile);
    connect(m_buffer, &Buffer::openFile, this, &TextEdit::loadFile);
    connect(m_buffer, &Buffer::errorWithFile, this, &TextEdit::showErrorMsg);
    if (m_buffer->pathFile().isEmpty()) m_buffer->setPathFile("");
    if (!m_buffer->pathFile().isEmpty()) m_buffer->setPathFile(m_buffer->pathFile());
    int fontWidth = QFontMetrics(this->currentCharFormat().font()).horizontalAdvance(' ');
    setTabStopDistance(8 * fontWidth);                                                                              // Qt 5.10+
    setMouseTracking(true);
    // Включение автодополнения
    if(VisibleAutocompletionText == true && VisibleAutocompletionCpp == false){
        disconnect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionCpp);
        connect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionText);
    } else if(VisibleAutocompletionText == false && VisibleAutocompletionCpp == true) {
        disconnect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionText);
        connect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionCpp);
    } else if(VisibleAutocompletionOff == true){
        disconnect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionCpp);
        disconnect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionText);
        this->setCompleter(nullptr);
    }
    disconnect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionCpp);
    disconnect(this, &TextEdit::blockCountChanged, this, &TextEdit::setAutocompletionText);
}

/* ============================================================================================================================= */

TextEdit::~TextEdit() {
    deleteBuffer(m_buffer);
}

/* ============================================================================================================================= */

Buffer *TextEdit::buffer() const {
    return m_buffer;
}

/* ============================================================================================================================= */

void TextEdit::setBuffer(Buffer *buffer) {
    m_buffer = buffer;
}

/* ============================================================================================================================= */

void TextEdit::newFile() {
    setWindowTitle(m_buffer->fileName() + "[*]");
    connect(document(), &QTextDocument::contentsChanged, this, &TextEdit::documentWasModified);
}

/* ============================================================================================================================= */

bool TextEdit::saveFile() {
    if (m_buffer->hasPath()) {
        QGuiApplication::setOverrideCursor(Qt::WaitCursor);
        m_buffer->save(toPlainText());
        QGuiApplication::restoreOverrideCursor();
        document()->setModified(false);
        setWindowModified(false);
        HighLighterText();
        setWindowTitle(m_buffer->fileName() + "[*]");
        return true;
    } else {
        return saveAsFile();
    }
}

/* ============================================================================================================================= */

void TextEdit::loadFile() {
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    setPlainText(m_buffer->getTextAll());
    QGuiApplication::restoreOverrideCursor();
    document()->setModified(false);
    setWindowModified(false);
    HighLighterText();
    setWindowTitle(m_buffer->fileName() + "[*]");
    connect(document(), &QTextDocument::contentsChanged, this, &TextEdit::documentWasModified);
    textCursor().setPosition(0);
}

/* ============================================================================================================================= */

void TextEdit::documentWasModified() {
    setWindowModified(document()->isModified());
}

/* ============================================================================================================================= */

void TextEdit::showErrorMsg(const QString &err) {
    QMessageBox::warning(this, tr("MDI"), err);
}

/* ============================================================================================================================= */

bool TextEdit::maybeSave() {
    if (!document()->isModified()) return true;
    const QMessageBox::StandardButton ret = QMessageBox::warning(this, tr("MDI"),
                                                                tr("'%1' has been modified.\n"
                                                                "Do you want to save your changes?")
                                                                .arg(m_buffer->fileName()),
                                                                QMessageBox::Save | QMessageBox::Discard
                                                                | QMessageBox::Cancel);
    switch (ret) {
        case QMessageBox::Save:
            return saveFile();
        case QMessageBox::Cancel:
            return false;
        default:
            break;
    }
    return true;
}

/* ============================================================================================================================= */

bool TextEdit::saveAsFile() {
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), m_buffer->pathFile(), "Text files (*.txt);;All files (*)");
    if (fileName.isEmpty()) return false;
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    m_buffer->save(fileName, toPlainText());
    QGuiApplication::restoreOverrideCursor();
    document()->setModified(false);
    setWindowModified(false);
    HighLighterText();
    setWindowTitle(m_buffer->fileName() + "[*]");
    return true;
}

/* ============================================================================================================================= */

void TextEdit::closeEvent(QCloseEvent *event) {
    if (maybeSave()) event->accept();
    else event->ignore();
}

void TextEdit::mouseMoveEvent(QMouseEvent *e)
{
    if(visibleColorWidget)
    {
        QTextCursor textCursor = cursorForPosition(e->pos());
        textCursor.select(QTextCursor::WordUnderCursor);
        QString word = textCursor.selectedText();
        QRegularExpression exp("[a-fA-F0-9]{6}");
        deleteColorWidget();
        if(toPlainText().mid(textCursor.selectionStart()-1,1) == "#" && cursorRect(textCursor).bottomRight().x() >= e->pos().x()
               && word.length() == 6 && cursorRect(textCursor).bottomRight().y() >= e->pos().y())
        {
            if(word.indexOf(exp) != -1 && colorWidget == nullptr)
            {
                colorWidget = new QWidget(this);
                colorWidget->setGeometry(e->pos().x()+10, e->pos().y()+10, 25, 25);
                colorWidget->setStyleSheet("border-width: 1px; border-style: solid; border-color: black; background: #"+word);
                colorWidget->show();
            }
        }
    }
    TextEdit::mousePressEvent(e);
}

/* ============================================================================================================================= */

void TextEdit::HighLighterText() {
    Brackets *brck = new Brackets(document());
    connect(this, &TextEdit::cursorPositionChanged, brck, &Brackets::matchBrackets);
    m_buffer->getHighLighter()->setDocument(document());
}

/* ============================================================================================================================= */

int TextEdit::lineNumberAreaWidth() {
    int lastLineNumber = blockCount();
    int numDigitsInLastLine = QString::number(lastLineNumber).length();
    int maxWidthOfAnyDigit = fontMetrics().QFontMetrics::horizontalAdvance(QLatin1Char('9'));
    if(VisibleLineNumber) {
        return numDigitsInLastLine * maxWidthOfAnyDigit + lineNumberAreaPadding;
    } else {
        return 0;
    }
}

/* ============================================================================================================================= */

void TextEdit::updateLineNumberAreaWidth() {
    auto val = VisibleLineNumber ? ( lineNumberAreaWidth() + lineNumberAreaPadding ): lineNumberAreaWidth();
    setViewportMargins( val, 0, 0, 0 );
}

/* ============================================================================================================================= */

void TextEdit::updateLineNumberArea(const QRect &rect, int numPixelsScrolledVertically) {
    if (numPixelsScrolledVertically) lineNumberArea->scroll(0, numPixelsScrolledVertically);
    else lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());
    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth();
}

/* ============================================================================================================================= */

void TextEdit::resizeEvent(QResizeEvent *event) {
    QPlainTextEdit::resizeEvent(event);
    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

/* ============================================================================================================================= */

void TextEdit::highlightCurrentLine() {
    QList<QTextEdit::ExtraSelection> extraSelections;
    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;

        QColor lineColor =  QColor(0, 0, 255, 127).lighter(160);
        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }
    setExtraSelections(extraSelections);
}

/* ============================================================================================================================= */

void TextEdit::lineNumberAreaPaintEvent(QPaintEvent *event) {
    QPainter painter(lineNumberArea);
    painter.fillRect(event->rect(),Appearance::getInstance()->getThemeManager()->getCurrentTheme()->getLineNumberBackground());
    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = qvariant_cast<int>(blockBoundingGeometry(block).translated(contentOffset()).top());
    int bottom = top + qvariant_cast<int>(blockBoundingRect(block).height());
    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Appearance::getInstance()->getThemeManager()->getCurrentTheme()->getLineNumberColor());
            painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(), Qt::AlignCenter, number);
        }
        block = block.next();
        top = bottom;
        bottom = top + qvariant_cast<int>(blockBoundingRect(block).height());
        ++blockNumber;
    }
}

/* ============================================= Автодополнение ===================================================== */

QAbstractItemModel *TextEdit::modelFromFile(const QString& fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
        return new QStringListModel(c);

#ifndef QT_NO_CURSOR
    QGuiApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
#endif
    QStringList words;

    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        if (!line.isEmpty())
            words << QString::fromUtf8(line.trimmed());
    }

#ifndef QT_NO_CURSOR
    QGuiApplication::restoreOverrideCursor();
#endif
    return new QStringListModel(words, c);
}

void TextEdit::setCompleter(QCompleter *completer)
{
    if (c)
        c->disconnect(this);

    c = completer;

    if (!c)
        return;

    c->setWidget(this);
    c->setCompletionMode(QCompleter::PopupCompletion);
    c->setCaseSensitivity(Qt::CaseInsensitive);
    QObject::connect(c, QOverload<const QString &>::of(&QCompleter::activated),
                     this, &TextEdit::insertCompletion);
}

QCompleter *TextEdit::completer() const
{
    return c;
}

QString TextEdit::getAutoCompletition()
{
    if(VisibleAutocompletionText)
        return "text";
    else if(VisibleAutocompletionCpp)
        return "cpp";
    else
        return nullptr;
}

void TextEdit::setAutocompletionText()          // Используем файл-словарь Text
{
    if(VisibleAutocompletionText == true){
    c = new QCompleter(this);
    QString source_file_Text = ":/autocompletion/wordlist.txt";
    QString dictionary_file_Text = "./wordlist.txt";
    // Проверяем наличие исходного файла
    if (!QFile(dictionary_file_Text).exists()) {
        qWarning("The source file does not exist"); // если файл не найден, то выводим предупреждение
        // Копируем исходный файл
        QFile::copy(source_file_Text, dictionary_file_Text); // первый параметр - это имя исходного файла, а второй - имя конечного файла
    }
    c->setModel(modelFromFile(dictionary_file_Text));
    c->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    c->setCaseSensitivity(Qt::CaseInsensitive);
    c->setWrapAround(true);
    this->setCompleter(c);
    }

    if (VisibleAutocompletionText == false){
        this->setCompleter(nullptr);
    }
}

void TextEdit::setAutocompletionCpp()           // Используем файл-словарь С++
{
    if(VisibleAutocompletionCpp == true){
    c = new QCompleter(this);
    QString source_file_Cpp = ":/autocompletion/wordlistCpp.txt";
    QString dictionary_file_Cpp = "./wordlistCpp.txt";
    // Проверяем наличие исходного файла
    if (!QFile(dictionary_file_Cpp).exists()) {
        qWarning("The source file does not exist"); // если файл не найден, то выводим предупреждение
        // Копируем исходный файл
        QFile::copy(source_file_Cpp, dictionary_file_Cpp); // первый параметр - это имя исходного файла, а второй - имя конечного файла
    }
    c->setModel(modelFromFile(dictionary_file_Cpp));
    c->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    c->setCaseSensitivity(Qt::CaseInsensitive);
    c->setWrapAround(true);
    this->setCompleter(c);
    }

    if (VisibleAutocompletionCpp == false){
        this->setCompleter(nullptr);
    }
}

void TextEdit::deleteColorWidget()
{
    if(colorWidget != nullptr)
    {
        colorWidget->hide();
        delete colorWidget;
        colorWidget = nullptr;
    }
}


void TextEdit::insertCompletion(const QString &completion) // Вставляем текст
{
    if (c->widget() != this)
        return;
    QTextCursor tc = textCursor();
    tc.movePosition(QTextCursor::StartOfWord);              // Помещаем курсор в начало слова
    tc.select(QTextCursor::WordUnderCursor);                // Выделяем текст под курсором
    tc.removeSelectedText();                                // Удаляем выделенный текст
    tc.insertText(completion);                              //Вставляем выбранное слов
    tc.movePosition(QTextCursor::EndOfWord);                // Перемещаем курсор в конец слова
    setTextCursor(tc);
}

QString TextEdit::textUnderCursor() const
{
    QTextCursor tc = textCursor();
    tc.select(QTextCursor::WordUnderCursor);
    return tc.selectedText();
}

void TextEdit::focusInEvent(QFocusEvent *e)
{
    if (c)
        c->setWidget(this);
    QPlainTextEdit::focusInEvent(e);
}

void TextEdit::keyPressEvent(QKeyEvent *e)
{    
    if (c && c->popup()->isVisible()) {
       switch (e->key()) {
       case Qt::Key_Enter:
       case Qt::Key_Return:
       case Qt::Key_Escape:
       case Qt::Key_Tab:
       case Qt::Key_Backtab:
            e->ignore();
            return;
       default:
           break;
       }
    }

    const bool isShortcut = (e->modifiers().testFlag(Qt::ControlModifier) && e->key() == Qt::Key_Space); // CTRL+Space
    if (!c || !isShortcut)
        QPlainTextEdit::keyPressEvent(e);

    const bool ctrlOrShift = e->modifiers().testFlag(Qt::ControlModifier) ||
                             e->modifiers().testFlag(Qt::ShiftModifier);
    if (!c || (ctrlOrShift && e->text().isEmpty()))
        return;

    static QString eow("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-=");
    const bool hasModifier = (e->modifiers() != Qt::NoModifier) && !ctrlOrShift;
    QString completionPrefix = textUnderCursor();

    if (!isShortcut && (hasModifier || e->text().isEmpty()|| completionPrefix.length() < 2
                      || eow.contains(e->text().right(1)))) {
        c->popup()->hide();
        return;
    }

    if (completionPrefix != c->completionPrefix()) {
        c->setCompletionPrefix(completionPrefix);
        c->popup()->setCurrentIndex(c->completionModel()->index(0, 0));
    }
    QRect cr = cursorRect();
    cr.setWidth(c->popup()->sizeHintForColumn(0)
                + c->popup()->verticalScrollBar()->sizeHint().width());
    c->complete(cr);
}
