/* ============================================================================================================================= */
/*
 *
 ***		Для дополнительных примеров см. реализации в классах menubar и imageviewer
 *
 *
 * Для включения поддержки редактора клавиш в виджете необходимо:
 * --- Подключить этот заголовочный файл (#include "Widgets/hotkeyseditor.h")
 * --- (Необязательно) Унаследоваться от виртуального класса IHotkeys
 *
 * --- Объявить 2 списка:
 *		QMap<QString,HotkeyEditor_shortcut> hotkeysList;
 *		QList<* > shortcutsList;  <-- Объекты в QList могут быть любыми, например QAction или QShortcut
 *
 *** Структура HotkeyEditor_shortcut:
 *		QString visible_name - имя горячей клавиши, которое будет видно пользователю. Если не указано, будет использован ключ (QMap) к этой горячей клавише. (может быть полезно при отладке)
 *		QString group_name - имя группы горячей клавиши
 *		QKeySequence key_sequence - последовательность горячих клавиш
 *
 * --- Переопределить слот updateHotkeys()
 *		Этот слот вызывается каждый раз, когда в редакторе происходят изменения (пользовательские или подгрузка из файла)
 *		Необходимо реализовать получение списка горячих клавиш с помощью метода HotkeysEditor::getHotkeys(this)
 *			Пример из класса ImageViewer:
 *				hotkeysList = HotkeysEditor::getInstance()->getHotkeys(this);
 *				foreach(auto obj,shortcutsList){
 *					obj->setKey(hotkeysList.value(obj->objectName()).key_sequence);
 *				}
 *
 * --- Переопределить метод initHotkeys()
 *		В этом методе преполагается инициализация горячих клавиш виджета и отправка их редактору с помощью метода HotkeysEditor::sendHotkeys.
 *				HotkeysEditor::sendHotkeys(
 *					QWidget* widget, - указатель на виджет (this)
 *					const QString visibleName,	- имя, видимое пользвателю в списке виджетов
 *					QMap<QString,HotkeyEditor_shortcut> hotkeysList, - список горячих клавиш виджета
 *					qint32 weight - (необзятельно) "вес" виджета в списке (чем меньше значение, тем выше будет виджет в списке)
 *				)
 *		В реализации необходимо прописать подключение к слоту updateHotkeys() и отправку списка горячих клавиш.
 *			Пример из класса ImageViewer:
 *				connect(HotkeysEditor::getInstance(),&HotkeysEditor::updated,this,&ImageViewer::updateHotkeys);
 *				HotkeysEditor::getInstance()->sendHotkeys(this,visibleName,hotkeysList);
 *
 *
 * --- Добавить классу Initialization в метод initHotkeys() строчку по образцу:
 *
 *					Widget();
 *
 *	   Это необходимо для правильной инициализации стандартных горячих клавиш и видимых имен
 */
/* ============================================================================================================================= */
#ifndef HOTKEYSEDITOR_H
#define HOTKEYSEDITOR_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QShowEvent>
#include <QMap>
#include <QString>
#include <QListView>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QKeySequenceEdit>
#include <QStackedWidget>
#include <QScrollArea>
#include "settingsloader.h"
#include <QDebug>
/* базовый контейнер горячих клавиш */
/* ============================================================================================================================= */
typedef struct HotkeyEditor_shortcut {
	QString visible_name = QString();
	QString group_name = QString();
	QKeySequence key_sequence = QKeySequence();
	inline bool operator==(HotkeyEditor_shortcut const& rhs)const{
			if(visible_name == rhs.visible_name && group_name == rhs.group_name) return true; else return false;
		}
	inline bool operator!=(HotkeyEditor_shortcut const& rhs)const{
			if(visible_name == rhs.visible_name && group_name == rhs.group_name) return false; else return true;
		}
}HotkeyEditor_shortcut;

/* ============================================================================================================================= */
// Интерфейсный класс для удобства
class IHotkeys
{
protected:
	virtual ~IHotkeys() = default;
	virtual void initHotkeys() = 0;
public:
	IHotkeys& operator=(const IHotkeys&) = delete;
public:
	virtual void updateHotkeys() = 0;
};

/* ============================================================================================================================= */
namespace Ui {
	class HotkeysEditor;
}
class HotkeysEditor : public QDialog
{
	friend class Test_HotkeysEditor;
	Q_OBJECT
	#define HotkeyEditor_MAP QMap<QString,HotkeyEditor_shortcut>
public:
	static HotkeysEditor *getInstance(QWidget *parent = nullptr){
			if(instance == nullptr){
				instance = new HotkeysEditor(parent);
			}else if(instance->parent() == nullptr && parent != nullptr){
				delete instance;
				instance = new HotkeysEditor(parent);
			}
			return instance;
		}
	HotkeyEditor_MAP getHotkeys(QWidget* widget);
	void sendHotkeys(QWidget* widget, const QString visibleName, HotkeyEditor_MAP hotkeysList, quint32 weight = 100);
	void initHotkeyEditor();
protected:

	void showEvent(QShowEvent *event) override ;
	void closeEvent(QCloseEvent *event) override;
private:
	static HotkeysEditor *instance;
	HotkeysEditor(QWidget *parent = nullptr);
	HotkeysEditor(HotkeysEditor const&) = delete;
	HotkeysEditor& operator=(HotkeysEditor const&) = delete;
	~HotkeysEditor();
	bool first = true;
	/* ============================================================================================================================= */
	/* константы класса */

	const QString mainSettings = "hotkeys.json"; //имя файла json с горячими клавишами
	QString titleName = tr("Hotkeys Editor"); //Заголовок окна

	/* ============================================================================================================================= */
	Ui::HotkeysEditor *ui;
	QStandardItemModel *mainModel = nullptr;

	typedef struct widget_info{
		QString visible_name = QString();
		HotkeyEditor_MAP hotkeys_list;
		inline bool operator==(widget_info const& rhs)const{
				if(hotkeys_list == rhs.hotkeys_list) return true; else return false;
			}
		inline bool operator!=(widget_info const& rhs)const{
				if(hotkeys_list == rhs.hotkeys_list) return false; else return true;
			}
	}widget_info;

	QMap<QString, widget_info> mainContainer;
	QMap<QString, widget_info> defaultsContainer;
	QMap<QString, qint32> weights;
	void rebuildScreen();
	void saveHotkeysToFile();
	void loadSettingsFile(QMap<QString, widget_info> *container);
	SettingsLoader *settingsLoader = nullptr;
	QStackedWidget *mainWidget = nullptr;
	void checkConflict(QKeySequenceEdit* kse);
	void checkConflict(QWidget* contentWindow);
private slots:
	void kseFinishedEditing(QKeySequenceEdit* kse, QWidget* contentWindow);
	void resetDefault(QKeySequenceEdit* kse, QWidget* contentWindow);
	void clearKSE(QKeySequenceEdit* kse, QWidget* contentWindow);
	void on_listView_activated(const QModelIndex &index);

	void on_listView_clicked(const QModelIndex &index);

signals:
	void updated();
};

#endif // HOTKEYSEDITOR_H
