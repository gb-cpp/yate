#ifndef WORKSPACESPLITTER_H
#define WORKSPACESPLITTER_H

#include <QWidget>
#include <QSplitter>
#include <QVector>
#include <QVBoxLayout>
#include <QPushButton>

class WorkspaceSplitter : public QSplitter {
    Q_OBJECT

public:
    explicit WorkspaceSplitter(Qt::Orientation orientation);
    void addNewWidget(QWidget *widget);

private:
    QVector<QWidget *> *pWidgets;                                              // Здесь хранятся указатели на WidgetFoundation
    QVector<QWidget *> *vector;
};

#endif // WORKSPACESPLITTER_H
