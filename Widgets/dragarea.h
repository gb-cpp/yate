#ifndef DRAGAREA_H
#define DRAGAREA_H

#include <QWidget>
#include <QDropEvent>
#include <QLabel>
#include <QMimeData>
#include <QDebug>
#include "QApplication"
#include <QFileInfo>
class DragArea : public QWidget
{
	Q_OBJECT
public:
	explicit DragArea(QWidget *parent = nullptr);
	void handleEvent(QDropEvent *event);
	void setResizeTo(QWidget* value);

protected:
	void paintEvent(QPaintEvent *event) override;
	void dropEvent(QDropEvent* event) override;
	void dragEnterEvent(QDragEnterEvent *event) override;
	void dragMoveEvent(QDragMoveEvent *event) override;
	void dragLeaveEvent(QDragLeaveEvent *event) override;
	bool eventFilter(QObject* object,QEvent* event) override;
signals:
	void openFileByPath(QString filePath, bool openInHex = false);
	void changeDir(QString path);
private:
	QWidget *resizeTo;
};

#endif // DRAGAREA_H
