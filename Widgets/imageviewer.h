#ifndef VIEWER_H
#define VIEWER_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QWheelEvent>
#include <QPushButton>
#include <QLabel>
#include <QSlider>
#include <QGridLayout>
#include <QGraphicsOpacityEffect>
#include <QTimer>
#include <QFrame>
#include <QShortcut>
#include "Style/appearance.h"
#include "Widgets/hotkeyseditor.h"
class ImageViewer : public QGraphicsView, public IHotkeys {
    Q_OBJECT

public:
    explicit ImageViewer(QWidget *parent = nullptr);
    void setImagePath(const QString &path);
    QString getFilePath() const;

protected:
	void wheelEvent(QWheelEvent *event) override;
	bool eventFilter(QObject* object,QEvent* event) override;
	/* ============================================================================================================================= */
				//Прозрачность
				const float enabledStateOpacity = 0.85f;
				const float disabledStateOpacity = 0.35f;
	/* ============================================================================================================================= */
				const QList<float> sliderScales{ // шаги слайдера в процентах
									//Значения должны быть строго по возрастанию!
						0.125f,		//12.5%
						0.25f,		//25%
						0.5f,		//50%
						0.75f,		//75%
						1.0f,		//100%
						2.0f,		//200%
						3.0f,		//300%
						4.0f,		//400%
						5.0f,		//500%
						6.0f,		//600%
						7.0f,		//700%
						8.0f,		//800%
					};
				const float minSize = sliderScales.first();
				const float maxSize = sliderScales.last();
	/* ============================================================================================================================= */

private:
	const QString visibleName = QString(tr("Image Viewer"));
	const float scaleFactor = 1.1f;
	float scaleSize = 1;
	bool isFirst = true; //индикатор того, что функция запущена впервые (см. реализацию eventFilter)
    QGraphicsScene *scene;
    QGraphicsPixmapItem *item;
    QString filePath;
	Appearance *appearance;
	QPushButton *buttonPlus;
	QPushButton *buttonMinus;
	QPushButton *buttonScaleReset;
	QPushButton *buttonFitToScreen;
	QSlider *scaleBar;
	QLabel *currentSize;
	QFrame  *toolBarContainer;
	QGraphicsOpacityEffect *effect;
	QGridLayout *toolBarLayout;
	void updateToolBar();
	void zoomByStep(int);
	int calcHandlePos();
	float getScaleByPos(int value);
	HotkeyEditor_MAP hotkeysList;
	QList<QShortcut*> shortcutsList;
	void initHotkeys() override;

private slots:
	void scaleUp(float multiplier = 1);
	void scaleDown(float multiplier = 1);
	void scaleSet(float setTo = 1);
	void handleSliderTrigger(int action);
	void handleButtonPlus();
	void handleButtonMinus();
	void fitToScreen();
	void updateStyle();
	void rotateLeft();
	void rotateRight();
	void updateHotkeys() override;
};

#endif // VIEWER_H
