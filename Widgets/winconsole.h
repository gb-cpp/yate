#ifndef WINCONSOLE_H
#define WINCONSOLE_H

#include <QWidget>
#include <QPlainTextEdit>
#include <QDebug>
#include <QProcess>
#include <QTextCodec>
#include <QPalette>
#include <QScrollBar>
#include <QApplication>
#include <QClipboard>


class winConsole : public QPlainTextEdit {
    Q_OBJECT

public:
    explicit winConsole(QWidget *parent = nullptr);
    ~winConsole() override;
    QString getQplainTest();


protected:
    void keyPressEvent(QKeyEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *) override;     //отключаем двойной клик мыши
    void contextMenuEvent(QContextMenuEvent *) override;    //отключаем контекстное меню


private:
    const int CURSOR_WIDTH = 7;
    QString userText;
    QProcess *m_process;
    QClipboard *clipboard;
    QTextCursor cursor;
    QByteArray output;
    QTextCodec *codec;

    void on_keyEnter_clicked();
    void on_keyBackspace_clicked();
    void moveCursor(QTextCursor::MoveOperation operation);

private slots:
    void outConsole();
    void outErrortoConsole();
};

#endif // WINCONSOLE_H
