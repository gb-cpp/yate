#ifndef WIDGETFOUNDATION_H
#define WIDGETFOUNDATION_H

#include <QWidget>
#include <QSplitter>
#include <QVector>
#include <QVBoxLayout>
#include <QPushButton>

#include "workspacesplitter.h"

/* Этот класс что-то типо обертки для пользовательского виджета ================================================================= */

class WidgetFoundation : public QWidget {
    Q_OBJECT

public:
    WidgetFoundation(QWidget *parent = nullptr);
    void setWidget(QWidget *widget);                            // Сюда присваиваем пользовательский виджет
    void setPWidgets(QVector<QWidget *> *value, QVector<QWidget *> *valueVector);                // Сюда присваиваем указатель на вектор из класса WorkspaceSplitter
    void setWorkspaceSplitter(WorkspaceSplitter *value);        // А здесь сам указатель на класс WorkspaceSplitter
    QWidget *getWidget();

private:
    WorkspaceSplitter *workspaceSplitter;
    QVector<QWidget*> *pWidgets;                                // Указатель на вектор из класса WorkspaceSplitter
    QVector<QWidget*> *vector;
    QPushButton *btnClose;
    QVBoxLayout *mainLayout;

private slots:
    void on_btnClose_clicked();
};

#endif // WIDGETFOUNDATION_H
