#include "hotkeyseditor.h"
#include "ui_hotkeyseditor.h"
HotkeysEditor *HotkeysEditor::instance = nullptr;
HotkeysEditor::HotkeysEditor(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::HotkeysEditor)
	{
		ui->setupUi(this);
		this->setWindowTitle(titleName);
		this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        initHotkeyEditor();
	}

HotkeyEditor_MAP HotkeysEditor::getHotkeys(QWidget* widget)
	{
		QString key = widget->metaObject()->className();
		if(weights.contains(key)){
			key = QString::number(weights.value(key)) + "_" +key;
		}
		return mainContainer.value(key).hotkeys_list;
	}

void HotkeysEditor::sendHotkeys(QWidget* widget, const QString visibleName, HotkeyEditor_MAP hotkeysList,quint32 weight)
	{
		QString key = widget->metaObject()->className();
		if(weight>100) weight=100;
		weights.insert(key,weight);
		key = QString::number(weight) + "_" + key;
		widget_info temp_container;
		temp_container.visible_name = visibleName;
		temp_container.hotkeys_list = hotkeysList;

		if(!defaultsContainer.contains(key)){
			defaultsContainer.insert(key,temp_container);
		}

		if(mainContainer.contains(key)){
			mainContainer[key].visible_name = visibleName;
			if(mainContainer.value(key).hotkeys_list != hotkeysList){
				HotkeyEditor_MAP::const_iterator listIterator = hotkeysList.constBegin();
				while (listIterator != hotkeysList.constEnd()) {
					if(mainContainer.value(key).hotkeys_list.contains(listIterator.key())){
						mainContainer[key].hotkeys_list[listIterator.key()].visible_name = hotkeysList.value(listIterator.key()).visible_name;
						mainContainer[key].hotkeys_list[listIterator.key()].group_name = hotkeysList.value(listIterator.key()).group_name;
					}
					++listIterator;
				}
			}
			emit updated();
		}else{
			mainContainer.insert(key,temp_container);
		}
	}

void HotkeysEditor::initHotkeyEditor()
	{
		if(!first) return;
		first = false;
		defaultsContainer.clear();
		mainContainer.clear();
		weights.clear();
		mainModel = new QStandardItemModel(this);
		ui->listView->setModel(mainModel);
		settingsLoader = new SettingsLoader(mainSettings);
		loadSettingsFile(&mainContainer);
	}


HotkeysEditor::~HotkeysEditor()
	{
		delete ui;

	}

void HotkeysEditor::showEvent(QShowEvent* event)
	{
		rebuildScreen();
		event->accept();
	}

void HotkeysEditor::closeEvent(QCloseEvent* event)
	{
		saveHotkeysToFile();
		emit updated();
		event->accept();
	}

void HotkeysEditor::rebuildScreen()
	{
		/* ============================================================================================================================= */
		qint32 nameLabelWidth = 250;
		qint32 textFieldWidth = 100;
		/* ============================================================================================================================= */
		mainModel->clear();								//очищаем модель и удаляем старый QStackedWidget вместе со всеми дочерними объектами
		if(mainWidget != nullptr) delete mainWidget;
		mainWidget = new QStackedWidget(this);
		ui->horizontalLayout->addWidget(mainWidget);
		/* ============================================================================================================================= */
		QMap<QString, widget_info>::const_iterator containerIterator = mainContainer.constBegin();	//главный итератор по контейнеру виджетов
		while (containerIterator != mainContainer.constEnd()) {

			QStandardItem *item = new QStandardItem(containerIterator->visible_name); //создаем элемент для модели
				if(containerIterator->visible_name.isEmpty()) item->setText(containerIterator.key()); //если читаемого имени нет, заменяем внутренним именем
			mainModel->appendRow(item);			//добавляем элемент в модель (строка в виджете QListView (создан один раз в редакторе дизайна))


				//инициализация основных элементов интерфейса
				QScrollArea *scroll = new QScrollArea(mainWidget);
				scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
				QWidget *content = new QWidget(scroll);
				content->setObjectName(containerIterator.key());
				QGridLayout *layout = new QGridLayout(content);

				int index = 0; //индекс строк в QGridLayout
					HotkeyEditor_MAP hotkeysList = mainContainer.value(containerIterator.key()).hotkeys_list; //извлекаем список горячих клавиш текущего виджета


					//составление списка групп
					QString groupName = QString("");
					QList<QString> groupList;
					groupList.append(groupName);

					HotkeyEditor_MAP::const_iterator listIterator = hotkeysList.constBegin();
					while (listIterator != hotkeysList.constEnd()) {
							if(groupName != listIterator.value().group_name){
								groupName = listIterator.value().group_name;
								if(!groupList.contains(groupName)) groupList.append(groupName);
							}
							++listIterator;
					}
					//основной цикл
					foreach(auto obj, groupList){
						//добавление метки с именем группы, если оно есть
						if(!obj.isEmpty()){
							QLabel *group = new QLabel(obj);
							layout->addWidget(group,index,0);
							++index;
						}

						listIterator = hotkeysList.constBegin();
						while (listIterator != hotkeysList.constEnd()) {
							if(listIterator.value().group_name == obj){
								//метка с именем
								QLabel *keyName = new QLabel("\t"+listIterator.value().visible_name);
									if(listIterator.value().visible_name.isEmpty()) keyName->setText("\t"+listIterator.key());
									//если нет имени горячей клавиши, замена "внутренним именем"
								keyName->setFixedWidth(nameLabelWidth);

								//поле ввода
								QKeySequenceEdit *kse = new QKeySequenceEdit(listIterator.value().key_sequence.toString());
								kse->setObjectName(listIterator.key());
								kse->setFixedWidth(textFieldWidth);
								connect(kse,&QKeySequenceEdit::editingFinished,this,[=](){kseFinishedEditing(kse,content);});

								//кнопка сброса на стандартное значение
								QPushButton *reset = new QPushButton(tr("Default"));
								connect(reset,&QPushButton::released,this,[=](){resetDefault(kse,content);});

								//кнопка сброса на стандартное значение
								QPushButton *clear = new QPushButton(tr("Clear"));
								connect(clear,&QPushButton::released,this,[=](){clearKSE(kse,content);});

								//метка "конфликт с горячей клавишей"
								QLabel *conflict = new QLabel();
								conflict->setStyleSheet("QLabel { color : red; }");
								conflict->setObjectName(kse->objectName());
								conflict->setFixedWidth(5);

								layout->addWidget(keyName,index,0);
								layout->addWidget(kse,index,2);
								layout->addWidget(reset,index,3);
								layout->addWidget(clear,index,4);
								layout->addWidget(conflict,index,1);
								++index;
							}
							++listIterator;
						}
					}
			content->setLayout(layout);
			scroll->setWidget(content);
			scroll->setAlignment(Qt::AlignHCenter);
			mainWidget->addWidget(scroll);
			checkConflict(content);
			++containerIterator;
		}
		ui->listView->setCurrentIndex(mainModel->index(0,0));
	}

void HotkeysEditor::saveHotkeysToFile()
	{
		if (settingsLoader != nullptr) {
			QMap<QString, widget_info>::const_iterator containerIterator = mainContainer.constBegin();
			while (containerIterator != mainContainer.constEnd()) {
				QString name = containerIterator.key();
				HotkeyEditor_MAP hotkeysList = containerIterator.value().hotkeys_list;
					HotkeyEditor_MAP::const_iterator listIterator = hotkeysList.constBegin();
					while (listIterator != hotkeysList.constEnd()) {
						settingsLoader->setValue({name,listIterator->group_name,listIterator.key(),"key_sequence"},listIterator.value().key_sequence.toString());
						++listIterator;
					}

				++containerIterator;
			}
			try {
				settingsLoader->saveJsonToFile(mainSettings);
			}
			catch (std::runtime_error saveFileErr) {
				qDebug() << "Cannot save file: " << QString(saveFileErr.what());
				return;
			}
		}
	}

void HotkeysEditor::loadSettingsFile(QMap<QString, widget_info>* container)
	{
		QJsonObject jsonObject = settingsLoader->getRawObject();
			QJsonObject::const_iterator widget = jsonObject.constBegin();
			while (widget != jsonObject.constEnd()) {
				QString name = widget.key();
				HotkeyEditor_MAP hotkeysList;
					QJsonObject::const_iterator group = widget.value().toObject().constBegin();
					while (group != widget.value().toObject().constEnd()) {
						QJsonObject::const_iterator keys = group.value().toObject().constBegin();
							while (keys != group.value().toObject().constEnd()){
                                //по каким то причинам в версии QT под Linux в QJSonObject метод value ломает контейнер
		/*Багфикс под Linux*/	QJsonObject::ConstIterator a = keys.value().toObject().constBegin();
		/*Багфикс под Linux*/	QString key_sequence = a.value().toString();
								HotkeyEditor_shortcut temp_short;
								temp_short.group_name = group.key();
		/*Багфикс под Linux*/	temp_short.key_sequence = key_sequence;
	/*Это нормальный вариант*/	//temp_short.key_sequence = keys.value().toObject().value("key_sequence").toString();
                                hotkeysList.insert(keys.key(),temp_short);
								++keys;
							}
						++group;
					}
					widget_info temp_container;
					temp_container.visible_name = widget.value().toObject().value("visible_name").toString();
					temp_container.hotkeys_list = hotkeysList;
                if(container)
                    container->insert(name,temp_container);
				++widget;
			}
	}

void HotkeysEditor::checkConflict(QKeySequenceEdit* kse)
	{
		checkConflict(kse->parentWidget());
	}

void HotkeysEditor::checkConflict(QWidget* contentWindow)
	{
		QList<QKeySequenceEdit *> childKSE = contentWindow->findChildren<QKeySequenceEdit *>(QString());
		QList<QKeySequenceEdit *> conflictList;
		QMap<QString,QStringList> conflictsWith;
		foreach(auto kse,childKSE){
			auto conflictLabel = contentWindow->findChild<QLabel*>(QString(kse->objectName()));
				if(conflictLabel == nullptr) continue;
			conflictLabel->setText("");
			conflictLabel->setToolTip("");
			if(!kse->keySequence().toString().isEmpty()){
			foreach(auto obj,childKSE){
					if(kse->keySequence() == obj->keySequence() && kse!=obj){
						if (!conflictList.contains(obj)) conflictList.append(obj);
						conflictsWith[kse->objectName()].append(obj->objectName());
					}
				}
			}
		}
		foreach(auto kse,conflictList){
			auto conflictLabel = contentWindow->findChild<QLabel*>(QString(kse->objectName()));
				if(conflictLabel == nullptr) continue;
			QString tooTip;
			foreach(auto obj,conflictsWith.value(kse->objectName())){
				tooTip.append("\n" + mainContainer.value(contentWindow->objectName()).hotkeys_list.value(obj).visible_name);
			}

			conflictLabel->setText("!");
			conflictLabel->setToolTip(tr("There is a conflict with other hotkeys:")+tooTip);
		}
	}

void HotkeysEditor::kseFinishedEditing(QKeySequenceEdit* kse, QWidget* contentWindow)
	{
		const QString mainKey = contentWindow->objectName();
		const QString fieldKey = kse->objectName();
		const QString value = kse->keySequence().toString();
		mainContainer[mainKey].hotkeys_list[fieldKey].key_sequence = value;
		checkConflict(kse);
		emit updated();
	}

void HotkeysEditor::resetDefault(QKeySequenceEdit* kse, QWidget* contentWindow)
	{
		const QString mainKey = contentWindow->objectName();
		const QString fieldKey = kse->objectName();
		const QString value = defaultsContainer[mainKey].hotkeys_list[fieldKey].key_sequence.toString();
		mainContainer[mainKey].hotkeys_list[fieldKey].key_sequence = value;
		kse->setKeySequence(value);
		checkConflict(kse);
		emit updated();
	}

void HotkeysEditor::clearKSE(QKeySequenceEdit* kse, QWidget* contentWindow)
	{
		const QString mainKey = contentWindow->objectName();
		const QString fieldKey = kse->objectName();
		const QString value = QString();
		mainContainer[mainKey].hotkeys_list[fieldKey].key_sequence = value;
		kse->setKeySequence(value);
		checkConflict(kse);
		emit updated();
	}

void HotkeysEditor::on_listView_activated(const QModelIndex &index)
{
		mainWidget->setCurrentIndex(index.row());
}

void HotkeysEditor::on_listView_clicked(const QModelIndex &index)
{
		mainWidget->setCurrentIndex(index.row());
}
