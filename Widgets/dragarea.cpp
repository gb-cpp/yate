#include "dragarea.h"
DragArea::DragArea(QWidget *parent) : QWidget(parent)
	{
		setAcceptDrops(true);
		setVisible(false);
		qApp->installEventFilter(this);
	}

void DragArea::handleEvent(QDropEvent* event)
	{
		const QMimeData* mimeData = event->mimeData();

		if (mimeData->hasUrls())
		{
			QStringList pathList;
			QList<QUrl> urlList = mimeData->urls();

			for (int i = 0; i < urlList.size(); i++)
			{
				pathList.append(urlList.at(i).toLocalFile());
			}

			foreach(auto obj,pathList){
				QFileInfo info(obj);
					if(info.isDir()){
						emit changeDir(obj);
					}else if(info.isFile()){
						emit openFileByPath(obj,false);
					}
			}
		}
	}


void DragArea::paintEvent(QPaintEvent* event)
	{
		Q_UNUSED(event);
		setGeometry(resizeTo->geometry());
	}

void DragArea::dropEvent(QDropEvent* event)
	{
		handleEvent(event);
		event->acceptProposedAction();
		this->setVisible(false);
	}

void DragArea::dragEnterEvent(QDragEnterEvent* event)
	{
		this->setVisible(true);
		event->acceptProposedAction();
	}

void DragArea::dragMoveEvent(QDragMoveEvent* event)
	{
		event->acceptProposedAction();
	}

void DragArea::dragLeaveEvent(QDragLeaveEvent* event)
	{
		event->accept();
		this->setVisible(false);
	}

bool DragArea::eventFilter(QObject* object, QEvent* event)
	{
		switch (event->type())
		{
		case QEvent::DragLeave:
			this->setVisible(false);
			break;
		case QEvent::DragEnter:
			this->setVisible(true);
			break;
		default:
			break;
		}
		return QWidget::eventFilter(object,event);
	}

void DragArea::setResizeTo(QWidget* value)
	{
		resizeTo = value;
	}
