#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include <QCloseEvent>
#include <QTextEdit>
#include <QPainter>
#include <QTextBlock>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QGuiApplication>
#include <QFileDialog>
#include <QPlainTextEdit>

#include <QCompleter>
#include <QStringListModel>
#include <QKeyEvent>
#include <QEvent>
#include <QAbstractItemView>
#include <QModelIndex>
#include <QAbstractItemModel>
#include <QApplication>

#include <QApplication>
#include <QCompleter>
#include <QFile>

#include "Buffers/buffer.h"
#include "HighLighters/brackets.h"
#include "Style/appearance.h"
#include "menubar.h"
#include "settingsloader.h"


QT_BEGIN_NAMESPACE

class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;

//Автодополнение
class QAbstractItemModel;
class QCompleter;

QT_END_NAMESPACE
class LineNumberArea;
class Appearance;


class TextEdit : public QPlainTextEdit {
    Q_OBJECT

public:
    TextEdit(Appearance *appearance = nullptr, Buffer *buffer = nullptr);
    ~TextEdit();
    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();
    Buffer *buffer() const;
    void setBuffer(Buffer *buffer);
    bool saveFile();
    bool saveAsFile();

    // Автодополнение
    void setCompleter(QCompleter *c);
    QCompleter *completer() const;
    QString getAutoCompletition(); //Возвращает установленное автодополнение

protected:
    void resizeEvent(QResizeEvent *event) override;
    void closeEvent(QCloseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    // Автодополнение
    void keyPressEvent(QKeyEvent *e) override;
    void focusInEvent(QFocusEvent *e) override;

private:
    QWidget *colorWidget;

    Buffer *m_buffer;
    Appearance *m_appearance;
    QWidget *lineNumberArea;
    const int lineNumberAreaPadding = 15;
    bool VisibleLineNumber;
    bool visibleColorWidget;
    void HighLighterText();

    //Автодополнение
    QAbstractItemModel *modelFromFile(const QString& fileName);
    QString textUnderCursor() const;
    QCompleter *c = nullptr;
    void setAutocompletionText();
    void setAutocompletionCpp();
    bool VisibleAutocompletionText;
    bool VisibleAutocompletionCpp;
    bool VisibleAutocompletionOff;

private slots:
    void deleteColorWidget();
    void updateLineNumberAreaWidth();
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &rect, int numPixelsScrolledVertically);
    void newFile();
    void loadFile();
    bool maybeSave();
    void documentWasModified();
    void showErrorMsg(const QString &err);

    //Автодополнение
    void insertCompletion(const QString &completion);



public slots:
    void  VisibleLine(bool status) {
        VisibleLineNumber = status;
        updateLineNumberAreaWidth();
    }
    bool getVisibleLine() {
           return VisibleLineNumber;
    }
    void  setVisibleColorWidget(bool status) {
        visibleColorWidget = status;
    }
    bool getVisibleColorWidget() {
           return visibleColorWidget;
    }

    //Автодополнение
    void VisibleAutocompleteText(bool state) {
        VisibleAutocompletionText = state;
        setAutocompletionText();
    }

    void VisibleAutocompleteCpp(bool state) {
        VisibleAutocompletionCpp = state;
        setAutocompletionCpp();
    }

    void VisibleAutocompleteOff(bool state) {
        VisibleAutocompletionOff = state;
        this->setCompleter(nullptr);
    }

signals:
    void deleteBuffer(Buffer *);
};

class LineNumberArea : public QWidget {
public:
    LineNumberArea(TextEdit *editor) : QWidget(editor), codeEditor(editor) {}
    QSize sizeHint() const override {
        return QSize(codeEditor->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) override {
        codeEditor->lineNumberAreaPaintEvent(event);
    }

private:
    TextEdit *codeEditor;
};

#endif // TEXTEDIT_H
