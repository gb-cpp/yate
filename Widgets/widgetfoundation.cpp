#include "widgetfoundation.h"

/* Создаем кнопку, которая будет закрывать выбранный QSplitter ================================================================== */

WidgetFoundation::WidgetFoundation(QWidget *parent) : QWidget(parent) {
    pWidgets = nullptr;
    workspaceSplitter = nullptr;
    btnClose = new QPushButton("");
    btnClose->setObjectName("widgetBtnClose");
    btnClose->setFixedSize(20, 20);
    connect(btnClose, &QPushButton::clicked, this, &WidgetFoundation::on_btnClose_clicked);
    mainLayout = new QVBoxLayout;
    mainLayout->addWidget(btnClose, 0, Qt::AlignTop | Qt::AlignRight);
    setLayout(mainLayout);
}

/* Пользовательский виджет будет расширятся в зависимости от размеров QSplitter ================================================= */

void WidgetFoundation::setWidget(QWidget *widget) {
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mainLayout->addWidget(widget);
}

/* Берем индекс из вектора, в котором один из элементов хранит указатель на этот класс, и берем индекс этого элемента =========== */

void WidgetFoundation::on_btnClose_clicked() {
    int index = pWidgets->indexOf(this);
    int indexWidget = workspaceSplitter->indexOf(this);
    pWidgets->remove(index);                                                       // Удаляем из QSplitter соответствующий виджет
    vector->remove(index);
    delete workspaceSplitter->widget(indexWidget);
}

/* ============================================================================================================================= */

void WidgetFoundation::setPWidgets(QVector<QWidget *> *value, QVector<QWidget *> *valueVector) {
    pWidgets = value;
    vector = valueVector;
}

/* ============================================================================================================================= */

void WidgetFoundation::setWorkspaceSplitter(WorkspaceSplitter *value) {
    workspaceSplitter = value;
}

/* ============================================================================================================================= */

QWidget *WidgetFoundation::getWidget() {
    return this;
}
