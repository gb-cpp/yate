#include "imageviewer.h"
ImageViewer::ImageViewer(QWidget *parent) : QGraphicsView(parent) {
    setDragMode(QGraphicsView::ScrollHandDrag);
    scene = new QGraphicsScene(this);
    item = new QGraphicsPixmapItem();
    item->setTransformationMode(Qt::SmoothTransformation);
    scene->addItem(item);
    setScene(scene);
	setTransformationAnchor( QGraphicsView::AnchorUnderMouse );
	//Константы интерфейса панели инструментов
		//QLabel
			const int labelWidth = 50;
		//Слайдер
			const int sliderWidth = 225;
		//Размер кнопок
			const QSize buttonfixedSize = QSize(20,20);
		//Основной контейнер
			const int aX = 15; //сдвиг по X от левого края
			const int aY = 15; //сдвиг по Y от левого края
			//const int aWidth = labelWidth + sliderWidth + buttonfixedSize.width() * 4 + 50; /* labelWidth + sliderWidth + buttonfixedSize * 4 + 50 */
			//const int aHeight = 40;

	/* ============================================================================================================================= */
	toolBarContainer = new QFrame(this);					//базовый виджет-контейнер для кнопок и слайдера
	//toolBarContainer->setGeometry(aX,aY,aWidth,aHeight);
	toolBarContainer->move(aX,aY);
	toolBarContainer->installEventFilter(this);				//установка фильтра событий для перехвата событий мыши (функция bool eventFilter(QObject*,QEvent*) override;)
	effect = new QGraphicsOpacityEffect;					//добавляем эффект непрозрачности и начальное значение непрозрачности
	effect->setOpacity(disabledStateOpacity);
	toolBarContainer->setGraphicsEffect(effect);
	toolBarContainer->setFrameShape(QFrame::StyledPanel);

	/* ============================================================================================================================= */

	//инициализация кнопок
		//Плюс
		buttonPlus = new QPushButton(toolBarContainer);				buttonPlus->setToolTip(tr("Zoom in"));
		connect(buttonPlus,SIGNAL(released()),this,SLOT(handleButtonPlus()));
		buttonPlus->setIconSize(buttonfixedSize);
		buttonPlus->setFixedSize(buttonfixedSize);
		//Минус
		buttonMinus = new QPushButton(toolBarContainer);			buttonMinus->setToolTip(tr("Zoom out"));
		connect(buttonMinus,SIGNAL(released()),this,SLOT(handleButtonMinus()));
		buttonMinus->setIconSize(buttonfixedSize);
		buttonMinus->setFixedSize(buttonfixedSize);
		//100%
		buttonScaleReset = new QPushButton(toolBarContainer);		buttonScaleReset->setToolTip(tr("Zoom to 100%"));
		connect(buttonScaleReset,SIGNAL(released()),this,SLOT(scaleSet()));
		buttonScaleReset->setIconSize(buttonfixedSize);
		buttonScaleReset->setFixedSize(buttonfixedSize);
		//Подогнать под экран
		buttonFitToScreen = new QPushButton(toolBarContainer);		buttonFitToScreen->setToolTip(tr("Fit to screen"));
		connect(buttonFitToScreen,SIGNAL(released()),this,SLOT(fitToScreen()));
		buttonFitToScreen->setIconSize(buttonfixedSize);
		buttonFitToScreen->setFixedSize(buttonfixedSize);
		//Заготовка имен кнопок для редактора тем, чтобы можно было задать особые значения, обратившись через ID Selector
		buttonPlus->setObjectName("ImageViewerToolbar_roundButton");
		buttonMinus->setObjectName("ImageViewerToolbar_roundButton");
		buttonScaleReset->setObjectName("ImageViewerToolbar_squareButton");
		buttonFitToScreen->setObjectName("ImageViewerToolbar_squareButton");
	//инициализация слайдера
	scaleBar = new QSlider(Qt::Orientation::Horizontal,toolBarContainer);
	scaleBar->setRange(0,sliderScales.size()-1);
	scaleBar->setMinimumWidth(sliderWidth);
	scaleBar->setSingleStep(1);
	scaleBar->setPageStep(1);
	connect(scaleBar,SIGNAL(actionTriggered(int)),this,SLOT(handleSliderTrigger(int)));

	//инициализация метки, где выводится текущее значение размера
	currentSize = new QLabel(toolBarContainer);
	currentSize->setMinimumWidth(labelWidth);
	currentSize->setAlignment(Qt::AlignCenter);
	currentSize->setToolTip(tr("Zoom level"));

	//инициализация разметки
	toolBarLayout = new QGridLayout(toolBarContainer);	toolBarContainer->setLayout(toolBarLayout);
	toolBarLayout->addWidget(currentSize,0,0);
	toolBarLayout->addWidget(buttonMinus,0,1);
	toolBarLayout->addWidget(scaleBar,0,2);
	toolBarLayout->addWidget(buttonPlus,0,3);
	toolBarLayout->addWidget(buttonScaleReset,0,4);
	toolBarLayout->addWidget(buttonFitToScreen,0,5);
	updateToolBar();
	initHotkeys();
	connect(Appearance::getInstance(), SIGNAL(onUpdate()), this, SLOT(updateStyle()));
	updateStyle();
}

/* ============================================================================================================================= */

void ImageViewer::setImagePath(const QString &path) {
    this->filePath = path;
    QImage img(path);
    item->setPixmap(QPixmap::fromImage(img));
}

/* ============================================================================================================================= */

QString ImageViewer::getFilePath() const {
    return filePath;
}

/* ============================================================================================================================= */

void ImageViewer::wheelEvent(QWheelEvent *event) {
	if (event->angleDelta().y() > 0)
		{
		if(event->modifiers() & Qt::ShiftModifier)
			scaleUp(2);
		else if(event->modifiers() & Qt::ControlModifier)
			zoomByStep(1);
		else
			scaleUp();
		}
	else
		{
		if(event->modifiers() & Qt::ShiftModifier)
			scaleDown(2);
		else if(event->modifiers() & Qt::ControlModifier)
			zoomByStep(-1);
		else
			scaleDown();
		};
}

/* ============================================================================================================================= */
void ImageViewer::initHotkeys()
	{
		{
			HotkeyEditor_shortcut temp_short;
			temp_short.key_sequence = QKeySequence(Qt::Key_Left);
			temp_short.visible_name = tr("Rotate left");
			const QString key = "rotate_left";
			QShortcut *shortcut = new QShortcut(hotkeysList.value(key).key_sequence,this);
			shortcut->setObjectName(key);
			connect(shortcut,&QShortcut::activated, this, &ImageViewer::rotateLeft);
			shortcutsList.append(shortcut);
			hotkeysList.insert(key,temp_short);
		}
		{
			HotkeyEditor_shortcut temp_short;
			temp_short.key_sequence = QKeySequence(Qt::Key_Right);
			temp_short.visible_name = tr("Rotate Right");
			const QString key = "rotate_right";
			QShortcut *shortcut = new QShortcut(hotkeysList.value(key).key_sequence,this);
			shortcut->setObjectName(key);
			connect(shortcut,&QShortcut::activated, this, &ImageViewer::rotateRight);
			shortcutsList.append(shortcut);
			hotkeysList.insert(key,temp_short);
		}
		{
			HotkeyEditor_shortcut temp_short;
			temp_short.key_sequence = QKeySequence(Qt::Key_Return);
			temp_short.visible_name = tr("Fit to screen");
			temp_short.group_name = tr("Zoom");
			const QString key = "fit_to_screen";
			QShortcut *shortcut = new QShortcut(hotkeysList.value(key).key_sequence,this);
			shortcut->setObjectName(key);
			connect(shortcut,&QShortcut::activated, this, &ImageViewer::fitToScreen);
			shortcutsList.append(shortcut);
			hotkeysList.insert(key,temp_short);
		}
		{
			HotkeyEditor_shortcut temp_short;
			temp_short.key_sequence = QKeySequence(Qt::Key_Plus);
			temp_short.visible_name = tr("Zoom In");
			temp_short.group_name = tr("Zoom");
			const QString key = "zoom_in";
			QShortcut *shortcut = new QShortcut(hotkeysList.value(key).key_sequence,this);
			shortcut->setObjectName(key);
			connect(shortcut,&QShortcut::activated, this, [=](){scaleUp(1);});
			shortcutsList.append(shortcut);
			hotkeysList.insert(key,temp_short);
		}
		{
			HotkeyEditor_shortcut temp_short;
			temp_short.key_sequence = QKeySequence(Qt::SHIFT + Qt::Key_Plus);
			temp_short.visible_name = tr("Zoom In By Step");
			temp_short.group_name = tr("Zoom");
			const QString key = "zoom_in_step";
			QShortcut *shortcut = new QShortcut(hotkeysList.value(key).key_sequence,this);
			shortcut->setObjectName(key);
			connect(shortcut,&QShortcut::activated, this, [=](){zoomByStep(1);});
			shortcutsList.append(shortcut);
			hotkeysList.insert(key,temp_short);
		}
		{
			HotkeyEditor_shortcut temp_short;
			temp_short.key_sequence = QKeySequence(Qt::Key_Minus);
			temp_short.visible_name = tr("Zoom Out");
			temp_short.group_name = tr("Zoom");
			const QString key = "zoom_out";
			QShortcut *shortcut = new QShortcut(hotkeysList.value(key).key_sequence,this);
			shortcut->setObjectName(key);
			connect(shortcut,&QShortcut::activated, this, [=](){scaleDown(1);});
			shortcutsList.append(shortcut);
			hotkeysList.insert(key,temp_short);
		}
		{
			HotkeyEditor_shortcut temp_short;
			temp_short.key_sequence = QKeySequence(Qt::SHIFT + Qt::Key_Minus);
			temp_short.visible_name = tr("Zoom Out By Step");
			temp_short.group_name = tr("Zoom");
			const QString key = "zoom_out_step";
			QShortcut *shortcut = new QShortcut(hotkeysList.value(key).key_sequence,this);
			shortcut->setObjectName(key);
			connect(shortcut,&QShortcut::activated, this, [=](){zoomByStep(-1);});
			shortcutsList.append(shortcut);
			hotkeysList.insert(key,temp_short);
		}
		connect(HotkeysEditor::getInstance(),&HotkeysEditor::updated,this,&ImageViewer::updateHotkeys);
		HotkeysEditor::getInstance()->sendHotkeys(this,visibleName,hotkeysList);
	}

void ImageViewer::updateHotkeys()
	{
		hotkeysList = HotkeysEditor::getInstance()->getHotkeys(this);
		foreach(auto obj,shortcutsList){
			obj->setKey(hotkeysList.value(obj->objectName()).key_sequence);
		}
	}

bool ImageViewer::eventFilter(QObject* object, QEvent* event)
	{
		switch (event->type())
		{
		case QEvent::Leave:
			effect->setOpacity(disabledStateOpacity);
			break;
		case QEvent::Enter:
			effect->setOpacity(enabledStateOpacity);
			break;
		case QEvent::Paint:
			//это событие отрисовки тулбара
			if(isFirst){
				// из-за проблем с очередностью загрузки виджетов и инциализацией, чтобы не разбирать половину классов, добавлена простая проверка
				// если это первый раз, когда сработал данный ивент, то подогнать изображение под экран
				fitToScreen();
				isFirst = false;
			}
			break;
		default:
			break;
		}
		return QGraphicsView::eventFilter(object,event);
	}



void ImageViewer::updateToolBar()
	{
		const QString procent = QString::number(qRound(scaleSize*10000)/100.0f)+QString("%");
		const QString zoomLevel = QString(tr("Zoom level: %1")).arg(procent);
		currentSize->setText(procent);
		currentSize->setToolTip(zoomLevel);
		scaleBar->setValue(calcHandlePos()); //"примагничивает" бегунок слайдера к ближайщему значению (например: при 45% бегунок встанет на 50% (само значение размера будет 45%))
		scaleBar->setToolTip(zoomLevel);
	}

void ImageViewer::zoomByStep(int value)
	{
		const float fromSlider = getScaleByPos(scaleBar->sliderPosition());
		const float currentScale = scaleSize;
		const int moveToPosition = scaleBar->sliderPosition()+value;
		if (value>0){
			if(fromSlider>currentScale)	scaleSet(fromSlider);
			else{
				if (moveToPosition > scaleBar->maximum()) scaleSet(maxSize);
				else scaleBar->setSliderPosition(moveToPosition);
			}
		}else{
			if(fromSlider<currentScale) scaleSet(fromSlider);
			else{
				if (moveToPosition < scaleBar->minimum()) scaleSet(minSize);
				else scaleBar->setSliderPosition(moveToPosition);
			}
		}

	}

int ImageViewer::calcHandlePos() //позиция слайдера по размеру
	{
		float value;
		for (int i = 0; i < sliderScales.size(); ++i) {
			if(i + 1 >= sliderScales.size()) return sliderScales.size();
			value = sliderScales.at(i);
			value +=(sliderScales.at(i+1)-value)/2;
			if (scaleSize<value) return i;
		}
		return sliderScales.size();  //теоретически никогда не должно срабатывать. Добавлено, чтобы компилятор не ругался.

	}

float ImageViewer::getScaleByPos(int value) //размер по позиции слайдера
	{
		if(value<0){
			return minSize;
		}
		if(value>sliderScales.size()){
			return maxSize;
		}
		return sliderScales.at(value);
	}

void ImageViewer::fitToScreen()
	{
		const int constant = 10; //константа добавлена, чтобы исключить появление полос прокрутки
		const float scaleByWidth = (float)this->width()/(item->pixmap().width()+constant);
		const float scaleByHeight = (float)this->height()/(item->pixmap().height()+constant);
		if(scaleByWidth<scaleByHeight) scaleSet(scaleByWidth); else scaleSet(scaleByHeight);
	}

void ImageViewer::updateStyle()
	{
        if(Appearance::getInstance()->getThemeManager()->getCurrentTheme()->isDark()){
			buttonPlus->setIcon(QIcon(":/images/icons/white/round_plus_icon&24.png"));
			buttonMinus->setIcon(QIcon(":/images/icons/white/round_minus_icon&24.png"));
			buttonScaleReset->setIcon(QIcon(":/images/icons/white/zoom_icon&24.png"));
			buttonFitToScreen->setIcon(QIcon(":/images/icons/white/expand_icon&24.png"));
		}else{
			buttonPlus->setIcon(QIcon(":/images/icons/black/round_plus_icon&24.png"));
			buttonMinus->setIcon(QIcon(":/images/icons/black/round_minus_icon&24.png"));
			buttonScaleReset->setIcon(QIcon(":/images/icons/black/zoom_icon&24.png"));
			buttonFitToScreen->setIcon(QIcon(":/images/icons/black/expand_icon&24.png"));
		}
	}

void ImageViewer::rotateLeft()
	{
		rotate(-90);
	}
void ImageViewer::rotateRight()
	{
		rotate(90);
	}

void ImageViewer::scaleUp(float multiplier)
	{
		const float Factor = scaleFactor*multiplier;
		const float predict = scaleSize*Factor;
		if(predict>=maxSize){scaleSet(maxSize);return;}
		scale(Factor, Factor);
		scaleSize*=Factor;
		updateToolBar();
	}

void ImageViewer::scaleDown(float multiplier)
	{

		const float Factor = scaleFactor*multiplier;
		const float predict = scaleSize*1/Factor;
		if(predict<=minSize){scaleSet(minSize);return;}
		scale(1/Factor, 1/Factor);
		scaleSize*=1/Factor;
		updateToolBar();
	}

void ImageViewer::scaleSet(float setTo)
	{
		if(setTo<minSize) setTo=minSize;
		else if(setTo>maxSize) setTo=maxSize;
		scale(setTo/scaleSize,setTo/scaleSize);
		scaleSize*=setTo/scaleSize;
		updateToolBar();
	}

void ImageViewer::handleSliderTrigger(int action)
	{
		Q_UNUSED(action);
		scaleSet(getScaleByPos(scaleBar->sliderPosition()));
	}

void ImageViewer::handleButtonPlus()
	{
		zoomByStep(1);
	}

void ImageViewer::handleButtonMinus()
	{
		zoomByStep(-1);
	}
